#include<stdio.h>
#include<stdlib.h>
#include"image.h"
#include"imgio.h"

usage()
{
	fprintf(stderr, "copy inputfile to outputfile\n"
			"usage: imgDup.a inputfile outputfile\n");
}
main( int argc, char **argv )
{
	IMAGE_FILE	*imgf1, *imgf2;
	char	*fname1, *fname2;

	fname1 = argv[1];
	fname2 = argv[2];

	if( argc != 3 ){
		usage();
		exit(1);
	}

	img_err_exit_set( 0 );
	imgf1 = image_file_open( fname1, IMAGE_RDONLY, IMAGE_BUFFERED);
	if( imgf1 == NULL ){
		fprintf(stderr, "file open error %s\n", fname1 );
		exit(1);
	}

	imgf2 = image_file_create( fname2, IMAGE_TRUNC, 0, imgf1->image );
	if( imgf2 == NULL ){
		fprintf(stderr, "file open error %s\n", fname1 );
		exit(1);
	}

	image_file_close( imgf2 );
	image_file_close( imgf1 );
}
