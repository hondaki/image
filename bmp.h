#if ! defined ( BMP_H )
#define	BMP_H

typedef struct tagBITMAPFILEHEADER2 {
/*	wingdi.h definition included from Windows.h
        WORD    bfType;
        DWORD   bfSize;
        WORD    bfReserved1;
        WORD    bfReserved2;
        DWORD   bfOffBits;
        unsigned short    bfType;
        unsigned long	  bfSize;
        unsigned short    bfReserved1;
        unsigned short    bfReserved2;
        unsigned long	  bfOffBits;
*/
  unsigned char  bfType[2];
  int  bfSize;
  unsigned short bfReserved1;
  unsigned short bfReserved2;
  unsigned int	 bfOffBits;
} BITMAPFILEHEADER2;

typedef struct tagBITMAPINFOHEADER2{
    unsigned int  biSize;
    int           biWidth;
    int           biHeight;
    unsigned short biPlanes;
    unsigned short biBitCount;
    unsigned int  biCompression;
    unsigned int  biSizeImage;
    int           biXPixPerMeter;
    int           biYPixPerMeter;
    unsigned int  biClrUsed;
    unsigned int  biClrImporant;
} BITMAPINFOHEADER2;

typedef struct STRUCT_BMP {
	char *fname;
	unsigned int	w, h, bitCount;
	BITMAPFILEHEADER2	bf;
	BITMAPINFOHEADER2	bi;
	unsigned int	linebytes;
	unsigned char	*lineBuf;
	FILE	*fp;
} BMP;

BMP *bmpOpen( char *fname );
int bmpClose( BMP *bmp );
unsigned char *bmpReadLine( BMP *bmp, int row );
void bmpSwapBR( unsigned char *lineBuf, int w, int bitCount );
#endif
