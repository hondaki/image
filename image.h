/*
	image.h : image handling routines header file

	Copyright(C)2015 HONDA Kiyoshi

  	Contact: Dr. HONDA Kiyoshi
			P.O.Box 4, Khlong Luang, Pathumthani, 12120, Thailand
			Asian Institute of Technology

			e-mail: honda.kiyoshi@gmail.com
			http://www.rsgis.ait.ac.th/~honda

If you wish to use or distribute this software in a way
which does not follow the license stated below, you must obtain
written agreement from the copyright holder(s) above.

-------------------------------------------------------------------------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Program you will develop using this software must use the same
license with this software which is GNU General Public License
for any distribution.

The latest version of GNU General Public License may be obtained
from http://www.gnu.org/licenses

*/

/*

	Modification History

	22 Apr	2011	Major Change

	12 Dec 2003		map info I/O for ENVI ( for Lat/Lon and UTM ): geoRegistration
	29 June 2003	pixel spacing information added	dx, dy
	May 1996	Extension for Win32 by H.Ishibashi
	May 1996	Remove old function definition by H.Ishibashi
	Mar 1995	Extension for compress files by H.Ishibashi
	Oct 1994	Extension for Windows by H.Ishibashi
	Jan 1994	Extension for MS-DOS by H.Ishibashi

*/

#if !defined (IMAGE_H)
#define	IMAGE_H

#if defined (WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) 
#	if !defined (WINDOWS)
#		define WINDOWS
#	endif
#	if defined(LINUX)
#error	image.h: both WINDOWS and LINUX defined
#	endif
#endif 

#if !defined (WINDOWS)
#	if !defined (LINUX)
#		define LINUX
//#warning image.h: COMPILING FOR LINUX
#	endif
#endif

# include<sys/types.h>

#if defined (WINDOWS)
typedef	unsigned char	u_char;
typedef	unsigned short	u_short;
typedef unsigned int	u_int;
typedef	unsigned long	u_long;
#endif

typedef struct { float  r; float  im; } IMAGE_COMPLEX_STRUCT;
typedef struct { double r; double im; } IMAGE_DCOMPLEX_STRUCT;


/* linebytes will be roundup to the multiplication of ALIGN	*/
#define	ALIGN	8

#define IMAGE_PLANE     1
#define IMAGE_CHAR      2
#define IMAGE_SHORT     3
#define IMAGE_USHORT    4
#define IMAGE_LONG      5
#define IMAGE_ULONG     6
#define IMAGE_FLOAT     7
#define IMAGE_DOUBLE    8
#define IMAGE_COMPLEX   9
#define IMAGE_DCOMPLEX  10
#define IMAGE_8BIT_COL  11
#define IMAGE_24BIT_COL 12
#define IMAGE_32BIT_COL 13
#define IMAGE_UNKNOWN   -1

#define MAX_PIXEL_LENGTH 64
					/* image allocate unit		*/
#define	LINE_CONT_IMAGE		0	/* 1 line 1 band 	continuous */
#define	BSQ_ALLCONT_IMAGE	1	/* 1 img & bsq 		continuous */
#define	BIL_ALLCONT_IMAGE	2	/* 1 img & bil 		continuous */
#define	BSQ_CONT_IMAGE		3	/* 1 band 		continuous */
#define	BIL_CONT_IMAGE		4	/* n band 1 line 	continuous */

#define	IMAGE_PROJECTION_GEOGRAPHIC		0
#define	IMAGE_PROJECTION_UTM			1
#define	IMAGE_PROJECTION_ARBITRARY		2
#define	IMAGE_PROJECTION_NONE			3
#define	IMAGE_PROJECTION_JTM			4

#define	IMAGE_DATUM_WGS84				0
#define	IMAGE_DATUM_NONE				1
#define	IMAGE_DATUM_TOKYO				2

#define	IMAGE_UTMNORTH					0
#define	IMAGE_UTMSOUTH					1

#define	IMAGE_UNITDEGREE				0
#define	IMAGE_UNITMETER					1

typedef struct IMAGE_GEOREGISTRATION_INFO_TAG {
		int		datum;
		int		projection;
		double	geoRegistrationX,		geoRegistrationY;
		double	geoRegistrationMapX,	geoRegistrationMapY;
		double	dx, dy;
		int		Unit;
		int		UTMZoneNumber;
		int		UTMNorthSouth;
		char	datumName[256];
} IMAGE_GEOREGISTRATION_INFO;

typedef struct IMAGE_ATTR_TAG {
		int		haveNoDataValue;
		double	noDataValue;
} IMAGE_ATTR;

typedef struct IMAGE_TAG{
	int     w;
	int     h;
	int     nchan;
	int     data_type;      /*PLANE,CHAR,SHORT,..   */
	int		alloc_image_mode;	/* LINE_CONT_IMAGE, BSQ_ALLCONT_IMAGE ... */
	int     nbit;
	long long	size;
	long long	max_size;
	int     md_primary; 
	int 	md_linebytes;
	long long	md_channelbytes;
	unsigned char	***data;
	IMAGE_ATTR	attr;
	IMAGE_GEOREGISTRATION_INFO	geoRegistration;
} IMAGE;

#ifndef TRUE
# define TRUE -1
#endif
#ifndef FALSE
# define FALSE  (!TRUE)
#endif

//#define image_size(nbit,w,h,nchan)	(((unsigned long)(nbit)*(w)*(h)*(nchan))>>3)
#define image_size(nbit,w,h,nchan)	((((long long)((long long) ((nbit)>>3))*((long long) w)*((long long) h)))*((long long) nchan))

IMAGE	*new_image( int w, int h, int nchan, int data_type);
IMAGE	*image_alloc( int w, int h, int data_type, int nchan );
IMAGE	*image_point( int w, int h, int data_type, int nchan,u_char ***data );
void	image_point_set( IMAGE *img, u_char ***data );
IMAGE	*image_skelton( int w, int h, int data_type, int nchan );
void	image_destroy( IMAGE *img );
int	alloc_image_mode_set( int mode );
u_char	***alloc_image_data( u_int linebytes, int h, int nchan );
int	free_image_data( IMAGE *img );
int	image_nbit_decide( int data_type );
int	image_cpy( IMAGE *dst, IMAGE *src );
int	image_cut_data( IMAGE *img, int xs, int ys, int w, int h, int chanl, u_char **buf);
int	image_put_data( IMAGE *img, int xs, int ys, int w, int h, int chanl, u_char **buf);
u_char*	image_pixel( IMAGE *img, int x, int y, int chanl );
int	image_pixel_v(IMAGE *img, int x, int y, int chanl, double *v );
int	image_pixel_vset( IMAGE *img, int x, int y, int chanl, double v );
int	image_pixel_v2D( IMAGE *img, int x, int y, int chanl, int nx, int ny, double **v );
double * to_rgb( double * hsi);
double *to_hsi(double *rgb);
void initGeoRegistrationInfo( IMAGE_GEOREGISTRATION_INFO * geoRegistration );
void initAttr( IMAGE_ATTR * attr );

int	img_err_exit_set( int sw );
int	img_err_exit_set( int sw );
void	img_err_exit_push( void );
void	img_err_exit_pop( void );
void	img_err_exit( char *message, int code );

#endif /* !defined IMAGE_H */
