/*	
	imgio.c : image I/O handling routines

	Copyright(C)2011 HONDA Kiyoshi

  	Contact: Dr. HONDA Kiyoshi
			P.O.Box 4, Khlong Luang, Pathumthani, 12120, Thailand
			Asian Institute of Technology

			e-mail: honda@ait.ac.th
			http://www.rsgis.ait.ac.th/~honda

If you wish to use or distribute this software in a way
which does not follow the license stated below, you must obtain
written agreement from the copyright holder(s) above.

-------------------------------------------------------------------------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Program you will develop using this software must use the same
license with this software which is GNU General Public License
for any distribution.

The latest version of GNU General Public License may be obtained
from http://www.gnu.org/licenses

*/

/*

 *	Modification History						*
 *	10 Oct	2003	Geo-Registration Information Added to IMAGE
 *	29 June 2003	pixel spacing I/O for ENVI: dx, dy
 *	Upto 26 June 2003 A lot of Modification to include
 *	JPEG, TIFF
 *									*
 *	Jun 1996	Extention for external header files		*
 *				(EXHDR, ERM, ENVI) by H.Ishibashi	*
 *	Jun 1996	Extention for byte order definition by H.Ishibashi  *
 *	May 1996	Extention for gzip compress files by H.Ishibashi*
 *	May 1996	Extention for Win32 by H.Ishibashi		*
 *	May 1996	Remove old function definition by H.Ishibashi	*
 *	Sep 1995	Extention for generic format			*
 *	Mar 1995	Extention for compress files by H.Ishibashi	*
 *	Oct 1994	Extention for Windows by H.Ishibashi		*
 *	Jan 1994	Extention for MS-DOS by H.Ishibashi		*
 *									*/

// these 3 defines are for gcc
#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#define _LARGEFILE64_SOURCE

/*
	for Linux size of long is 64 in 64 bit system
	for VC++ size of long is still 32 in 64 bit system
*/

#include"image.h"

#if defined ( WINDOWS ) 
#include<stdio.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<io.h>
#include<stdlib.h>
#include<malloc.h>
#include<memory.h>
#include<string.h>
#include<errno.h>
#include<time.h>
#include<ctype.h>

#elif defined( LINUX )
//#warning imgio.c: COMPILING FOR LINUX
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/uio.h>
#include<unistd.h>
#include<memory.h>
#include<string.h>
#include<errno.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<ctype.h>

#endif

#include"imgio.h"

#if defined ( IMAGE_INCLD_TIFF )
#include"tiffio.h"
#endif

#if defined ( IMAGE_INCLD_JPEG )
#include"jpeglib.h"
typedef struct JPGINFO_TAG
{
	FILE	*jpegFp;
	int	jpegQuality;
	int	jpegLineBufNo;
	int	*jpegWriteLineInbuf;
	u_char	*jpegLineBuf;
	int	jpegCompressDir;

	struct	jpeg_compress_struct cinfo;
	struct	jpeg_decompress_struct dcinfo;
	JSAMPROW jpeg_row_pointer[1];	/* pointer to JSAMPLE row[s] */
	struct	jpeg_error_mgr jerr;
	struct jpegst	*jpeg;
} JPG;
#endif


#if defined ( IMAGE_INCLD_BMP )
#include"bmp.h"
#endif


#ifndef TRUE
#define TRUE -1
#define FALSE  !TRUE
#endif

//#define STRICT		0
//#define NOT_STRICT	1

# define MALLOC(p)	malloc((size_t)(p))
# define FREE(p)		free(p)
# define MEMCPY(p1,p2,c)	memcpy(p1,p2,(size_t)(c))
# define MEMCMP(p1,p2,c)	memcmp(p1,p2,(size_t)(c))

#if defined( LINUX )
#	define	STRICMP(a,b)		strcasecmp(a,b)
#	define	STRNICMP(a,b,c)		strncasecmp(a,b,c)
#	define	STRCPY(a,b)		strcpy(a,b)
#	define	STRNCPY(a,b,c)		strncpy(a,b,c)
#	define	STRCAT(a,b)		strcat(a,b)
#	define	ACCESS(a,b)		access(a,b)
#	define	OPEN(a,b)		open(a,b)
#	define	OPEN2(a,b,c)		open(a,b,c)
#	define	CLOSE(a)		close(a)
#	define IREAD(hd,buf,cnt)	read(hd,(char*)(buf),(size_t)(cnt))
#	define IWRITE(hd,buf,cnt)	write(hd,(char*)(buf),(size_t)(cnt))
#	define LSEEK(a,b,c)		lseek(a,b,c)
#	define SWAB(a,b,c)		swab(a,b,c)
#else
#	define	STRICMP(a,b)		_stricmp(a,b)
#	define	STRNICMP(a,b,c)		_strnicmp(a,b,c)
#	define	STRCPY(a,b)		strcpy(a,b)
#	define	STRNCPY(a,b,c)		strncpy(a,b,c)
#	define	STRCAT(a,b)		strcat(a,b)
#	define	ACCESS(a,b)		_access(a,b)
#	define	OPEN(a,b)		_open(a,b)
#	define	OPEN2(a,b,c)		_open(a,b,c)
#	define	CLOSE(a)		_close(a)
#	define IREAD(hd,buf,cnt)	_read(hd,(char*)(buf),(size_t)(cnt))
#	define IWRITE(hd,buf,cnt)	_write(hd,(char*)(buf),(size_t)(cnt))
#	define LSEEK(a,b,c)		_lseeki64(a,b,c)
#	define SWAB(a,b,c)		_swab(a,b,c)
#endif

/* static function definitions */

static int image_file_check( IMAGE_FILE *imgf );
static int create_header( IMAGE_FILE *imgf );
static int set_fix_header( IMAGE_FILE *imgf );
static int header_to_image( IMAGE_FILE *imgf );
static int read_header( IMAGE_FILE *imgf , char *header );
static int write_header( IMAGE_FILE *, char *header );
//static int read_descriptor( IMAGE_FILE *imgf, int i,  char *descriptor );
//static int write_descriptor( IMAGE_FILE *imgf, int i,  char *descriptor );
static int read_body( IMAGE_FILE *imgf );
static int write_body( IMAGE_FILE *imgf );
static int file_cutout_BSQ( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data );
static int file_cutout_BIL( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data );
static int file_putin_BSQ( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data );
static int file_putin_BIL( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data );
static int file_cutout_BIL( IMAGE_FILE *, int, int, int, int, int, u_char **);
static int file_putin_BSQ( IMAGE_FILE *, int, int, int, int, int, u_char **);
static int file_putin_BIL( IMAGE_FILE *, int, int, int, int, int, u_char **);
static off_t foffset( IMAGE_FILE *imgf, int x, int y, int chanl );
static const char * fbody( const char * str );
static int errmes( char *message, int erno );
static int errmesSys( char *message, int erno );
static int bftostr( char *dst, char *src, int n );
static int image_to_header( IMAGE_FILE *imgf );
static int strtobf( char *dst, char *src, int n );
static int byterev( char *src_buf, char *dst_buf, int buf_size, int nbyte );
#if defined(HAS_FORK)
static int rmZextension( char *fname );
#endif
static char *GetValue( char *buf );
static char *GetValue2( char *buf );
static char *rmSpace( char *buf );
static int default_setup_imgf( IMAGE_FILE *imgf );
static int msif_check( IMAGE_FILE *imgf );
static int ext_check( IMAGE_FILE *imgf, char *ext, int file_header, int (*external_header_to_image)( FILE *ext_fp, IMAGE_FILE *imgf ) );
static int ex_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf );
static int erm_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf );
static int envi_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf );
static off_t fsize( char *name );
static int write_ext_header( IMAGE_FILE *imgf );
static int write_erm_header( IMAGE_FILE *imgf );
static int write_envi_header( IMAGE_FILE *imgf );
static int removeExtention( char *fname, char *extention );
//static int removeExtention2( char *fname );
static int write_arcInfo_header( IMAGE_FILE *imgf );

#if defined ( IMAGE_INCLD_TIFF ) || defined (IMAGE_INCLD_JPEG )
static void bip2bndLine( u_char *bip, u_char *bndLine, int nchan, int ichan, int w, int nbit);
static void bndLine2bip( u_char *bip, u_char *bndLine, int nchan, int ichan, int w, int nbit);
#endif

#if defined ( IMAGE_INCLD_TIFF )
static TIFF	* tiffCreate( IMAGE_FILE  *imgf );
static TIFF	* tiffOpen( IMAGE_FILE  *imgf, int mode );
static TIFF	* tiffClose( IMAGE_FILE  *imgf );
/*static int haveTiffExtention ( char *fname );	*/
static int tiff_read_body( IMAGE_FILE *imgf );
static int tiff_write_body( IMAGE_FILE *imgf );
int tiff_image_file_read(  IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );
int tiff_image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );
#endif
#if defined ( IMAGE_INCLD_JPEG )
static int jpegCreate( IMAGE_FILE  *imgf );
static int jpegOpen( IMAGE_FILE  *imgf, int mode );
static int	jpegClose( IMAGE_FILE  *imgf );
/* static int haveJPEGExtention ( char *fname );	*/
static int jpeg_read_body( IMAGE_FILE *imgf );
static int jpeg_write_body( IMAGE_FILE *imgf );
int jpeg_image_file_read(  IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );
int jpeg_image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );
#endif

static int bmpOpen2( IMAGE_FILE  *imgf, int mode );
static int bmpClose2( IMAGE_FILE  *imgf );
static int bmp_read_body( IMAGE_FILE  *imgf );
int bmp_image_file_read(  IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );

static int default_interleave = IMAGE_BIL;
static int default_file_header = IMAGE_ENVI;

static int byte_order_this_machine = 
#if defined( LSB_1ST )
	IMAGE_LSB_1ST;
#elif defined( MSB_1ST )
	IMAGE_MSB_1ST;
#endif

static int filemode =
# if defined (WINDOWS)
	S_IREAD|S_IWRITE;
# elif defined(SUN370)
        0000440|0000222;
# else
	S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH;
# endif /* defined (DOS) */

static int UseCompress = 
# if defined(LINUX)
        TRUE;
# else
        FALSE;
# endif

#if defined( LINUX )
//# define HAS_FORK
#endif

#if defined ( IMAGE_INCLD_JPEG )
#define	JPEG_COMPRESS	1
#define	JPEG_DECOMPRESS	2
int jpeg_default_quality = 75;
#endif

//extern to imgio.h to utilize the string from applications
// String order is accordance with Constant in image.h
// The order must not be changed

char	*enviRegistrationString[] = 
{
	"Geographic Lat/Lon",
	"UTM",
	"Arbitrary",
	NULL
};
char	*enviUTMNorthSouthString[] = 
{
	"North",
	"South",
	NULL
};

char	*enviDatumString[] = 
{
	"WGS-84",
	"None",
	"TOKYO",
	NULL
};

char	*enviUTMUnitString[] = 
{
	"units=Degree",
	"units=Meters",
	NULL
};

/************************************************************
	Create image file
*************************************************************/
IMAGE_FILE *
image_file_create( char *fname, int mode, int ndesc, IMAGE *img )
{
	IMAGE_FILE	*imgf;
	int		flags;

	img_err_exit_push();

	if((
	imgf = (IMAGE_FILE *) MALLOC( sizeof( IMAGE_FILE ) )
		) == NULL ) {
		img_err_exit_pop(); img_err_exit( "image_file_create: malloc IMAGE_FILE error", 1 );
		return NULL;
	}
#if defined ( IMAGE_INCLD_JPEG )
	if(( imgf->jpg = malloc( sizeof( JPG ) )) == NULL ) {
		img_err_exit_pop();
		img_err_exit( "image_file_create: malloc JPGINFO_TAG error", 1 );
		free( imgf );
		return NULL;
	}
#endif

	default_setup_imgf( imgf );
	imgf->fhandl = -1;

	/* Set parameter	*/
	STRNCPY( imgf->fname, fbody(fname), IMAGE_FNAME_MAX );
	STRNCPY( imgf->fullPath, fname, IMAGE_FULLPATH_LENGTH );
	imgf->hdrFullPath[0] = 0;
	imgf->ndesc 	= ndesc;
	imgf->md_primary = FALSE;
	imgf->compressed = FALSE;
	imgf->new_case	= TRUE;
	imgf->mode	= IMAGE_RDWR;
	imgf->interleave	= default_interleave;
	imgf->byte_order= byte_order_this_machine;
	imgf->image	= img;
	imgf->w 	= img->w;
	imgf->h 	= img->h;
	imgf->geoRegistration = img->geoRegistration;
	imgf->attr = img->attr;
//	imgf->dx	= img->dx;
//	imgf->dy	= img->dy;
	imgf->nchan 	= img->nchan;
	imgf->data_type	= img->data_type;
	imgf->nbit	= img->nbit;
	imgf->size	= img->size;

	if( haveERMExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ERM;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ERM_EXTENTION );
	} else if( haveENVIExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ENVI;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ENVI_EXTENTION );
	} else if( haveARCINFOExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ARCINFO;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ARCINFO_EXTENTION );
	} else if( haveTiffExtention ( imgf->fullPath ) ){
#if defined ( IMAGE_INCLD_TIFF )
		imgf->file_header = IMAGE_TIFF;
#else
		errmes("image_file_create:does not have Tiff Component",0);
#endif
	} else if ( haveJPEGExtention( imgf->fullPath ) ){
#if defined ( IMAGE_INCLD_JPEG )
		imgf->file_header = IMAGE_JPEG;
#else
		errmes("image_file_create:does not have JPEG Component",0);
#endif
	} else if ( haveBMPExtention( imgf->fullPath ) ){
//#if defined ( IMAGE_INCLD_BMP )
//		imgf->file_header = IMAGE_BMP;
		errmes("image_file_create:BMP creation is not supported",0);
//#endif
	} else {
		imgf->file_header	= default_file_header;
		switch( imgf->file_header ){
			STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
			case IMAGE_ERM:
				STRCAT( imgf->hdrFullPath, ERM_EXTENTION );
				break;
			case IMAGE_ENVI:
				STRCAT( imgf->hdrFullPath, ENVI_EXTENTION );
				break;
			case IMAGE_ARCINFO:
				STRCAT( imgf->hdrFullPath, ARCINFO_EXTENTION );
				break;
			case IMAGE_TIFF:
				STRCAT( imgf->hdrFullPath, TIFF_EXTENTION );
				break;
			case IMAGE_JPEG:
				STRCAT( imgf->hdrFullPath, JPEG_EXTENTION );
				break;
			case IMAGE_BMP:
				STRCAT( imgf->hdrFullPath, BMP_EXTENTION );
				break;
			default:
				errmes("image_file_create:illegal default_file_header",default_file_header);
				break;
		}
	}
//	printf("image_file_open: %d %s %s\n", imgf->file_header, imgf->fullPath, imgf->hdrFullPath);

	if( imgf->file_header == IMAGE_ERM )
		imgf->interleave = IMAGE_BIL;
	else if ( imgf->file_header == IMAGE_TIFF )
		imgf->interleave = IMAGE_BIP;
	else if ( imgf->file_header == IMAGE_JPEG )
		imgf->interleave = IMAGE_BIP;
	else if ( imgf->file_header == IMAGE_ARC_INFO )
		imgf->interleave = IMAGE_BIL;
	else if ( imgf->file_header == IMAGE_BMP )
		imgf->interleave = IMAGE_BIP;

	if( img->data != NULL )
		imgf->buffering = TRUE;
	else
		imgf->buffering = FALSE;
	
	/*	Check File existance:	if exist error return	*/
	if( mode == IMAGE_EXCL &&
	(ACCESS( imgf->fullPath, 0 ) == 0 || ( strlen(imgf->hdrFullPath) > 0 && ACCESS( imgf->hdrFullPath, 0 ) == 0) )){
		errmes("image_file_create:file already exists",0);
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_create", 1 );
		return NULL;
	}
	
	/*	Create File for read/write	*/
	if(      mode == IMAGE_EXCL )
		flags = O_CREAT|O_RDWR|O_EXCL;
	else if( mode == IMAGE_TRUNC )
		flags = O_CREAT|O_RDWR|O_TRUNC;
	else{
		errmes("image_file_create:file create mode error",0);
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_create", 1 );
		return NULL;
	}

	switch( imgf->file_header )
	{
	case IMAGE_TIFF:
#if defined ( IMAGE_INCLD_TIFF )
		if (tiffCreate( imgf ) == NULL) {
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_create: tiffCreate error", 1 );
			return NULL;
		}
		break;
#else
		img_err_exit_pop(); img_err_exit( "image_file_create: tiff not supported", 1 );
			return NULL;
		break;
#endif
	case IMAGE_JPEG:
#if defined ( IMAGE_INCLD_JPEG )
		if (jpegCreate( imgf ) != 0) {
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_create: jpegCreate error", 1 );
			return NULL;
		}
		break;
#else
		img_err_exit_pop(); img_err_exit( "image_file_create: jpeg not supported", 1 );
			return NULL;
		break;
#endif
	case IMAGE_BMP:
		img_err_exit_pop(); img_err_exit( "image_file_create: BMP not supported", 1 );
			return NULL;
		break;
	default:
	    if((
#if defined(WINDOWS)
	imgf->fhandl = OPEN2( imgf->fullPath, flags|O_BINARY, filemode )
#else
	imgf->fhandl = OPEN2( imgf->fullPath, flags, filemode  )
#endif
		) == -1 ){
			errmesSys("image_file_create:file create error",errno);
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_create", 1 );
			return NULL;
	    }
		break;
	}

	/* set header length */
	image_file_header_set( imgf, imgf->file_header );
	switch( imgf->file_header ){
		case MSIF: /*	create header data	*/
			create_header( imgf );
			break;
		default:
			break;
	}
	
	img_err_exit_pop();
	return imgf;
}


/************************************************
	Open old image file
************************************************/
IMAGE_FILE *
image_file_open( const char *fname, int mode, int buffering )
{
	IMAGE_FILE *imgf;
	int oflag = O_RDONLY;
	int	opened;

	/* Allocate IMAGE_FILE structrue	*/

	img_err_exit_push();

	if((
	imgf = (IMAGE_FILE *) MALLOC( sizeof( IMAGE_FILE ) )
		) == NULL ){
		errmes("image_file_open:Not enough memory for IMAGE_FILE",0);
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
		return NULL;
	}
#if defined ( IMAGE_INCLD_JPEG )
	if(( imgf->jpg = malloc( sizeof( struct JPGINFO_TAG ) )) == NULL ) {
		img_err_exit_pop();
		img_err_exit( "image_file_create: malloc JPGINFO_TAG error", 1 );
		free( imgf );
		return NULL;
	}
#endif


	default_setup_imgf( imgf );
	imgf->fhandl = -1;

	/* Set IMAGE_FILE control block	*/
	STRNCPY( imgf->fname, fbody(fname), IMAGE_FNAME_MAX );
	STRNCPY( imgf->fullPath, fname, IMAGE_FULLPATH_LENGTH );
	imgf->hdrFullPath[0] = 0;
	imgf->new_case	= FALSE;
	imgf->mode = mode;
	imgf->buffering = FALSE;
	imgf->md_primary= FALSE;
	imgf->compressed= FALSE;

	if( haveERMExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ERM;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ERM_EXTENTION );
	} else if( haveENVIExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ENVI;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ENVI_EXTENTION );
	} else if( haveARCINFOExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_ARCINFO;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
		removeExtention( imgf->fullPath, ARCINFO_EXTENTION );
	} else if( haveTiffExtention ( imgf->fullPath ) ){
#if defined ( IMAGE_INCLD_TIFF )
		imgf->file_header = IMAGE_TIFF;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
#else
		errmes("image_file_opem:does not have Tiff Component",0);
#endif
	} else if ( haveJPEGExtention( imgf->fullPath ) ){
#if defined ( IMAGE_INCLD_JPEG )
		imgf->file_header = IMAGE_JPEG;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
#else
		errmes("image_file_open:does not have JPEG Component",0);
#endif
	} else if( haveBMPExtention ( imgf->fullPath ) ){
		imgf->file_header = IMAGE_BMP;
		STRNCPY( imgf->hdrFullPath, imgf->fullPath, IMAGE_FULLPATH_LENGTH );
	} else {
		imgf->file_header = IMAGE_UNKNOWN_FORMAT;
	}
	/*	Check File existance: if not exist error return	*/

#if defined(HAS_FORK)
   if( UseCompress == TRUE ){
//	( imgf->fname );
	rmZextension( imgf->fullPath );
	if(
	ACCESS( imgf->fullPath, 0 ) != 0 ){
		char zfname[256+3];
		char gzfname[256+3];
		STRNCPY( zfname, imgf->fullPath, 256 );
		STRNCPY( gzfname, imgf->fullPath, 256 );
		STRCAT( zfname, ".Z" );
		STRCAT( gzfname, ".gz" );

		if(
		ACCESS( zfname, 0 ) == 0 ){
			if( fork()  == 0 )
				execlp( "uncompress", "uncompress", zfname, (char*) 0 );
			else{
				fprintf(stderr, "now uncompressing %s\n",zfname);
				wait(NULL);
				fprintf(stderr, "done..\n");
			}
			imgf->compressed = COMPRESS;
		} else if(
		ACCESS( gzfname, 0 ) == 0 ){
			if( fork()  == 0 )
				execlp( "gunzip", "gunzip", gzfname, (char*) 0 );
			else{
				fprintf(stderr, "now uncompressing %s\n",gzfname);
				wait(NULL);
				fprintf(stderr, "done..\n");
			}
			imgf->compressed = GZIP;
		} else {
			fprintf(stderr, "image_file_open:file doesn't exist %s\n", imgf->fullPath);
			errmes("image_file_open:file doesn't exist",0);
			FREE( imgf );
			img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
		}
	}
 } else {
#endif /* defined(HAS_FORK) */

	if((
	imgf->fhandl = OPEN( imgf->fullPath, O_RDONLY )
			) == -1 ){
		fprintf(stderr, "image_file_open: file does not exist: %s\n", imgf->fullPath);
//		errmesSys("image_file_open:file doesn't exist(open check error)",errno);
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
		return NULL;
	} else {
		CLOSE( imgf->fhandl );
		imgf->fhandl = -1;
	}

#if defined(HAS_FORK)
  }
#endif /* defined(HAS_FORK) */

	/*	Open File 	*/
	if( mode == IMAGE_RDONLY )
		oflag = O_RDONLY;
	else if( mode == IMAGE_RDWR )
		oflag = O_RDWR;

#if defined(WINDOWS)
		oflag = oflag|O_BINARY;
#endif

	opened = FALSE;
#if defined ( IMAGE_INCLD_TIFF )
	if( ! opened && haveTiffExtention( imgf->fullPath ) ) {
		if( tiffOpen( imgf, mode ) == NULL ){
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open: tiffOpen", 1 );
			return NULL;
		}
		imgf->file_header = IMAGE_TIFF;
		opened = TRUE;
	}
#endif
#if defined ( IMAGE_INCLD_JPEG )
	if( ! opened && haveJPEGExtention( imgf->fullPath ) ) {
		if( jpegOpen( imgf, mode ) != 0 ){
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open: jpegOpen", 1 );
			return NULL;
		}
		imgf->file_header = IMAGE_JPEG;
		opened = TRUE;
	}
#endif
	if( ! opened && haveBMPExtention( imgf->fullPath ) ) {
#if defined ( IMAGE_INCLD_BMP )
		if( bmpOpen2( imgf, mode ) != 0 ){
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open: bmpOpen2", 1 );
			return NULL;
		}
		imgf->file_header = IMAGE_BMP;
		opened = TRUE;
#else
		img_err_exit_pop(); img_err_exit( "image_file_open: compile with BMP option", 1 );
#endif
	}
	if( ! opened ){
		if((
		imgf->fhandl = OPEN( imgf->fullPath, oflag )
			) == -1 ){
			errmesSys("image_file_open:file open error",errno);
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
		}
		switch( imgf->file_header ){
		case IMAGE_ERM:
			if( ext_check( imgf, ERM_EXTENTION, IMAGE_ERM, erm_header_to_image ) != 0 ){
				errmes("image_file_open:ERM illegal header",0);
				CLOSE( imgf->fhandl );
				FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
				return NULL;
			}
			opened = TRUE;
			break;
		case IMAGE_ENVI:
//			fprintf(stderr, "image_file_open: ext_check for ENVI\n");
			if( ext_check( imgf, ENVI_EXTENTION, IMAGE_ENVI, envi_header_to_image ) != 0 ){
				errmes("image_file_open:ENVI illegal header",0);
				CLOSE( imgf->fhandl );
				FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
				return NULL;
			}
//			printf("image_file_open: mimgf->file_header %d %d %d\n",imgf->file_header, imgf->w, imgf->h);
			opened = TRUE;
			break;
		case IMAGE_UNKNOWN_FORMAT:
			imgf->file_header = MSIF;
			if( msif_check( imgf ) != 0 ) {
//			printf("image_file_open: msif_check failed\n");
				imgf->file_header = EXHDR;
				if( ext_check( imgf, EXHDR_EXTENTION, EXHDR, ex_header_to_image ) != 0 ){
//					printf("image_file_open: ex_header_to_image failed\n");
					imgf->file_header = ERM;
					if( ext_check( imgf, ERM_EXTENTION, ERM, erm_header_to_image ) != 0 ){
//						printf("image_file_open: erm_header_to_image failed\n");
						imgf->file_header = ENVI;
						if( ext_check( imgf, ENVI_EXTENTION, ENVI, envi_header_to_image ) != 0 ){
							errmes("image_file_open:illegal header",0);
							CLOSE( imgf->fhandl );
							FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
							return NULL;
			}}}}
//			printf("image_file_open: mimgf->file_header %d %d %d\n",imgf->file_header, imgf->w, imgf->h);
			opened = TRUE;
			break;
		case IMAGE_ARCINFO:
		default:
			errmes("image_file_open:arcinfo open is not implemented",0);
			CLOSE( imgf->fhandl );
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
			break;
		}
	}

/*
	printf("image_file_open: allocate memory\n");
	printf("w,h,ncha = %d %d %d\n",imgf->w, imgf->h, imgf->nchan );
*/

	/*	allocate buffering memory	*/
	switch( buffering ){
	case IMAGE_NO_BUF:
		if((
		imgf->image = image_skelton(
			imgf->w,imgf->h,imgf->data_type, imgf->nchan )
			) == NULL ){
			errmes("image_file_open: image_skelton error", buffering);
			CLOSE( imgf->fhandl );
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
		}
		imgf->buffering = FALSE;
		imgf->md_primary = TRUE;
		break;
	case IMAGE_BUFFERED:
		if((
		imgf->image = image_alloc(
			imgf->w,imgf->h,imgf->data_type,imgf->nchan )
			) == NULL ){
			errmes("image_file_open: image_alloc_error", buffering);
			CLOSE( imgf->fhandl );
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
		}
		imgf->buffering = TRUE;
		imgf->md_primary = TRUE;
		break;
	default:
		errmes("image_file_open:illegal buffering mode", buffering);
		CLOSE( imgf->fhandl );
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
		return NULL;
	}
	if( imgf->buffering == TRUE ){
		if( read_body( imgf ) != 0 ){
			errmes("image_file_open:read body error",0);
			image_file_quit( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open", 1 );
			return NULL;
		}
	}
	imgf->image->geoRegistration = imgf->geoRegistration;
	imgf->image->attr = imgf->attr;
		img_err_exit_pop();
	return imgf;
}


IMAGE_FILE *
image_file_open_bin( char *fname, int mode, int buffering, int w, int h, int data_type, int nchan, int interleave )
{
	IMAGE_FILE *imgf;
	int oflag = O_RDONLY;
	int	opened;

	img_err_exit_push();

	if((
	imgf = (IMAGE_FILE *) MALLOC( sizeof( IMAGE_FILE ) )
		) == NULL ){
		errmes("image_file_open:Not enough memory for IMAGE_FILE",0);
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
		return NULL;
	}
#if defined ( IMAGE_INCLD_JPEG )
	if(( imgf->jpg = malloc( sizeof( struct JPGINFO_TAG ) )) == NULL ) {
		img_err_exit_pop();
		img_err_exit( "image_file_create: malloc JPGINFO_TAG error", 1 );
		free( imgf );
		return NULL;
	}
#endif


	default_setup_imgf( imgf );
	imgf->fhandl = -1;

	/* Set IMAGE_FILE control block	*/
	STRNCPY( imgf->fname, fbody(fname), IMAGE_FNAME_MAX );
	STRNCPY( imgf->fullPath, fname, IMAGE_FULLPATH_LENGTH );
	imgf->hdrFullPath[0] = 0;
	imgf->new_case	= FALSE;
	imgf->mode = mode;
	imgf->buffering = FALSE;
	imgf->md_primary= FALSE;
	imgf->compressed= FALSE;

	if((
	imgf->fhandl = OPEN( imgf->fullPath, O_RDONLY )
			) == -1 ){
		errmesSys("image_file_open:file doesn't exist(open check error)",errno);
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
		return NULL;
	} else {
		CLOSE( imgf->fhandl );
		imgf->fhandl = -1;
	}

	/*	Open File 	*/
	if( mode == IMAGE_RDONLY )
		oflag = O_RDONLY;
	else if( mode == IMAGE_RDWR )
		oflag = O_RDWR;

#if defined(WINDOWS)
		oflag = oflag|O_BINARY;
#endif

	opened = FALSE;

	if( ! opened ){
		if((
		imgf->fhandl = OPEN( imgf->fullPath, oflag )
			) == -1 ){
			errmesSys("image_file_open:file open error",errno);
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
			return NULL;
		}
		imgf->buffering = buffering;
		imgf->h = h;
		imgf->w = w;
		imgf->nchan = nchan;
		imgf->data_type = data_type;
		imgf->nbit      = image_nbit_decide( data_type );
		imgf->interleave = interleave;
		imgf->file_header = IMAGE_ENVI;
	}

	/*	allocate buffering memory	*/
	switch( buffering ){
	case IMAGE_NO_BUF:
		if((
		imgf->image = image_skelton(
			imgf->w,imgf->h,imgf->data_type, imgf->nchan )
			) == NULL ){
			errmes("image_file_open: image_skelton error", buffering);
			CLOSE( imgf->fhandl );
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
			return NULL;
		}
		imgf->buffering = FALSE;
		imgf->md_primary = TRUE;
		break;
	case IMAGE_BUFFERED:
		if((
		imgf->image = image_alloc(
			imgf->w,imgf->h,imgf->data_type,imgf->nchan )
			) == NULL ){
			errmes("image_file_open: image_alloc_error", buffering);
			CLOSE( imgf->fhandl );
			FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
			return NULL;
		}
		imgf->buffering = TRUE;
		imgf->md_primary = TRUE;
		break;
	default:
		errmes("image_file_open:illegal buffering mode", buffering);
		CLOSE( imgf->fhandl );
		FREE( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
		return NULL;
	}
	if( imgf->buffering == TRUE ){
		if( read_body( imgf ) != 0 ){
			errmes("image_file_open:read body error",0);
			image_file_quit( imgf );
		img_err_exit_pop(); img_err_exit( "image_file_open_bin", 1 );
			return NULL;
		}
	}
	imgf->image->geoRegistration = imgf->geoRegistration;
	imgf->image->attr = imgf->attr;

		img_err_exit_pop();
	return imgf;
} 
/************************************************
	Close image file
	Retained all data is output to file
*************************************************/
int
image_file_close( IMAGE_FILE *imgf )
{
	int irtn;
	
	img_err_exit_push();
//	fprintf(stderr, "image_file_close: mode = %d %d\n", imgf->mode, IMAGE_RDWR );
	if( imgf->mode == IMAGE_RDWR ) {
		irtn = image_file_save( imgf );
		if( irtn != 0) {
			img_err_exit_pop(); img_err_exit( "image_file_close", 1 );
		}
	} else {
		irtn = 0;
	}
		

	img_err_exit_pop();

	image_file_quit( imgf );
	return irtn;
}


/************************************************
	Quit image
*************************************************/
void
image_file_quit( IMAGE_FILE *imgf )
{
	int	closed;

	closed = FALSE;
#if defined ( IMAGE_INCLD_TIFF )
	if( ! closed && imgf->tiff != NULL ){
		(void) TIFFClose( imgf->tiff );
		if ( imgf->tiffLineBuf )
			_TIFFfree( imgf->tiffLineBuf );
		free( imgf->written );
		imgf->tiff = NULL;
		imgf->tiffLineBuf = NULL;
		imgf->written = NULL;
		closed = TRUE;
	}
#endif
#if defined ( IMAGE_INCLD_JPEG )
	if( ! closed && ((JPG*)imgf->jpg)->jpegFp != NULL ){
//		fprintf(stderr, "image_file_quit: calling jpegClose\n");
		jpegClose( imgf );
//		fprintf(stderr, "image_file_quit: returned from jpegClose\n");
		closed = TRUE;
	}
#endif
#if defined ( IMAGE_INCLD_BMP )
	if( ! closed && imgf->bmp != NULL ){
//		fprintf(stderr, "image_file_quit: calling bmpClose2\n");
		bmpClose2( imgf );
//		fprintf(stderr, "image_file_quit: returned from jpegClose\n");
		closed = TRUE;
	}
#endif

	if( ! closed ) {
		CLOSE( imgf->fhandl );
		imgf->fhandl = -1;
	}

	if( imgf->md_primary == TRUE )
		image_destroy( imgf->image );

   if( UseCompress == TRUE ){

	char cmd[16];

	if(imgf->compressed != FALSE ){
		switch (imgf->compressed){
		case GZIP:
			STRCPY( cmd, "gzip");
			break;
		case COMPRESS:
			STRCPY( cmd, "compress");
			break;
		default:
			STRCPY( cmd, "oops");
		}

#if defined(HAS_FORK)
		if( fork()  == 0 )
			execlp( cmd, cmd, imgf->fullPath, (char*) 0 );
		else{
			fprintf(stderr, "now compressing %s\n", imgf->fname);
			wait(NULL);
			fprintf(stderr, "done..\n");
		}
#endif /* defined(HAS_FORK) */
		}
	}
	FREE( imgf );
}


/************************************************
	Save data to file
*************************************************/
int
image_file_save( IMAGE_FILE *imgf )
{
	time_t	clock;
	struct tm *stm;

	if( imgf->mode == IMAGE_RDONLY )
		return 0;

	time(&clock); stm = localtime( &clock );

	switch( imgf->file_header ){
		case IMAGE_MSIF:
			sprintf(imgf->header.mmddyy,"%2.2d%2.2d%2.2d",
				stm->tm_mon+1,stm->tm_mday,stm->tm_year);
			image_to_header( imgf );

			if( write_header( imgf, (char*)&imgf->physical_header ) != 0 )
				return -1;
			break;
		case IMAGE_EXHDR:
			write_ext_header( imgf );
			break;
		case IMAGE_ERM:
			write_erm_header( imgf );
			break;
		case IMAGE_ENVI:
			write_envi_header( imgf );
			break;
		case ALLEXHDR:
			write_ext_header( imgf );
			write_erm_header( imgf );
			write_envi_header( imgf );
			write_arcInfo_header( imgf );
			break;
		case IMAGE_TIFF:
		case IMAGE_JPEG:
			break;
		case IMAGE_ARC_INFO:
			write_arcInfo_header( imgf );
			break;
		default:
			return -1;
	}

	if( imgf->buffering == TRUE )
		if( write_body( imgf ) != 0 )
			return -1;
	imgf->new_case = FALSE;
	return 0;
}


/************************************************
	Read image
*************************************************/
int
image_file_read( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	if( imgf->buffering == FALSE ){
#if defined ( IMAGE_INCLD_TIFF )
		if( imgf->tiff != NULL ){
			return tiff_image_file_read(imgf,xs,ys,w,h,chanl,buf);
		}
#endif
#if defined ( IMAGE_INCLD_JPEG )
		if( imgf->file_header == IMAGE_JPEG ){
			return jpeg_image_file_read(imgf,xs,ys,w,h,chanl,buf);
		}
#endif
		if( imgf->interleave == IMAGE_BIL )
			return file_cutout_BIL(imgf,xs,ys,w,h,chanl,buf);
		else
			return file_cutout_BSQ(imgf,xs,ys,w,h,chanl,buf);
	} else {
		return image_cut_data(imgf->image,xs,ys,w,h,chanl,buf);
	}
}


/************************************************
	Write image
*************************************************/
int
image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	irtn;

	if( imgf->buffering == FALSE ) {
#if defined ( IMAGE_INCLD_TIFF )
		if( imgf->tiff != NULL ){
			irtn = tiff_image_file_write(imgf,xs,ys,w,h,chanl,buf);
			if( irtn != 0) {
				img_err_exit( "image_file_write: tiff_image_file_write", 1 );
			}
			return irtn;
		} 
#endif
#if defined ( IMAGE_INCLD_JPEG )
		if( imgf->file_header == IMAGE_JPEG ){
			irtn = jpeg_image_file_write(imgf,xs,ys,w,h,chanl,buf);
			if( irtn != 0) {
				img_err_exit( "image_file_write: jpeg_image_file_write", 1 );
			}
			return irtn;
		}
#endif
		if( imgf->interleave == IMAGE_BIL ) {
			irtn = file_putin_BIL(imgf,xs,ys,w,h,chanl,buf);
			if( irtn != 0) {
				img_err_exit( "image_file_write:file_putin_BIL", 1 );
			}
			return irtn;
		} else {
			irtn = file_putin_BSQ(imgf,xs,ys,w,h,chanl,buf);
			if( irtn != 0) {
				img_err_exit( "image_file_write:file_putin_BSQ", 1 );
			}
			return irtn;
		}
	} else {
		irtn = image_put_data(imgf->image,xs,ys,w,h,chanl,buf);
			if( irtn != 0) {
				img_err_exit( "image_file_write:image_put_data", 1 );
			}
			return irtn;
	}
	return irtn;

}


/************************************************
	Set IMAGE addres to IMAGE_FILE
*************************************************/
int
image_file_image_set( IMAGE_FILE *imgf, IMAGE *img )
{
	if( imgf->buffering == TRUE && imgf->mode == IMAGE_RDWR ){
		if( image_file_save(imgf) != 0 ) return -1;
	}

	if( imgf->md_primary == TRUE )
		image_destroy( imgf->image );

	imgf->image = img;
	imgf->md_primary = FALSE;
	if( img->data == NULL ){
		imgf->buffering = FALSE;
	}else{
		imgf->buffering = TRUE;
		if( imgf->new_case == FALSE )
			if( read_body( imgf ) != 0 ) return -2;
	}
	return 0;
}


/************************************************
	Get pixel value
*************************************************/
u_char *
image_file_get_pixel( IMAGE_FILE *imgf, int x, int y, int chanl )
{
	static u_char buf[MAX_PIXEL_LENGTH];
	u_char	*pt;

	pt = buf;
	if( image_file_read( imgf, x, y, 1, 1, chanl, &pt ) != 0 )
		return NULL;
	return buf;
}


/************************************************
	Put pixel value
************************************************/
int
image_file_put_pixel( IMAGE_FILE *imgf, int x, int y, int chanl, u_char *buf )
{
	if( image_file_write( imgf, x, y, 1, 1, chanl, &buf ) != 0 )
		return -1;
	return 0;
}


/************************************************
	set header file name
*************************************************/
int
image_file_set_fname( IMAGE_FILE *imgf, char *fname )
{
	STRNCPY( imgf->header.dname, fbody(fname), 12 );
	return 0;
}


/************************************************
	get header comment			
*************************************************/
int
image_file_get_comment( IMAGE_FILE *imgf, char *comment )
{
	STRCPY( comment, imgf->header.comment );
	return 0;
}


/************************************************
	set header comment
*************************************************/
int
image_file_set_comment( IMAGE_FILE *imgf, char *comment )
{
	STRNCPY( imgf->header.comment, comment, 60 );
	return 0;
}


/************************************************
	Compression Switch
*************************************************/
void
CompressSwitch( int mode )
{
	UseCompress = mode;
}


/************************************************
	Default Image File Header Set
*************************************************/
void
image_file_default_header_set( int mode )
{
	default_file_header = mode;
}


/************************************************
	Default Image File Interleave Set
*************************************************/
void
image_file_default_interleave_set( int mode )
{
	default_interleave = mode;
}

/************************************************
	Image File Header Set
*************************************************/
void
image_file_header_set( IMAGE_FILE *imgf, int mode )
{
	imgf->file_header = mode;
	if( mode != MSIF ){
		imgf->header_length = IMAGE_HEADER_LENGTH * (    imgf->ndesc );
	} else {
		imgf->header_length = IMAGE_HEADER_LENGTH * (1 + imgf->ndesc);
	}
}


/************************************************
	Image File Interleave Set
*************************************************/
void
image_file_interleave_set( IMAGE_FILE *imgf, int mode )
{
	imgf->interleave = mode;
}

/************************************************
	STATIC FUNCTIONS BELOW
*************************************************/

static int
image_file_check( IMAGE_FILE *imgf )
{
	int	w,h,nchan,version,data_type,nbit,ndesc;
	short	u1;
	union PHYSICAL_HEADER *phd = &imgf->physical_header;


	static struct stat buf;
	if( fstat( imgf->fhandl, &buf ) )
		return -1;

	if( memcmp(&phd->ch[0xd6*2],"LSB",3) == 0 ) /* Case of Big Endian */
		imgf->byte_order = IMAGE_LSB_1ST;
	else if( memcmp(&phd->ch[0xd6*2],"MSB",3) == 0 ||
		memcmp(&phd->ch[0xd6*2],"SUN",3) == 0 ||
		memcmp(&phd->ch[0xd6*2],"MAC",3) == 0 ||
		memcmp(&phd->ch[0xd6*2],"DOS",3) == 0 ||
		memcmp(&phd->ch[0xd6*2],"MX",2) == 0 ) /* Case of Little Endian */
			imgf->byte_order = IMAGE_MSB_1ST;
	else
			imgf->byte_order = IMAGE_MSB_1ST;
/*
		default is IMAGE_MSB_1ST
		return -1;
*/

//	printf("image_file_check: nbit %d %d\n", imgf->byte_order, byte_order_this_machine);
	if( imgf->byte_order != byte_order_this_machine )
		imgf->need_reverse = TRUE;
	else
		imgf->need_reverse = FALSE;
/*
{

short	tmp;
SWAB( (char*)&phd->us[0x06], (char*)&tmp, 2);
printf("%u\n",tmp );
SWAB( (char*)&phd->us[0x07], (char*)&tmp, 2);
printf("%u\n",tmp );
}
*/

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x06], (char*)&u1, 2);	w = u1;
	SWAB( (char*)&phd->us[0x07], (char*)&u1, 2);	h = u1;
	SWAB( (char*)&phd->us[0x2b], (char*)&u1, 2);	nchan = u1;
	SWAB( (char*)&phd->us[0x68], (char*)&u1, 2);	version = u1;
	SWAB( (char*)&phd->us[0x3d], (char*)&u1, 2);	ndesc = u1;
	nbit	= phd->ch[0x0c*2  ];
} else {
	w 	= phd->us[0x06];
	h 	= phd->us[0x07];
	nchan	= phd->us[0x2b    ];
	version	= phd->us[0x68];
	ndesc	= phd->us[0x3d    ];
	nbit	= phd->ch[0x0c*2  ];
}
ndesc=0;
// printf("image_file_check: nbit %d %d %d %d\n", nbit, w, h, nchan);


	if( version == 0 ){
		if( ( long long ) buf.st_size != image_size(nbit,w,h,nchan) +IMAGE_HEADER_LENGTH*(1+ndesc) )
			return -1;
	} else { /* version != 0 */
		if( imgf->need_reverse == TRUE ) {
			SWAB((char*)&phd->us[0xc2], (char*)&u1, 2);data_type = u1;
		} else {
			data_type = phd->us[0xc2    ];
		}

		switch( nbit ){
			case	8:
				data_type	= IMAGE_CHAR;
				break;
			case	16:
				data_type	= IMAGE_USHORT;
				break;
			case	32:
				data_type	= IMAGE_ULONG;
				break;
			case	64:
				data_type	= IMAGE_DOUBLE;
				break;
				default:
				return -1;
				/* break; */
		}

		nbit = image_nbit_decide( data_type );
//		printf("image_file_check: %d %d %d %d\n",w,h,nchan,nbit);

		if( ( unsigned long ) buf.st_size !=
			image_size(nbit,w,h,nchan) + IMAGE_HEADER_LENGTH*(1+ndesc ) ){
//		printf("image_file_check: buf.st_size =%d %d %d\n",buf.st_size,image_size(nbit,w,h,nchan) + IMAGE_HEADER_LENGTH*(1+ndesc ), ndesc );
			return -1;
	}
	}
	return 0;
}


/************************************************
	Create header
*************************************************/
static int
create_header( IMAGE_FILE *imgf )
{
	int i;
	time_t	clock;
	struct tm *stm;
	
	IMAGE_HEADER	*hd = &imgf->header;

	for(i=0;i<IMAGE_HEADER_LENGTH;i++)
		imgf->physical_header.ch[i]=0;

	set_fix_header( imgf );
	
	hd->dname[12] = 0;
	STRNCPY( hd->dname, imgf->fname, 12 );
	hd->ebit 	=imgf->nbit;
	hd->lr		= 0;
	hd->scale	= 0;
	hd->podr = hd->sodr	= 0;
	hd->ml = hd->mr	= hd->mu = hd->md =0;

	switch ( imgf->byte_order ){
		case IMAGE_LSB_1ST:
			STRCPY( hd->device, "LSB" );
			break;
		case IMAGE_MSB_1ST:
			STRCPY( hd->device, "MSB" );
			break;
		default:
			return -1;
	}

/*
#if defined(DOS)
	STRCPY( hd->device, "DOS" );
#elif defined(SUN)
	STRCPY( hd->device, "SUN" );
#elif defined(MAC)
	STRCPY( hd->device, "MAC" );
#else
	STRCPY( hd->device, "MX"  );
#endif
*/
	hd->method[0]=0;
	hd->vx = imgf->w;
	hd->vy = imgf->h;
	hd->rxs = hd->rys =	1;
	hd->rxe = hd->vx;
	hd->rye = hd->vy;
	hd->fxs = hd->fys = 1;
	time(&clock); stm = localtime( &clock );
	sprintf(hd->mmddyy,"%2.2d%2.2d%2.2d",
		stm->tm_mon+1,stm->tm_mday,stm->tm_year);
	STRCPY(hd->place,"MIEUNV");
	hd->sampldx=hd->sampldy=1;
	hd->mask_level=0xffff;
	for(i=0;i<8;i++)
		hd->channel_id[i][0]=0;
	hd->dt1=hd->dt2=1;
	hd->mode=0;
	hd->nsamppx=hd->nsamppy=hd->nsamplx=hd->nsamply=1;
	hd->comment[0]=0;
	for(i=0;i<15;i++){
		hd->bit_plane[i].comment[0]=0;
		hd->bit_plane[i].maxv=0;
		hd->bit_plane[i].minv=0;
	}
	return 0;
}


/*************************************************
	Set Fix data to Headder
*************************************************/
static int
set_fix_header( IMAGE_FILE *imgf )
{
	
	IMAGE_HEADER	*hd = &imgf->header;

	hd->fx 	= imgf->w;
	hd->fy	= imgf->h;
	hd->sx	= imgf->w;
	hd->sy	= 1;
	hd->nsx = 1;
	hd->nsy = imgf->h;
	hd->nbit = imgf->nbit;
	hd->lleng	= hd->fx;
	hd->pleng	= 16384;
	hd->nchan=imgf->nchan;
	hd->ndesc=imgf->ndesc;
	hd->version=1;
	hd->data_type = imgf->data_type;
	hd->file_spec = imgf->interleave;
	hd->byte_order = imgf->byte_order;
	return 0;
}


/*************************************************
	Headder to image block
*************************************************/
static int
header_to_image( IMAGE_FILE *imgf )
{
	union PHYSICAL_HEADER *phd = &imgf->physical_header;
	IMAGE_HEADER	*hd = &imgf->header;
	int i;
	short	u1;
	short tmp;	/* for swab read */
/*	char	ctmp[16];	*/

	bftostr(hd->dname, (char*)&phd->ch[0x00], 12 );

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x06], (char*)&u1, 2 );	hd->fx = u1;
	SWAB( (char*)&phd->us[0x07], (char*)&u1, 2 );	hd->fy = u1;
	SWAB( (char*)&phd->us[0x08], (char*)&u1, 2 );	hd->sx = u1;
	SWAB( (char*)&phd->us[0x09], (char*)&u1, 2 );	hd->sy = u1;
	SWAB( (char*)&phd->us[0x0a], (char*)&u1, 2 );	hd->nsx = u1;
	SWAB( (char*)&phd->us[0x0b], (char*)&u1, 2 );	hd->nsy = u1;
} else {
	hd->fx		=phd->us[0x06];
	hd->fy		=phd->us[0x07];
	hd->sx		=phd->us[0x08];
	hd->sy		=phd->us[0x09];
	hd->nsx		=phd->us[0x0a];
	hd->nsy		=phd->us[0x0b];
}

	hd->nbit	=phd->ch[0x0c*2  ];
	hd->ebit	=phd->ch[0x0c*2+1];
	hd->lr		=phd->ch[0x0d*2  ];
	hd->scale	=phd->ch[0x0d*2+1];
	hd->lleng	=phd->lg[0x0e/2  ];

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x10], (char*)&u1, 2 );	hd->pleng = u1;
} else {
	hd->pleng	=phd->us[0x10    ];
}

	hd->podr	=phd->ch[0x11*2  ];
	hd->sodr	=phd->ch[0x11*2+1];
	hd->ml		=phd->ch[0x12*2  ];
	hd->mu		=phd->ch[0x13*2+1];
	hd->md		=phd->ch[0x13*2  ];
	hd->mr		=phd->ch[0x12*2+1];
	bftostr(hd->device,(char*)&phd->ch[0x14*2],6);
	bftostr(hd->method,(char*)&phd->ch[0x17*2],6);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x1a], (char*)&u1, 2 );	hd->vx = u1;
	SWAB( (char*)&phd->us[0x1b], (char*)&u1, 2 );	hd->vy = u1;
	SWAB( (char*)&phd->us[0x1c], (char*)&u1, 2 );	hd->rxs = u1;
	SWAB( (char*)&phd->us[0x1d], (char*)&u1, 2 );	hd->rys = u1;
	SWAB( (char*)&phd->us[0x1e], (char*)&u1, 2 );	hd->rxe = u1;
	SWAB( (char*)&phd->us[0x1f], (char*)&u1, 2 );	hd->rye = u1;
	SWAB( (char*)&phd->us[0x20], (char*)&u1, 2 );	hd->fxs = u1;
	SWAB( (char*)&phd->us[0x21], (char*)&u1, 2 );	hd->fys = u1;
} else {
	hd->vx		=phd->us[0x1a    ];
	hd->vy		=phd->us[0x1b    ];
	hd->rxs		=phd->us[0x1c    ];
	hd->rys		=phd->us[0x1d    ];
	hd->rxe		=phd->us[0x1e    ];
	hd->rye		=phd->us[0x1f    ];
	hd->fxs		=phd->us[0x20    ];
	hd->fys		=phd->us[0x21    ];
}
	bftostr(hd->mmddyy,(char*)&phd->ch[0x22*2],6);
	bftostr(hd->place, (char*)&phd->ch[0x25*2],6);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x28], (char*)&u1, 2 );	hd->sampldx = u1;
	SWAB( (char*)&phd->us[0x29], (char*)&u1, 2 );	hd->sampldy = u1;
	SWAB( (char*)&phd->us[0x2a], (char*)&u1, 2 );	hd->mask_level = u1;
	SWAB( (char*)&phd->us[0x2b], (char*)&u1, 2 );	hd->nchan = u1;
} else {
	hd->sampldx = phd->us[0x28    ];
	hd->sampldy = phd->us[0x29    ];
	hd->sampldy = phd->us[0x29    ];
	hd->mask_level	= phd->us[0x2a    ];
	hd->nchan	= phd->us[0x2b    ];
}
	for(i=0;i<8;i++){
		bftostr(hd->channel_id[i],(char*)&phd->us[0x2c+2*i],4);
	}

	hd->dt1		= phd->ch[0x3c*2  ];
	hd->dt2		= phd->ch[0x3c*2+1];

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x3d], (char*)&u1, 2 );	hd->ndesc = u1;
	SWAB( (char*)&phd->us[0x3e], (char*)&u1, 2 );	hd->mode = u1;
	SWAB( (char*)&phd->us[0x3f], (char*)&u1, 2 );	hd->nsamppx = u1;
	SWAB( (char*)&phd->us[0x40], (char*)&u1, 2 );	hd->nsamppy = u1;
	SWAB( (char*)&phd->us[0x41], (char*)&u1, 2 );	hd->nsamplx = u1;
	SWAB( (char*)&phd->us[0x42], (char*)&u1, 2 );	hd->nsamply = u1;
} else {
	hd->ndesc	= phd->us[0x3d    ];
	hd->mode	= phd->us[0x3e    ];
	hd->nsamppx	= phd->us[0x3f    ];
	hd->nsamppy	= phd->us[0x40    ];
	hd->nsamplx	= phd->us[0x41    ];
	hd->nsamply	= phd->us[0x42    ];
}

	bftostr(hd->comment,(char*)&phd->ch[0x43*2],60);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&phd->us[0x68], (char*)&u1, 2 );	hd->version = u1;
} else {
	hd->version	= phd->us[0x68];
}

	for(i=0;i<15;i++){
		bftostr(hd->bit_plane[i].comment,(char*)&phd->ch[0x6a*2+i*20],16);

if( imgf->need_reverse == TRUE ){
		tmp = phd->us[0x6a+16+i*20];
		SWAB( (char*)&tmp, (char*)&u1, 2);	hd->bit_plane[i].maxv = u1;
		tmp = phd->us[0x6a+18+i*20];
		SWAB( (char*)&tmp, (char*)&u1, 2);	hd->bit_plane[i].minv = u1;
} else {
		hd->bit_plane[i].maxv=phd->us[0x6a+16+i*20];
		hd->bit_plane[i].minv=phd->us[0x6a+18+i*20];
}
	}
	if( hd->version == 0 ){
		hd->file_spec	= IMAGE_BIL;
		imgf->byte_order = IMAGE_MX;
		switch( hd->nbit ){
		case	8:
			hd->data_type	= IMAGE_CHAR;
			break;
		case	16:
			hd->data_type	= IMAGE_USHORT;
			break;
		case	32:
			hd->data_type	= IMAGE_ULONG;
			break;
		default:
			return -1;
			/* break; */
		}
	} else {
if( imgf->need_reverse == TRUE ){
		SWAB( (char*)&phd->ch[0xc2*2  ], (char*)&u1, 2 );	hd->data_type = u1;
} else {
		hd->data_type = phd->us[0xc2    ];
}

		if( memcmp(&phd->ch[0xcc*2],"BSQ",3) == 0 )
			hd->file_spec = IMAGE_BSQ;
		else
			hd->file_spec = IMAGE_BIL;


		if(     memcmp(&phd->ch[0xd6*2 ],"MSB",3) == 0 ||
			memcmp(&phd->ch[0xd6*2 ],"SUN",3) == 0 ||
			memcmp(&phd->ch[0xd6*2 ],"DOS",3) == 0 ||
			memcmp(&phd->ch[0xd6*2 ],"MAC",3) == 0 ||
			memcmp(&phd->ch[0xd6*2 ],"MX",2) == 0 )
			imgf->byte_order = IMAGE_MSB_1ST;
		else if ( memcmp(&phd->ch[0xd6*2 ],"LSB",3) == 0 )
			imgf->byte_order = IMAGE_LSB_1ST;
		else {
			imgf->byte_order = IMAGE_MSB_1ST;
			switch( hd->nbit ){
			case	8:
				hd->data_type	= IMAGE_CHAR;
				break;
			case	16:
				hd->data_type	= IMAGE_USHORT;
				break;
			case	32:
				hd->data_type	= IMAGE_ULONG;
				break;
			default:
				return -1;
				break;
			}
		}
	}

	imgf->w		= hd->fx;
	imgf->h		= hd->fy;
	imgf->nchan     = hd->nchan;
	imgf->data_type = hd->data_type;
	imgf->nbit      = hd->nbit; 
	imgf->ndesc     = hd->ndesc;
/*	printf("header_to_image: imgf->ndesc= %d\n", imgf->ndesc ); */
	imgf->interleave = hd->file_spec;
	imgf->byte_order= hd->byte_order;
	imgf->header_length = IMAGE_HEADER_LENGTH * (1 + imgf->ndesc );
	imgf->file_header = MSIF;
	return 0;
}

static int
strtobf( char *dst, char *src, int n )
{
	while( *src && n>0 ){
		*dst=*src;
		dst++;
		src++;
		n--;
	}
	while( n>0 ){
		*dst=' ';
		dst++;
		n--;
	}
	return 0;
}


static int
bftostr( char *dst, char *src, int n )
{
	memcpy(dst,src,n);
	*(dst+n)='\0';
	while( *(dst+n-1) == ' ' && n > 0 ){
		*(dst+n-1)='\0';
		n--;
	}
	return 0;
}


/************************************************
	image to Headder
*************************************************/
static int
image_to_header( IMAGE_FILE *imgf )
{
	union PHYSICAL_HEADER *phd = &imgf->physical_header;
	IMAGE_HEADER	*hd = &imgf->header;
	int i;

set_fix_header( imgf );

strtobf((char*)&phd->ch[0x00], hd->dname, 12 );

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->fx, (char*)&phd->us[0x06], 2 );
	SWAB( (char*)&hd->fy, (char*)&phd->us[0x07], 2 );
	SWAB( (char*)&hd->sx, (char*)&phd->us[0x08], 2 );
	SWAB( (char*)&hd->sy, (char*)&phd->us[0x09], 2 );
	SWAB( (char*)&hd->nsx,(char*)&phd->us[0x0a], 2 );
	SWAB( (char*)&hd->nsy,(char*)&phd->us[0x0b], 2 );
} else {
	phd->us[0x06    ]	= hd->fx;
	phd->us[0x07    ]	= hd->fy;
	phd->us[0x08    ]	= hd->sx;
	phd->us[0x09    ]	= hd->sy;
	phd->us[0x0a    ]	= hd->nsx;
	phd->us[0x0b    ]	= hd->nsy;
}
	phd->ch[0x0c*2  ]	= hd->nbit;
	phd->ch[0x0c*2+1]	= hd->ebit;
	phd->ch[0x0d*2  ]	= hd->lr;
	phd->ch[0x0d*2+1]	= hd->scale;
	phd->lg[0x0e/2  ]	= hd->lleng;
if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->pleng, (char*)&phd->us[0x10], 2 );
} else {
	phd->us[0x10    ]	= hd->pleng;
}
	phd->ch[0x11*2  ]	= hd->podr;
	phd->ch[0x11*2+1]	= hd->sodr;
	phd->ch[0x12*2  ]	= hd->ml;
	phd->ch[0x13*2+1]	= hd->mu;
	phd->ch[0x13*2  ]	= hd->md;
	phd->ch[0x12*2+1]	= hd->mr;
	strtobf((char*)&phd->ch[0x14*2],hd->device,6);
	strtobf((char*)&phd->ch[0x17*2],hd->method,6);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->vx, (char*)&phd->us[0x1a], 2 );
	SWAB( (char*)&hd->vy, (char*)&phd->us[0x1b], 2 );
	SWAB( (char*)&hd->rxs,(char*)&phd->us[0x1c], 2 );
	SWAB( (char*)&hd->rys,(char*)&phd->us[0x1d], 2 );
	SWAB( (char*)&hd->rxe,(char*)&phd->us[0x1e], 2 );
	SWAB( (char*)&hd->rye,(char*)&phd->us[0x1f], 2 );
	SWAB( (char*)&hd->fxs,(char*)&phd->us[0x20], 2 );
	SWAB( (char*)&hd->fys,(char*)&phd->us[0x21], 2 );
} else {
	phd->us[0x1a    ]	= hd->vx;
	phd->us[0x1b    ]	= hd->vy;
	phd->us[0x1c    ]	= hd->rxs;
	phd->us[0x1d    ]	= hd->rys;
	phd->us[0x1e    ]	= hd->rxe;
	phd->us[0x1f    ]	= hd->rye;
	phd->us[0x20    ]	= hd->fxs;
	phd->us[0x21    ]	= hd->fys;
}
	strtobf((char*)&phd->ch[0x22*2],hd->mmddyy,6);
	strtobf((char*)&phd->ch[0x25*2],hd->place,6);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->sampldx,(char*)&phd->us[0x28], 2 );
	SWAB( (char*)&hd->sampldy,(char*)&phd->us[0x29], 2 );
	SWAB( (char*)&hd->mask_level,(char*)&phd->us[0x2a], 2 );
	SWAB( (char*)&hd->nchan,(char*)&phd->us[0x2b], 2 );
} else {
	phd->us[0x28    ]	= hd->sampldx;
	phd->us[0x29    ]	= hd->sampldy;
	phd->us[0x2a    ]	= hd->mask_level;
	phd->us[0x2b    ]	= hd->nchan;
}

	for(i=0;i<8;i++)
		strtobf((char*)&phd->us[0x2c+2*i],hd->channel_id[i],4);
	phd->ch[0x3c*2  ]	= hd->dt1;
	phd->ch[0x3c*2+1]	= hd->dt2;

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->ndesc,(char*)&phd->us[0x3d], 2 );
	SWAB( (char*)&hd->mode,(char*)&phd->us[0x3e], 2 );
	SWAB( (char*)&hd->nsamppx,(char*)&phd->us[0x3f], 2 );
	SWAB( (char*)&hd->nsamppy,(char*)&phd->us[0x40], 2 );
	SWAB( (char*)&hd->nsamplx,(char*)&phd->us[0x41], 2 );
	SWAB( (char*)&hd->nsamply,(char*)&phd->us[0x42], 2 );
} else {
	phd->us[0x3d    ]	= hd->ndesc;
	phd->us[0x3e    ]	= hd->mode;
	phd->us[0x3f    ]	= hd->nsamppx;
	phd->us[0x40    ]	= hd->nsamppy;
	phd->us[0x41    ]	= hd->nsamplx;
	phd->us[0x42    ]	= hd->nsamply;
}

	strtobf((char*)&phd->ch[0x43*2],hd->comment,60);

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->version,(char*)&phd->us[0x68], 2 );
} else {
	phd->us[0x68    ]	= hd->version;
}

	for(i=0;i<15;i++){
		strtobf((char*)&phd->ch[0x6a*2+i*20],hd->bit_plane[i].comment,16);

if( imgf->need_reverse == TRUE ){
		SWAB( (char*)&hd->bit_plane[i].maxv,(char*)&phd->us[0x6a+(i*20+16)/2], 2 );
		SWAB( (char*)&hd->bit_plane[i].minv,(char*)&phd->us[0x6a+(i*20+18)/2], 2 );
} else {
		phd->us[0x6a+(i*20+16)/2]=hd->bit_plane[i].maxv;
		phd->us[0x6a+(i*20+18)/2]=hd->bit_plane[i].minv;
}
	}

if( imgf->need_reverse == TRUE ){
	SWAB( (char*)&hd->data_type,(char*)&phd->us[0xc2], 2 );
} else {
	phd->us[0xc2    ]	= hd->data_type;
}

	if( hd->file_spec == IMAGE_BSQ )
		memcpy(&phd->us[0xcc  ],"BSQ ",4);
	else
		memcpy(&phd->us[0xcc  ],"BIL ",4);
	
/*
	if(      hd->byte_order == IMAGE_SUN )	
		memcpy(&phd->us[0xd6  ],"SUN ",4);
	else if( hd->byte_order == IMAGE_DOS )
		memcpy(&phd->us[0xd6  ],"DOS ",4);
	else if( hd->byte_order == IMAGE_MAC )
		memcpy(&phd->us[0xd6  ],"MAC ",4);
	else
		memcpy(&phd->us[0xd6  ],"MX  ",4);
*/
	switch (hd->byte_order){
		case IMAGE_LSB_1ST:
			memcpy(&phd->us[0xd6  ],"LSB ",4);
			break;
		case IMAGE_MSB_1ST:
			memcpy(&phd->us[0xd6  ],"MSB ",4);
			break;
		default:
			return -1;
	}
	return 0;
}


/************************************************
	Read header from file
*************************************************/
static int
read_header( IMAGE_FILE *imgf, char *header )
{
	LSEEK(imgf->fhandl, (off_t) 0L, SEEK_SET);
	if(
	IREAD(imgf->fhandl,header,IMAGE_HEADER_LENGTH)
		!= IMAGE_HEADER_LENGTH ){

		errmesSys("image_read_header:read error",errno);
		return -1;
	}
	return 0;
}


/************************************************
	Write header to file
*************************************************/
static int
write_header( IMAGE_FILE *imgf, char *header )
{
	LSEEK( imgf->fhandl, (off_t) 0L, SEEK_SET );
	if(
	IWRITE(imgf->fhandl,header,IMAGE_HEADER_LENGTH)
		!= IMAGE_HEADER_LENGTH ){
		errmesSys("image_write_header:write error",errno);
		return -1;
	}
	return 0;
}


/*************************************************
	Read descriptor from file
*************************************************/
/*
static int
read_descriptor( IMAGE_FILE *imgf, int i,  char *descriptor )
{
	LSEEK( imgf->fhandl, (off_t) IMAGE_HEADER_LENGTH*(i+1), SEEK_SET );
	if(
	IREAD(imgf->fhandl,descriptor,IMAGE_HEADER_LENGTH)
		!= IMAGE_HEADER_LENGTH ){
		errmesSys("image_read_descriptor:read error",errno);
		return -1;
	}
	return 0;
}
*/


/************************************************
	Write descriptor to file
*************************************************/
/*
static int
write_descriptor( IMAGE_FILE *imgf, int i,  char *descriptor )
{
	LSEEK( imgf->fhandl, (off_t) IMAGE_HEADER_LENGTH*(i+1), SEEK_SET );
	if(
	IWRITE(imgf->fhandl,descriptor,IMAGE_HEADER_LENGTH)
		!= IMAGE_HEADER_LENGTH ){
		errmesSys("image_write_descriptor:write error",errno);
		return -1;
	}
	return 0;
}
*/


/************************************************
	Read image body from file
*************************************************/
static int
read_body( IMAGE_FILE *imgf )
{
	int i,j;
/*
	long delta,count;
	u_char *bufp1,*bufp2;
*/
	IMAGE *img = imgf->image;

	char	*buf = NULL;

	if( imgf->buffering == FALSE )
		return -1;

#if defined ( IMAGE_INCLD_TIFF )
	if( imgf->tiff != NULL ){
		return tiff_read_body( imgf );
	}
#endif
#if defined ( IMAGE_INCLD_JPEG )
		if( imgf->file_header == IMAGE_JPEG ){
			return jpeg_read_body( imgf );
		}
#endif
#if defined ( IMAGE_INCLD_BMP )
		if( imgf->file_header == IMAGE_BMP ){
			return bmp_read_body( imgf );
		}
#endif

	LSEEK( imgf->fhandl, (off_t) imgf->header_length, SEEK_SET );
	
	switch( imgf->interleave ){

	case IMAGE_BSQ:

if( imgf->need_reverse == TRUE ){
		if((
//		buf = (char*)MALLOC(img->md_channelbytes)
		buf = (char*)MALLOC(img->md_linebytes)
		) == NULL)
			return -1;
}

		for( j=0; j<imgf->nchan; j++ ){
			for(i=0; i<imgf->h; i++){
if( imgf->need_reverse == TRUE ){
				if(
//					(u_long )IREAD( imgf->fhandl,buf,img->md_channelbytes )
//					!= img->md_channelbytes ){
					(u_long )IREAD( imgf->fhandl, buf, img->md_linebytes )
					!= img->md_linebytes){
					errmesSys("imgio:read_body:read error BSQ reverse",errno);
					free(buf);
					return -1;
				}

//				byterev(buf, (char*)img->data[j], img->md_channelbytes, imgf->nbit/8 );
				byterev(buf, (char*)img->data[j][i], img->md_linebytes, imgf->nbit/8 );

} else {

				if(
//				(u_long )IREAD( imgf->fhandl,(char*)img->data[j],img->md_channelbytes )
//				!= img->md_channelbytes ){
				(u_long )IREAD( imgf->fhandl, (char*)img->data[j][i], img->md_linebytes )
				!= img->md_linebytes){
				errmesSys("imgio:read_body:read error BSQ no reverse",errno);
				return -1;
				}
}
			}
		}

if( imgf->need_reverse == TRUE ){
		free(buf);
}

		break;


	case IMAGE_BIL:

//	printf("read_body: imgf->need_reverse %d\n", imgf->need_reverse);
if( imgf->need_reverse == TRUE ){
		if((
		buf = (char*)MALLOC(img->md_linebytes)
		) == NULL)
			return -1;
}

		for(i=0; i<imgf->h; i++){

			for(j=0; j<imgf->nchan; j++){

if( imgf->need_reverse == TRUE ){
				if(
				(u_long )IREAD( imgf->fhandl, buf, img->md_linebytes )
				!= img->md_linebytes){
					errmesSys("imgio:read_body:BIL read error reverse",errno);
					free(buf);
					return -1;
				}

				byterev(buf, (char*)img->data[j][i], img->md_linebytes, imgf->nbit/8 );

} else {
				u_long	tmp;
				if(
				(tmp=
				(u_long )IREAD( imgf->fhandl, (char*)img->data[j][i], img->md_linebytes )
				)
					!= img->md_linebytes){
//				printf("read_body: IREAD error %d %d %d\n", imgf->header_length, tmp,img->md_linebytes);
		errmesSys("imgio:read_body:BIL read error no reverse",errno);
					return -1;
				}
}
			}
		}

if( imgf->need_reverse == TRUE ){
		free(buf);
}
		break;
	}
	return 0;
}


/************************************************
	Save image body to file
*************************************************/
static int
write_body( IMAGE_FILE *imgf )
{
	int i,j;
/*
	long delta,count;
	char *bufp1,*bufp2;
*/
	IMAGE *img = imgf->image;
	char	*buf = NULL;
	
	if( imgf->buffering == FALSE )
		return -1;

#if defined ( IMAGE_INCLD_TIFF )
	if( imgf->tiff != NULL ){
		return tiff_write_body( imgf );
	}
#endif
#if defined ( IMAGE_INCLD_JPEG )
		if( imgf->file_header == IMAGE_JPEG ){
			return jpeg_write_body( imgf );
		}
#endif

		LSEEK( imgf->fhandl, (off_t)imgf->header_length, SEEK_SET );

	switch( imgf->interleave ){

	case IMAGE_BSQ:

if( imgf->need_reverse == TRUE ){
		if((
		buf = (char*)MALLOC(img->md_linebytes)
		) == NULL)
			return -1;
}

		for( j=0; j<imgf->nchan; j++ ){
			for(i=0; i<imgf->h; i++){

if( imgf->need_reverse == TRUE ){

				byterev((char*)img->data[j][i], buf, img->md_linebytes, imgf->nbit/8 );
				if(
				(u_long )IWRITE( imgf->fhandl,buf,img->md_linebytes )
					!= img->md_linebytes ){
		errmes("image_write_body:write error",errno);
					free(buf);
					return -1;
				}
} else {
				if(
				(u_long )IWRITE( imgf->fhandl, (char*)img->data[j][i],img->md_linebytes )
					!= img->md_linebytes ){
		errmesSys("image_write_body:write error",errno);
					return -1;
				}
}
			}
		}

if( imgf->need_reverse == TRUE ){
		free(buf);
}
		break;

	case IMAGE_BIL:

if( imgf->need_reverse == TRUE ){
		if((
		buf = (char*)MALLOC(img->md_channelbytes)
		) == NULL)
			return -1;
}
		for(i=0; i<imgf->h; i++){
			for(j=0; j<imgf->nchan; j++){
if( imgf->need_reverse == TRUE ){
				byterev((char*)img->data[j][i], buf, img->md_linebytes, imgf->nbit/8 );
				if(
				(u_long ) IWRITE( imgf->fhandl,buf,img->md_linebytes )
					!= img->md_linebytes ){
		errmesSys("image_write_body:BIL write error",errno);
					free(buf);
					return -1;
				}
} else {
				if(
				(u_long ) IWRITE( imgf->fhandl, (char*)img->data[j][i], img->md_linebytes )
					!= img->md_linebytes){
		errmesSys("image_write_body:BIL write error",errno);
					return -1;
				}
}
			}
		}
		break;
	}

if( imgf->need_reverse == TRUE ){
	free(buf);
}

	return 0;
}


/************************************************
	Cut out image from file
*************************************************/
static int
file_cutout_BSQ( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data )
{
	int i;
	off_t count,ofst,delta;
	char *buf = NULL;
	
	count = image_size(imgf->nbit,w,1,1);
	ofst = foffset(imgf,xs,ys,chanl);
	delta = image_size(imgf->nbit,imgf->w,1,1);

if( imgf->need_reverse == TRUE ){
	if((
		buf = (char*)MALLOC(count)
	)==NULL)
		return -1;
}

	for(i=0; i<h; i++,ofst+=delta){
		LSEEK(imgf->fhandl, ofst, SEEK_SET);

if( imgf->need_reverse == TRUE ){
		if( IREAD( imgf->fhandl, buf, count ) != count ){
			free(buf);
			return -1;
		}
		byterev(buf, (char*)data[i], count, imgf->nbit/8 );
} else {
		if( IREAD( imgf->fhandl, (char*)data[i], count ) != count )
			return -1;
}

	}

if( imgf->need_reverse == TRUE ){
	free(buf);
}

	return 0;
}


/************************************************
	Cut out image from file
*************************************************/
static int
file_cutout_BIL( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data )
{
	int i;
	off_t count,ofst,delta;
	char *buf = NULL;
	
	count = image_size(imgf->nbit,w,1,1);
	ofst = foffset(imgf,xs,ys,chanl);
	delta = image_size(imgf->nbit,imgf->w,1,imgf->nchan);

if( imgf->need_reverse == TRUE ){
	if((
		buf = (char*)MALLOC(count)
	)==NULL)
		return -1;
}
	for(i=0; i<h; i++,ofst+=delta){
		LSEEK(imgf->fhandl, ofst, SEEK_SET);

if( imgf->need_reverse == TRUE ){
		if( IREAD( imgf->fhandl, buf, count ) != count ){
			free(buf);
			return -1;
		}
		byterev(buf, (char*)data[i], count, imgf->nbit/8 );
} else {
		if( IREAD( imgf->fhandl, (char*)data[i], count ) != count )
			return -1;
}
	}

if( imgf->need_reverse == TRUE ){
	free(buf);
}

	return 0;
}


/************************************************
	Putin image to file
*************************************************/
static int
file_putin_BSQ( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data )
{
	int i;
	off_t count,ofst,delta;
	char	*buf = NULL;
	
	count = image_size(imgf->nbit,w,1,1);
	ofst = foffset(imgf,xs,ys,chanl);
	delta = image_size(imgf->nbit,imgf->w,1,1);

if( imgf->need_reverse == TRUE ){
	if((
		buf = (char*)MALLOC(count)
	)==NULL)
		return -1;
}

	for(i=0; i<h; i++,ofst+=delta){
		LSEEK(imgf->fhandl, ofst, SEEK_SET);

if( imgf->need_reverse == TRUE ){
		byterev((char*)data[i], buf, count, imgf->nbit/8 );
		if( IWRITE( imgf->fhandl, buf, count ) != count ){
			free(buf);
			return -1;
		}
} else {
		if( IWRITE( imgf->fhandl, (char*)data[i], count ) != count )
			return -1;
}

	}

if( imgf->need_reverse == TRUE ){
	free(buf);
}

	return 0;
}


static int
file_putin_BIL( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **data )
{
	int i;
	off_t count,ofst,delta;
	char	*buf = NULL;
	
	count = image_size(imgf->nbit,w,1,1);
	ofst = foffset(imgf,xs,ys,chanl);
	delta = image_size(imgf->nbit,imgf->w,1,imgf->nchan);

if( imgf->need_reverse == TRUE ){
	if((
		buf = (char*)MALLOC(count)
	)==NULL)
		return -1;
}
	for(i=0; i<h; i++,ofst+=delta){

		LSEEK(imgf->fhandl, ofst, SEEK_SET);

if( imgf->need_reverse == TRUE ){
		byterev((char*)data[i], buf, count, imgf->nbit/8 );
		if( IWRITE( imgf->fhandl, buf, count ) != count ){
			free(buf);
			return -1;
		}
} else {
		if( IWRITE( imgf->fhandl, (char*)data[i], count ) != count )
			return -1;
}

	}

if( imgf->need_reverse == TRUE ){
	free(buf);
}

	return 0;
}


/************************************************
	Get offset of pixel in image_file
*************************************************/
static off_t
foffset( IMAGE_FILE *imgf, int x, int y, int chanl )
{
	switch( imgf->interleave ){
	case IMAGE_BSQ:
		return
		 (off_t )imgf->header_length +
		 image_size( imgf->nbit, imgf->w, imgf->h, chanl ) +
		 image_size( imgf->nbit, imgf->w,       y,     1 ) +
		 image_size( imgf->nbit,       x,       1,     1 )   ;
		
	case IMAGE_BIL:
		return
		 (off_t)imgf->header_length +
		 image_size( imgf->nbit, imgf->w,      y, imgf->nchan ) +
		 image_size( imgf->nbit, imgf->w,      1,       chanl ) +
		 image_size( imgf->nbit,       x,      1,           1 )   ;
	default:
		errmes("foffset: illegal file_type", imgf->interleave);
		return
		 (off_t)imgf->header_length;
	}
}


static const char *
fbody( const char *str )
{
	const char	*pt;
	if( strlen( str ) < 2 )
		return str;

	pt = str + strlen(str);
	while( pt > str ){
		pt--;
#if defined( LINUX )
		if( *pt == '/' )
			return pt+1;
#else
		if( *pt == ':' || *pt == '\\' )
			return pt+1;
#endif
	}
	return str;
}


/************************************************
	Print out error message to stderr
*************************************************/
static int
errmes( char *message, int erno )
{
	fprintf(stderr,"%s", message);
	fprintf(stderr,"%d\n", erno);
	return 0;
}

static int
errmesSys( char *message, int erno )
{
		errmes( message, erno );
        if( erno != 0 ){
				perror(message);
		}
	return 0;
}

/*
static int
errmes( char *message, int erno )
{
#if defined(DEBUG)  || defined (_DEBUG )
	if( erno == 0 ){
		fprintf(stdout,message); fprintf(stdout,"\n");
	} else
		perror(message);
#endif
	return 0;
}
*/

/************************************************
	reverse byte order
*************************************************/
static int
byterev( char *src_buf, char *dst_buf, int buf_size, int nbyte )
{
	int	i, j, k;

	if( nbyte == 1 ){
		memcpy( dst_buf, src_buf, buf_size );
	} else {
		i = 0;
		while(i<buf_size){
			k=0;
			for(j=nbyte-1; j>=0; j--, k++)
				dst_buf[i+k] = src_buf[i+j];
			i+=nbyte;
		}
	}
	return 0;
}


#if defined(HAS_FORK)
static int
rmZextension( char *fname )
{
	int	l;

	l = strlen( fname );

	if( l >= 3 && fname[l-2] == '.' && fname[l-1] == 'Z' ){
		fname[l-2] = 0;
		return 1;
	}
	if( l >= 4 && fname[l-3] == '.' && fname[l-2] == 'g' && fname[l-1] == 'z' ){
		fname[l-3] = 0;
		return 1;
	}
	return 0;
}
#endif


static int
msif_check( IMAGE_FILE *imgf )
{

	if( read_header( imgf, (char*)&imgf->physical_header ) != 0 ){
		errmes("msif_check: read_header error\n",0);
		imgf->file_header = EXHDR;
		return -1;
	}

	if( image_file_check( imgf ) != 0 ){
//		errmes("msif_check: image_file_check failed\n",0);
		return -1;
	}

	if( imgf->byte_order != byte_order_this_machine )
		imgf->need_reverse = TRUE;
	else
		imgf->need_reverse = FALSE;

	/*	Set IMAGE_FILE control block after header	*/

	if(
	header_to_image( imgf ) != 0 ){
		errmes("msif_check:header_to_image: illegal header",0);
		return -1;
	}
	return 0;

}


static int
ext_check( IMAGE_FILE *imgf, char *ext, int file_header, int (*external_header_to_image)( FILE *ext_fp, IMAGE_FILE *imgf ) )
{
	char ext_fname[256+3];
	FILE *ext_fp;

	STRNCPY( ext_fname, imgf->fullPath, 256 );
	STRCAT( ext_fname, ext );

	if( ACCESS( ext_fname, 0 ) == 0 ){
		if((
			ext_fp = fopen( ext_fname, "r" )
		) == NULL ){
			errmes("ext_check: external header file open error",0);
//			fclose( ext_fp );
			return -1;
		}

		imgf->file_header = file_header;

		if( (external_header_to_image)( ext_fp, imgf ) != 0 ){
			errmes("ext_check: external_header_to_image error :", 0 );
			fclose( ext_fp );
			return -1;
		}

		fclose( ext_fp );
	} else {
//		fprintf( stderr, "ext_check: external header %s\n", ext_fname);
//		errmes("ext_check:external header file access error",0);
		return -1;
	}

	if( imgf->byte_order != byte_order_this_machine )
		imgf->need_reverse = TRUE;
	else
		imgf->need_reverse = FALSE;

	return 0;
}


static int
ex_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf ) 
{
	char buf[512];
	char *pt, *pt2;
	off_t	st_size;

//	printf("ex_header_to_image %d %d %d\n", imgf->w,imgf->h ,imgf->nchan );
	default_setup_imgf( imgf );

	fseek(hdrfp, 0, SEEK_SET);

	while( fgets(buf,512,hdrfp) != NULL ){

		pt = rmSpace( buf );

		if( strncmp( "skip", pt, 4 ) == 0 ){
			imgf->header_length = atoi( GetValue(pt) );
        	} else if (strncmp( "width", pt, 5 ) == 0 ){
			imgf->w = atoi( GetValue(pt) );
        	} else if (strncmp( "height", pt, 6 ) == 0 ){
			imgf->h = atoi( GetValue(pt) );
        	} else if (strncmp( "nchan", pt, 5 ) == 0 ){
			imgf->nchan = atoi( GetValue(pt) );
        	} else if (strncmp( "format", pt, 6 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(strncmp( "bil", pt2, 3 ) == 0 )
				imgf->interleave = IMAGE_BIL;
			else if(strncmp( "bsq", pt2, 3 ) == 0 )
				imgf->interleave = IMAGE_BSQ;
        	} else if (strncmp( "data_type", pt, 9 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(strncmp( "char", pt2, 4 ) == 0 ){
				imgf->data_type = IMAGE_CHAR;
				imgf->nbit      = 8;
			} else if( strncmp( "short", pt2, 5 ) == 0 ){
				imgf->data_type = IMAGE_SHORT;
				imgf->nbit      = 16;
			} else if( strncmp( "u_short", pt2, 7 ) == 0 ){
				imgf->data_type = IMAGE_USHORT;
				imgf->nbit      = 16;
			} else if( strncmp( "long", pt2, 4 ) == 0 ){
				imgf->data_type = IMAGE_LONG;
				imgf->nbit      = 32;
			} else if( strncmp( "u_long", pt2, 6 ) == 0 ){
				imgf->data_type = IMAGE_ULONG;
				imgf->nbit      = 32;
			} else if( strncmp( "float", pt2, 5 ) == 0 ){
				imgf->data_type = IMAGE_FLOAT;
				imgf->nbit      = 32;
			} else if( strncmp( "double", pt2, 6 ) == 0 ){
				imgf->data_type = IMAGE_DOUBLE;
				imgf->nbit      = 64;
			} else
				return -1;

        	} else if (strncmp( "byte_order", pt, 10 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(strncmp( "lsb", pt2, 3 ) == 0 )
				imgf->byte_order = IMAGE_LSB_1ST;
			else if(strncmp( "msb", pt2, 3 ) == 0 )
				imgf->byte_order = IMAGE_MSB_1ST;
		}
	}

//	printf("ex_header_to_image %d %d %d\n", imgf->w,imgf->h ,imgf->nchan );
	if( imgf->w == -1 || imgf->h == -1 || imgf->nchan == -1 )
		return -1;

	st_size = fsize( imgf->fullPath );

	if( st_size != image_size(imgf->nbit,imgf->w,imgf->h,imgf->nchan) + (long)imgf->header_length ){
			return -1;
	}
	return 0;
}


static int
erm_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf ) 
{
	char buf[512];
	char *pt, *pt2;
	off_t   st_size;

//	printf("erm_header_to_image: %d\n", imgf->fhandl);
	default_setup_imgf( imgf );

	fseek(hdrfp, 0, SEEK_SET);

	while( fgets(buf, 512, hdrfp) != NULL ){
		pt = rmSpace( buf );
		if( STRNICMP( "HeaderOffset", pt, 12 ) == 0 ){
			imgf->header_length = atoi( GetValue(pt) );
        	} else if (STRNICMP( "NrOfCellsPerLine", pt, 16 ) == 0 ){
			imgf->w = atoi( GetValue(pt) );
        	} else if (STRNICMP( "NrOfLines", pt, 9 ) == 0 ){
			imgf->h = atoi( GetValue(pt) );
        	} else if (STRNICMP( "NrOfBands", pt, 9 ) == 0 ){
			imgf->nchan = atoi( GetValue(pt) );
        	} else if (STRNICMP( "DataType", pt, 8 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(STRNICMP( "Raster", pt2, 6 ) == 0 )
				imgf->interleave = IMAGE_BIL;
			else /* this is vector */
				return -1;
        	} else if (STRNICMP( "CellType", pt, 6 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(STRNICMP( "Unsigned8BitInteger", pt2, 19 ) == 0 ){
				imgf->data_type = IMAGE_CHAR;
				imgf->nbit      = 8;
			} else if( STRNICMP( "Signed16BitInteger", pt2, 18 ) == 0 ){
				imgf->data_type = IMAGE_SHORT;
				imgf->nbit      = 16;
			} else if( STRNICMP( "Unsigned16BitInteger", pt2, 20 ) == 0 ){
				imgf->data_type = IMAGE_USHORT;
				imgf->nbit      = 16;
			} else if( STRNICMP( "Signed32BitInteger", pt2, 18 ) == 0 ){
				imgf->data_type = IMAGE_LONG;
				imgf->nbit      = 32;
			} else if( STRNICMP( "Unsigned32BitInteger", pt2, 20 ) == 0 ){
				imgf->data_type = IMAGE_ULONG;
				imgf->nbit      = 32;
			} else if( STRNICMP( "IEEE4ByteReal", pt2, 13 ) == 0 ){
				imgf->data_type = IMAGE_FLOAT;
				imgf->nbit      = 32;
			} else if( STRNICMP( "IEEE8ByteReal", pt2, 13 ) == 0 ){
				imgf->data_type = IMAGE_DOUBLE;
				imgf->nbit      = 64;
			} else {
				fprintf( stderr, "erm_header_to_image: illegal data type %s\n", pt2);

				return -1;
}
        	} else if (STRNICMP( "ByteOrder", pt, 9 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(STRNICMP( "LSBFirst", pt2, 8 ) == 0 )
				imgf->byte_order = IMAGE_LSB_1ST;
			else if(STRNICMP( "MSBFirst", pt2, 8 ) == 0 )
				imgf->byte_order = IMAGE_MSB_1ST;
		}
	}

	if( imgf->w == -1 || imgf->h == -1 || imgf->nchan == -1 )
		return -1;

	st_size = fsize( imgf->fullPath );
	if( st_size != image_size(imgf->nbit,imgf->w,imgf->h,imgf->nchan) + (long)imgf->header_length )
			return -1;

	return 0;
}


static int
envi_header_to_image( FILE *hdrfp, IMAGE_FILE *imgf ) 
{
	char buf[512];
	char *pt, *pt2, *token;
	off_t    st_size;
	double	dx, dy;
	int	i;

	default_setup_imgf( imgf );

	fseek(hdrfp, 0, SEEK_SET);

	while( fgets(buf, 512, hdrfp) != NULL ){
		pt = rmSpace( buf );
		if( strncmp( "header offset", pt, 13 ) == 0 ){
			imgf->header_length = atoi( GetValue(pt) );
        	} else if (strncmp( "samples", pt, 7 ) == 0 ){
			imgf->w = atoi( GetValue(pt) );
        	} else if (strncmp( "lines", pt, 5 ) == 0 ){
			imgf->h = atoi( GetValue(pt) );
        	} else if (strncmp( "bands", pt, 5 ) == 0 ){
			imgf->nchan = atoi( GetValue(pt) );
        	} else if (strncmp( "interleave", pt, 10 ) == 0 ){
			pt2 = rmSpace( GetValue(pt) );
			if(strncmp( "bil", pt2, 3 ) == 0 )
				imgf->interleave = IMAGE_BIL;
			else if(strncmp( "bsq", pt2, 3 ) == 0 )
				imgf->interleave = IMAGE_BSQ;
			else {
				errmes("envi_header_to_image: interleave must be bil or bsq ",0);
				return -1;
			}
        } else if (strncmp( "data type", pt, 9 ) == 0 ){
			int data_type;
			data_type = atoi( GetValue(pt) );

			switch ( data_type ){
				case 1:
					imgf->data_type = IMAGE_CHAR;
					imgf->nbit      = 8;
					break;
				case 2:
					imgf->data_type = IMAGE_SHORT;
					imgf->nbit      = 16;
					break;
				case 12:
					imgf->data_type = IMAGE_USHORT;
					imgf->nbit      = 16;
					break;
				case 3:
					imgf->data_type = IMAGE_LONG;
					imgf->nbit      = 32;
					break;
				case 13:
					imgf->data_type = IMAGE_ULONG;
					imgf->nbit      = 32;
					break;
				case 4:
					imgf->data_type = IMAGE_FLOAT;
					imgf->nbit      = 32;
					break;
				case 5:
					imgf->data_type = IMAGE_DOUBLE;
					imgf->nbit      = 64;
					break;
				default:
					fprintf(stderr, "envi_header_to_image: data type not supported: %d\n",data_type);
					return -1;
			}

        } else if ( strncmp( "byte order", pt, 10 ) == 0 ){
			int byte_order;
			byte_order = atoi( GetValue(pt) );
			switch (byte_order){
				case 0:
					imgf->byte_order = IMAGE_LSB_1ST;
					break;
				case 1:
					imgf->byte_order = IMAGE_MSB_1ST;
					break;
				default:
					return -1;
			}
        } else if ( strncmp( "data ignore value", pt, 17 ) == 0 ){
			double noDataValue;
			noDataValue = atof( GetValue(pt) );
			imgf->attr.haveNoDataValue = TRUE;
			imgf->attr.noDataValue = noDataValue;
        } else if ( strncmp( "pixel size", pt, 10 ) == 0 ){
			dx = dy = -1.0;
			if( sscanf( (pt2=GetValue2(pt)), " { %lf , %lf", &dx, &dy ) != 2 ){
				fprintf(stderr,"envi_header_to_image: dx, dy read error %s\n", pt2);
				return -1;
			}
			if( dx > 0  && dy > 0 ) {
				imgf->geoRegistration.dx = dx;
				imgf->geoRegistration.dy = dy;
//				fprintf(stderr,"envi_header_to_image: dx, dy %lf %lf\n", dx, dy);
			} else {
				fprintf(stderr,"envi_header_to_image: dx, dy < 0 %lf %lf\n", dx, dy);
			}
        } else if ( strncmp( "map info", pt, 8 ) == 0 ){
			char *seps = "{,}";
//map info = {Geographic Lat/Lon, 1, 1, -180.000, 90.000, 0.008333, 0.008333}
//map info = {UTM, 1.000, 1.000, 288404.265, 4214499.607, 28.50000000, 28.50000000, 52, North, WGS-84, units=Meters}

			dx = dy = -1.0;

			pt2 = GetValue2(pt);
			pt2 = rmSpace(pt2);
			token = strtok( pt2, seps );
			if( token == NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 1 %s\n", pt2);
				return -1;
			}
//			printf("envi_header_to_image: token 1  [%s]\n", token );
			token = rmSpace(token);
			i = 0;
			imgf->geoRegistration.projection = IMAGE_PROJECTION_NONE;
			while( enviRegistrationString[i] != NULL ){
//			printf("envi_header_to_image: %d %s %s\n", i,  token, enviRegistrationString[i] );
				if( strcmp(token, enviRegistrationString[i] ) == 0  ){
					imgf->geoRegistration.projection = i;
					break;
				}
				i++;
			}
			imgf->geoRegistration.Unit = IMAGE_UNITMETER;
			if( imgf->geoRegistration.projection == IMAGE_PROJECTION_NONE ){
				fprintf(stderr, "warning: envi_header_to_image: unknown projection %s\n", token );
			}
			if( imgf->geoRegistration.projection == IMAGE_PROJECTION_GEOGRAPHIC ){
				imgf->geoRegistration.Unit = IMAGE_UNITDEGREE;
			} else {
				imgf->geoRegistration.Unit = IMAGE_UNITMETER;
			}

//			printf("envi_header_to_image: projection from [%s] %d [%s]\n", token, i,  enviRegistrationString[imgf->geoRegistration.projection] );


			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 2 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.geoRegistrationX = atof ( token ) -1.;

			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 3 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.geoRegistrationY = atof ( token ) -1.;

			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 4 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.geoRegistrationMapX = atof ( token );

			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 5 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.geoRegistrationMapY = atof ( token );

			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 6 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.dx = atof ( token );
			if( (token = strtok( NULL, seps ) )== NULL ){
				fprintf(stderr,"envi_header_to_image: map info read error 7 %s\n", pt2);
				return -1;
			}
			imgf->geoRegistration.dy = atof ( token );

			if( imgf->geoRegistration.projection == IMAGE_PROJECTION_UTM ){

				if( (token = strtok( NULL, seps ) )== NULL ){
					fprintf(stderr,"envi_header_to_image: map info read error 8 %s\n", pt2);
					return -1;
				}
				imgf->geoRegistration.UTMZoneNumber = atoi ( token );


				if( (token = strtok( NULL, seps ) )== NULL ){
					fprintf(stderr,"envi_header_to_image: map info read error 9 %s\n", pt2);
					return -1;
				}
				token = rmSpace(token);
				i = 0;
				imgf->geoRegistration.UTMNorthSouth = IMAGE_UTMNORTH;
				while( enviUTMNorthSouthString[i] != NULL ){
//				printf("envi_header_to_image: %d %s %s\n", i,  token, enviRegistrationString[i] );
					if( STRICMP(token, enviUTMNorthSouthString[i] ) == 0  ){
						imgf->geoRegistration.UTMNorthSouth = i;
						break;
					}
					i++;
				}
//				printf("envi_header_to_image: [%s] %d [%s]\n", token, i,  enviRegistrationString[imgf->geoRegistration.projection] );
			
				if( (token = strtok( NULL, seps ) )== NULL ){
					fprintf(stderr,"envi_header_to_image: map info read error 10 %s\n", pt2);
					return -1;
				}
				STRCPY( imgf->geoRegistration.datumName, token );
				imgf->geoRegistration.datum= IMAGE_DATUM_NONE;
				token = rmSpace(token);
				i = 0;
				while( enviDatumString[i] != NULL ){
//					printf("%d [%s] [%s]\n", i, token, enviDatumString[i] );
					if( STRICMP(token, enviDatumString[i] ) == 0  ){
						imgf->geoRegistration.datum = i;
						break;
					}
					i++;
				}			
				if( (token = strtok( NULL, seps ) )== NULL ){
					fprintf(stderr,"envi_header_to_image: map info read error 11 %s\n", pt2);
					return -1;
				}
				token = rmSpace(token);
				i = 0;
				imgf->geoRegistration.Unit= IMAGE_UNITMETER;
				while( enviUTMUnitString[i] != NULL ){
					if( STRICMP(token, enviUTMUnitString[i] ) == 0  ){
						imgf->geoRegistration.Unit = i;
						break;
					}
					i++;
				}			

			} else if( imgf->geoRegistration.projection == 	IMAGE_PROJECTION_GEOGRAPHIC ){
				if( (token = strtok( NULL, seps ) )== NULL ){
					fprintf(stderr,"envi_header_to_image: map info read error 10 %s\n", pt2);
					return -1;
				}
				STRCPY( imgf->geoRegistration.datumName, token );
				imgf->geoRegistration.datum= IMAGE_DATUM_NONE;
				token = rmSpace(token);
				i = 0;
				while( enviDatumString[i] != NULL ){
//					printf("%d [%s] [%s]\n", i, token, enviDatumString[i] );
					if( STRICMP(token, enviDatumString[i] ) == 0  ){
						imgf->geoRegistration.datum = i;
						break;
					}
					i++;
				}			
			}

/*
		fprintf(stderr, "map info = { %s, %lf, %lf, %lf, %lf, %14.8lf, %14.8lf, %d, %s, %s, %s }\n",
			enviRegistrationString[imgf->geoRegistration.projection],
			imgf->geoRegistration.geoRegistrationX+1,
			imgf->geoRegistration.geoRegistrationY+1,
			imgf->geoRegistration.geoRegistrationMapX,
			imgf->geoRegistration.geoRegistrationMapY,
			imgf->geoRegistration.dx,
			imgf->geoRegistration.dy,
			imgf->geoRegistration.UTMZoneNumber,
			enviUTMNorthSouthString[ imgf->geoRegistration.UTMNorthSouth],
			enviDatumString[ imgf->geoRegistration.datum],
			enviUTMUnitString[ imgf->geoRegistration.Unit ]
			);
*/
/*
		fprintf(stderr, "envi_header_to_image: map info = { %s, %lf, %lf, %lf, %lf, %lf, %lf }\n",
			enviRegistrationString[imgf->geoRegistration.projection],
			imgf->geoRegistration.geoRegistrationX+1,
			imgf->geoRegistration.geoRegistrationY+1,
			imgf->geoRegistration.geoRegistrationMapX,
			imgf->geoRegistration.geoRegistrationMapY,
			imgf->geoRegistration.dx,
			imgf->geoRegistration.dy );
*/

/*
			if( sscanf( (pt2=GetValue2(pt)), " { %s, %lf, %lf, %lf, %lf, %lf, %lf",
			buf,
			&imgf->geoRegistration.geoRegistrationX,
			&imgf->geoRegistration.geoRegistrationY,
			&imgf->geoRegistration.geoRegistrationMapX,
			&imgf->geoRegistration.geoRegistrationMapY,
			&imgf->geoRegistration.dx,
			&imgf->geoRegistration.dy ) != 7 ){
				fprintf(stderr,"envi_header_to_image: map info read error %s\n", pt2);
				return -1;
			}
*/
		}
	}

/*
			if( imgf->attr.haveNoDataValue )
				printf( "envi_header_to_image no data value = %lf\n", imgf->attr.noDataValue );
			else
				printf( "envi_header_to_image No no data value\n" );
*/

	if( imgf->w == -1 || imgf->h == -1 || imgf->nchan == -1 )
		return -1;

	st_size = fsize( imgf->fullPath );
//	printf("%s\n", imgf->fullPath);
//	printf("%d %d %d %d %d\n", imgf->w ,imgf->h ,imgf->nchan, st_size, image_size(imgf->nbit,imgf->w,imgf->h,imgf->nchan));
	if( st_size != image_size(imgf->nbit,imgf->w,imgf->h,imgf->nchan) + (long)imgf->header_length ) {
		fprintf(stderr,"envi_header_to_image: image size error %lld\n", st_size);
//		fprintf(stderr,"envi_header_to_image: image size error %ld\n", st_size);
		return -1;
	}
	return 0;
}

static int
default_setup_imgf( IMAGE_FILE *imgf )
{
/*	imgf->fhandl = -1;	*/
	imgf->w		= -1;
	imgf->h		= -1;
	imgf->nchan	= -1;
	imgf->data_type = IMAGE_CHAR;
	imgf->nbit      = 8;
	imgf->ndesc     = 0;
	initGeoRegistrationInfo ( &imgf->geoRegistration );
	initAttr ( &imgf->attr );
	imgf->interleave = IMAGE_BIL;
	imgf->byte_order= byte_order_this_machine;
	imgf->header_length = 0;
	STRCPY( imgf->header.comment, "\0" );
	STRCPY( imgf->header.mmddyy, "\0" );
#if defined ( IMAGE_INCLD_TIFF )
	imgf->tiff = NULL;
	imgf->tiffLineBuf = NULL;
	imgf->tiffBufRow = -1;
	imgf->written = NULL;
#endif
#if defined ( IMAGE_INCLD_JPEG )
	((JPG*)(imgf->jpg))->jpegFp = NULL;
	((JPG*)(imgf->jpg))->jpegQuality = 75;
	((JPG*)(imgf->jpg))->jpegQuality = jpeg_default_quality;
	((JPG*)(imgf->jpg))->jpegLineBufNo = -1;
	((JPG*)(imgf->jpg))->jpegWriteLineInbuf = NULL;
	((JPG*)(imgf->jpg))->jpegLineBuf = NULL;
	((JPG*)(imgf->jpg))->jpeg_row_pointer[0] = NULL;
	((JPG*)(imgf->jpg))->jpegCompressDir= -1;
#endif
#if defined ( IMAGE_INCLD_BMP )
	imgf->bmp = NULL;
#endif
	return 0;
}

static char *
GetValue( char *buf )
{
	char    *pt;

	pt = buf + strlen(buf);

	while( pt > buf ){
		pt--;
                if( *pt == '=' )
                        return pt+1;
        }
	return NULL;
}

static char *
GetValue2( char *buf )
{
	char    *pt;

	pt = buf;

	while( pt ){
		if( *pt == '=' )
			return pt+1;
		pt++;
	}
	return NULL;
}

/* Remove Preceedent Spaces */
static char *
rmSpace( char *buf )
{
	char	*pt = buf;

	while( isspace(*pt) ){
		pt++;
	}

	return pt;
}

static off_t fsize( char *name )
{
	struct stat 	stbuf;

	if( stat(name, &stbuf) == -1)
		return (off_t ) -1;
	return stbuf.st_size;
}

static int
write_ext_header( IMAGE_FILE *imgf )
{
	FILE	*ext_fp;
	char ext_fname[256+3];
	time_t  clock;
	struct tm *stm;
	char	s[80];
 
	STRNCPY( ext_fname, imgf->fullPath, 256 );
	STRCAT( ext_fname, EXHDR_EXTENTION );

	time(&clock); stm = localtime( &clock );
	strftime( s, 80, "%c", stm );

	if((
		ext_fp = fopen( ext_fname, "wt" )
	) == NULL ){
		fprintf(stderr,"write_ext_header: ext file open error\n");
		return -1;
	}

	fprintf(ext_fp, "# Image File Headder Version 1.0\n");
	fprintf(ext_fp, "# Created on : %s\n", s);

	fprintf(ext_fp, "width = %d\n", imgf->w );
	fprintf(ext_fp, "height = %d\n", imgf->h );
	fprintf(ext_fp, "nchan = %d\n", imgf->nchan );

	switch( imgf->data_type ){
		case IMAGE_CHAR:
			fprintf(ext_fp, "data_type = char\n" );
			break;
		case IMAGE_SHORT:
			fprintf(ext_fp, "data_type = short\n" );
			break;
		case IMAGE_USHORT:
			fprintf(ext_fp, "data_type = u_short\n" );
			break;
		case IMAGE_LONG:
			fprintf(ext_fp, "data_type = long\n" );
			break;
		case IMAGE_ULONG:
			fprintf(ext_fp, "data_type = u_long\n" );
			break;
		case IMAGE_FLOAT:
			fprintf(ext_fp, "data_type = float\n" );
			break;
		case IMAGE_DOUBLE:
			fprintf(ext_fp, "data_type = double\n" );
			break;
		default:
			return -1;
	}

	switch( imgf->byte_order ){
		case IMAGE_LSB_1ST:
			fprintf(ext_fp, "byte_order = lsb\n" );
			break;
		case IMAGE_MSB_1ST:
			fprintf(ext_fp, "byte_order = msb\n" );
			break;
		default:
			return -1;
	}

	switch( imgf->interleave ){
		case IMAGE_BIL:
			fprintf(ext_fp, "format = bil\n" );
			break;
		case IMAGE_BSQ:
			fprintf(ext_fp, "format = bsq\n" );
			break;
		default:
			return -1;
	}

	fprintf(ext_fp, "skip = %d\n", imgf->header_length );

	fclose( ext_fp );

	return 0;
}

static int
write_erm_header( IMAGE_FILE *imgf )
{
	FILE	*erm_fp;
	char erm_fname[256+3];
	time_t  clock;
	struct tm *stm;
	char	s[80];
	int	i;
 
	STRNCPY( erm_fname, imgf->fullPath, 256 );
	STRCAT( erm_fname, ERM_EXTENTION );

	switch( imgf->interleave ){
		case IMAGE_BIL:
			break;
		case IMAGE_BSQ:
			errmes("write_erm_header : BSQ format is not supported\n",0);
			return -1;
		default:
			return -1;
	}

	time(&clock); stm = localtime( &clock );
	strftime( s, 80, "%c", stm );

	if((
		erm_fp = fopen( erm_fname, "wt" )
	) == NULL ){
		fprintf(stderr,"write_erm_header: erm file open error\n");
		return -1;
	}

	fprintf(erm_fp, "DatasetHeader Begin\n");
	fprintf(erm_fp, "\tVersion\t = \"5.0\"\n");
/*	fprintf(erm_fp, "\tLastUpdated\t = %s\n", s);
	fprintf(erm_fp, "\tSenseDate\t = %s\n", s);
	fprintf(erm_fp, "\tLastUpdated\t = %s\n", ctime(&clock));
	fprintf(erm_fp, "\tSenseDate\t = %s\n", ctime(&clock));
*/
	fprintf(erm_fp, "\tDataSetType\t = ERStorage\n");
	fprintf(erm_fp, "\tDataType\t = Raster\n");

	switch( imgf->byte_order ){
		case IMAGE_LSB_1ST:
			fprintf(erm_fp, "\tByteOrder\t = LSBFirst\n" );
			break;
		case IMAGE_MSB_1ST:
			fprintf(erm_fp, "\tByteOrder\t = MSBFirst\n" );
			break;
		default:
			return -1;
	}

	fprintf(erm_fp, "\tHeaderOffset\t = %d\n", imgf->header_length );
	fprintf(erm_fp, "\tCoordinateSpace Begin\n" );
	fprintf(erm_fp, "\t\tDatum\t = \"RAW\"\n" );
	fprintf(erm_fp, "\t\tProjection\t = \"RAW\"\n" );

	fprintf(erm_fp, "\t\tCoordinateType\t = RAW\n" );
	fprintf(erm_fp, "\t\tRotation\t = 0:0:0.0\n" );
	fprintf(erm_fp, "\tCoordinateSpace End\n" );

	fprintf(erm_fp, "\tRasterInfo Begin\n" );

	switch( imgf->data_type ){
		case IMAGE_CHAR:
			fprintf(erm_fp, "\t\tCellType\t = Unsigned8BitInteger\n" );
			break;
		case IMAGE_SHORT:
			fprintf(erm_fp, "\t\tCellType\t = Signed16BitInteger\n" );
			break;
		case IMAGE_USHORT:
			fprintf(erm_fp, "\t\tCellType\t = Unsigned16BitInteger\n" );
			break;
		case IMAGE_LONG:
			fprintf(erm_fp, "\t\tCellType\t = Signed32BitInteger\n" );
			break;
		case IMAGE_ULONG:
			fprintf(erm_fp, "\t\tCellType\t = Unsigned32BitInteger\n" );
			break;
		case IMAGE_FLOAT:
			fprintf(erm_fp, "\t\tCellType\t = IEEE4ByteReal\n" );
			break;
		case IMAGE_DOUBLE:
			fprintf(erm_fp, "\t\tCellType\t = IEEE8ByteReal\n" );
			break;
		default:
			return -1;
	}

//	fprintf(erm_fp, "\t\tNullCellValue\t = 0\n" );
	fprintf(erm_fp, "\t\tNrOfLines\t = %d\n", imgf->h );
	fprintf(erm_fp, "\t\tNrOfCellsPerLine\t = %d\n", imgf->w );
	fprintf(erm_fp, "\t\tNrOfBands\t = %d\n", imgf->nchan );

	for(i=0; i<imgf->nchan; i++){
		fprintf(erm_fp, "\t\tBandID Begin\n");
		fprintf(erm_fp, "\t\t\tValue\t = \"band %d\"\n",i+1);
		fprintf(erm_fp, "\t\tBandID End\n");
	}

	fprintf(erm_fp, "\tRasterInfo End\n");
	fprintf(erm_fp, "DatasetHeader End\n");
	fclose( erm_fp );

	return 0;
}


static int
write_envi_header( IMAGE_FILE *imgf )
{
	FILE	*envi_fp;
	char envi_fname[256+3];
	time_t  clock;
	struct tm *stm;
	char	s[80];
 
	STRNCPY( envi_fname, imgf->fullPath, 256 );
	STRCAT( envi_fname, ENVI_EXTENTION );

	time(&clock); stm = localtime( &clock );
	strftime( s, 80, "%c", stm );

	if((
		envi_fp = fopen( envi_fname, "wt" )
	) == NULL ){
		fprintf(stderr,"write_envi_header: envi file open error\n");
		return -1;
	}

	fprintf(envi_fp, "ENVI\n");
	fprintf(envi_fp, "samples = %d\n", imgf->w );
	fprintf(envi_fp, "lines   = %d\n", imgf->h );
	fprintf(envi_fp, "bands   = %d\n", imgf->nchan );

	switch( imgf->data_type ){
		case IMAGE_CHAR:
			fprintf(envi_fp, "data type = 1\n" );
			break;
		case IMAGE_SHORT:
			fprintf(envi_fp, "data type = 2\n" );
			break;
		case IMAGE_USHORT:
			fprintf(envi_fp, "data type = 12\n" );
			break;
		case IMAGE_LONG:
			fprintf(envi_fp, "data type = 3\n" );
			break;
		case IMAGE_ULONG:
			fprintf(envi_fp, "data type = 13\n" );
			break;
		case IMAGE_FLOAT:
			fprintf(envi_fp, "data type = 4\n" );
			break;
		case IMAGE_DOUBLE:
			fprintf(envi_fp, "data type = 5\n" );
			break;
		default:
			return -1;
	}

	switch( imgf->interleave ){
		case IMAGE_BIL:
			fprintf(envi_fp, "interleave = bil\n" );
			break;
		case IMAGE_BSQ:
			fprintf(envi_fp, "interleave = bsq\n" );
			break;
		default:
			return -1;
	}

	fprintf(envi_fp, "header offset = %d\n", imgf->header_length );

	switch( imgf->byte_order ){
		case IMAGE_LSB_1ST:
			fprintf(envi_fp, "byte order = 0\n" );
			break;
		case IMAGE_MSB_1ST:
			fprintf(envi_fp, "byte order = 1\n" );
			break;
		default:
			return -1;
	}


	if( imgf->attr.haveNoDataValue ) {
		fprintf(envi_fp, "data ignore value = %lf\n", imgf->attr.noDataValue );
	}
	if( imgf->geoRegistration.projection == IMAGE_PROJECTION_GEOGRAPHIC ){
		fprintf(envi_fp, "map info = { %s, %lf, %lf, %18.12le, %18.12le, %18.12le, %18.12le, %s, %s }\n",
			enviRegistrationString[imgf->geoRegistration.projection],
			imgf->geoRegistration.geoRegistrationX+1,
			imgf->geoRegistration.geoRegistrationY+1,
			imgf->geoRegistration.geoRegistrationMapX,
			imgf->geoRegistration.geoRegistrationMapY,
			imgf->geoRegistration.dx,
			imgf->geoRegistration.dy,
			enviDatumString[ imgf->geoRegistration.datum ],
			enviUTMUnitString[ imgf->geoRegistration.Unit ]);

//map info = {Geographic Lat/Lon, 1, 1, -180.000, 90.000, 0.008333, 0.008333}
	} else if( imgf->geoRegistration.projection == IMAGE_PROJECTION_UTM ){
//map info = {UTM, 1.000, 1.000, 288404.265, 4214499.607, 28.50000000, 28.50000000, 52, North, WGS-84, units=Meters}
		fprintf(envi_fp, "map info = { %s, %lf, %lf, %18.12le, %18.12le, %18.12le, %18.12le, %d, %s, %s, %s }\n",
			enviRegistrationString[imgf->geoRegistration.projection],
			imgf->geoRegistration.geoRegistrationX+1,
			imgf->geoRegistration.geoRegistrationY+1,
			imgf->geoRegistration.geoRegistrationMapX,
			imgf->geoRegistration.geoRegistrationMapY,
			imgf->geoRegistration.dx,
			imgf->geoRegistration.dy,
			imgf->geoRegistration.UTMZoneNumber,
			enviUTMNorthSouthString[ imgf->geoRegistration.UTMNorthSouth],
			enviDatumString[ imgf->geoRegistration.datum ],
			enviUTMUnitString[ imgf->geoRegistration.Unit ]
			);

	} else if( imgf->geoRegistration.projection == IMAGE_PROJECTION_ARBITRARY ){
//map info = {Arbitrary, 1.000, 1.000, 288404.265, 4214499.607, 28.50000000, 28.50000000, 1, units=Meters}
		fprintf(envi_fp, "map info = { %s, %lf, %lf, %18.12le, %18.12le, %18.12le, %18.12le, %d, %s }\n",
			enviRegistrationString[imgf->geoRegistration.projection],
			imgf->geoRegistration.geoRegistrationX+1,
			imgf->geoRegistration.geoRegistrationY+1,
			imgf->geoRegistration.geoRegistrationMapX,
			imgf->geoRegistration.geoRegistrationMapY,
			imgf->geoRegistration.dx,
			imgf->geoRegistration.dy,
			1,
			enviUTMUnitString[ imgf->geoRegistration.Unit ]
			);

	} else {
		if( imgf->geoRegistration.dx > 0  && imgf->geoRegistration.dy > 0 ){
			fprintf(envi_fp, "pixel size = { %18.12le , %18.12le }\n",
				imgf->geoRegistration.dx, imgf->geoRegistration.dy );
		}
	}

	fclose( envi_fp );

	return 0;
}

static int removeExtention( char *fname, char *extention )
{

	int	l;

	if( ( l = strlen( fname )) < 5 )
		return -1;
	if( strcmp( fname+l-4, extention ) )
		return -1;

	fname[ l-4 ] = 0;

	return 0;
}

/*
static int removeExtention2( char *fname )
{

	int	l;

	l = strlen( fname );
	if( l > 4 && fname[l-4] == '.' ) {
		fname[l-4] = 0;
	} else if( l > 5 && fname[l-5] == '.' ){
		fname[l-5] = 0;
	} else {
		return -1;
	}
	return 0;
}
*/

static int
write_arcInfo_header( IMAGE_FILE *imgf )
{
	FILE	*arc_fp;
	char arcInfo_fname[256+3];

	if( imgf->interleave != IMAGE_BIL )
		return -2;

	STRNCPY( arcInfo_fname, imgf->fullPath, 256 );
	if( removeExtention( arcInfo_fname, ".bil") )
		return -1;

	STRCAT( arcInfo_fname, ARCINFO_EXTENTION );

	if((
		arc_fp = fopen( arcInfo_fname, "wt" )
	) == NULL ){
		fprintf(stderr,"write_arcInfo_header: arc_info file open error\n");
		return -1;
	}

	fprintf(arc_fp, "nrows %d\n", imgf->h );
	fprintf(arc_fp, "ncols %d\n", imgf->w );
	fprintf(arc_fp, "nbits %d\n", imgf->nbit );
	fprintf(arc_fp, "nbands %d\n", imgf->nchan );

	fclose( arc_fp );

	return 0;
}

#if defined(MAC)
static void
swab( char *from,  char *to, int nbytes)
{
	int	i;

	if( nbytes < 2 )
		return;
	nbytes = ( nbytes / 2 ) * 2;
	while( nbytes ){
		*(to+1) = *from;
		*(to) = *(from+1);
		to +=2;
		from +=2;
		nbytes -= 2;
	}
}

static int
access( char * path, int mode )
{
	int	fhandl;
	if((
	fhandl = OPEN( path, O_RDONLY )
		) == -1 ){
/*
		printf("path=%s\n", path );
		errmes("access:file does not exist",errno);
*/
		return -1;
	}
	CLOSE( fhandl );
	return 0;
}

#endif

#if defined ( IMAGE_INCLD_TIFF ) || defined (IMAGE_INCLD_JPEG )
static void bip2bndLine( u_char *bip, u_char *bndLine, int nchan, int ichan, int w, int nbit)
{
	int	i;
	int	byt = nbit / 8;
	int	bytnc= byt * nchan;

	for(i=0,bip=bip+ichan*byt; i<w; i++, bip+=bytnc, bndLine+=byt)
		memcpy( bndLine, bip, byt );
}

static void bndLine2bip( u_char *bip, u_char *bndLine, int nchan, int ichan, int w, int nbit)
{
	int	i;
	int	byt = nbit / 8;
	int	bytnc= byt*nchan;

	for(i=0,bip=bip+ichan*byt; i<w; i++, bip+=bytnc, bndLine+=byt)
		memcpy( bip, bndLine, byt );
}
#endif

int haveERMExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ERM_EXTENTION ) )
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

int haveENVIExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ENVI_EXTENTION ) )
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

int haveARCINFOExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ARCINFO_EXTENTION ) )
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

int haveTiffExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ".tiff") || !STRICMP(pt, ".tif" ))
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

#if defined ( IMAGE_INCLD_TIFF )

void image_tif_compress_lzw( IMAGE_FILE	*imgf, int lzwPredictor )
{
	if( imgf->file_header == IMAGE_TIFF ) {
		TIFFSetField(imgf->tiff, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
		if (lzwPredictor != 0)
			TIFFSetField(imgf->tiff, TIFFTAG_PREDICTOR, lzwPredictor);
	} else {
		errmes("image_tif_compress_lzw:file is not tiff file",0);
	}
}

static TIFF	* tiffCreate( IMAGE_FILE  *imgf )
{
	double	resolution = -1;
	int	compression = COMPRESSION_NONE;
	tsize_t linebytes;
	long rowsperstrip = (long) -1;
	int	i;

	imgf->tiff = TIFFOpen(imgf->fullPath, "w");
	if( imgf->tiff == NULL )
		return NULL;
	TIFFSetField(imgf->tiff, TIFFTAG_IMAGEWIDTH,  imgf->w);
	TIFFSetField(imgf->tiff, TIFFTAG_IMAGELENGTH, imgf->h);
	TIFFSetField(imgf->tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
	TIFFSetField(imgf->tiff, TIFFTAG_SAMPLESPERPIXEL, imgf->nchan );
	TIFFSetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, imgf->nbit );
	switch( imgf->data_type ){
	case	IMAGE_CHAR:
/*		TIFFSetField(out, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT );	*/
		TIFFSetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, imgf->nbit );
		break;
	case	IMAGE_USHORT:
	case	IMAGE_ULONG:
		TIFFSetField(imgf->tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT );
		TIFFSetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, imgf->nbit );
		break;
	case	IMAGE_SHORT:
	case	IMAGE_LONG:
		TIFFSetField(imgf->tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT );
		TIFFSetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, imgf->nbit );
		break;
	case	IMAGE_FLOAT:
	case	IMAGE_DOUBLE:
		TIFFSetField(imgf->tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP );
		TIFFSetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, imgf->nbit );
		break;
	}
	TIFFSetField(imgf->tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	if( imgf->nchan == 3 && imgf->data_type == IMAGE_CHAR )
		TIFFSetField(imgf->tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	else
		TIFFSetField(imgf->tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

	TIFFSetField(imgf->tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
/*
	TIFFSetField(imgf->tiff, TIFFTAG_COMPRESSION, compression);
	switch (compression) {
	case COMPRESSION_JPEG:
		TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
		TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
		break;
	case COMPRESSION_LZW:
	case COMPRESSION_DEFLATE:
		if (predictor != 0)
			TIFFSetField(out, TIFFTAG_PREDICTOR, lzwPredictor);
		break;
	}
*/
	linebytes = imgf->nchan * imgf->w * imgf->nbit / 8;
	if (TIFFScanlineSize(imgf->tiff) > linebytes)
		imgf->tiffLineBuf = (unsigned char *)_TIFFmalloc(linebytes);
	else
		imgf->tiffLineBuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(imgf->tiff));
	TIFFSetField(imgf->tiff, TIFFTAG_ROWSPERSTRIP,
	    TIFFDefaultStripSize(imgf->tiff, rowsperstrip));
	if (resolution > 0) {
		TIFFSetField(imgf->tiff, TIFFTAG_XRESOLUTION, resolution);
		TIFFSetField(imgf->tiff, TIFFTAG_YRESOLUTION, resolution);
		TIFFSetField(imgf->tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
	}

	if((imgf->written = malloc( imgf->h ) ) == NULL ){
		fprintf(stderr,"tiffCreate: written buffer allocation error\n");
		exit(1);
	}
	for(i=0;i<imgf->h;i++)
		imgf->written[i] = 0;

	imgf->tiffWriteLineY = 0;
	if((imgf->tiffWriteLineInbuf = malloc( sizeof(int)*imgf->nchan ) ) == NULL ){
		fprintf(stderr,"tiffCreate: tiffWriteLineInbuf allocation error\n");
		exit(1);
	}
	for(i=0;i<imgf->nchan;i++)
		imgf->tiffWriteLineInbuf[i] = 0;

	return imgf->tiff;
}

static TIFF	* tiffOpen( IMAGE_FILE  *imgf, int mode ){

	uint16 photometric;
	uint32 rowsperstrip = (uint32) -1;
	double resolution = -1;
	uint16 samplesperpixel;
	uint16 bitspersample;
	uint16 config;
	unsigned char *buf = NULL;
	tsize_t linebytes;
	uint32 w, h;
	uint16 sampleformat;
	int	i;

	if( mode == IMAGE_RDONLY )
		imgf->tiff = TIFFOpen(imgf->fullPath, "r");
	else if( mode == IMAGE_RDWR )
		imgf->tiff = TIFFOpen(imgf->fullPath, "r+");
	if (imgf->tiff == NULL)
		return NULL;

	photometric = 0;
	TIFFGetField(imgf->tiff, TIFFTAG_PHOTOMETRIC, &photometric);
	if (photometric == PHOTOMETRIC_PALETTE ) {
		fprintf(stderr, "tiffOpen: warning: Tiff file is palette mode\n");
	}

/*
	if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_PALETTE ) {
		fprintf(stderr,
	    "%s: Bad photometric; can only handle RGB and Palette images.\n",
		    infile);
		return (-1);
	}
*/
	TIFFGetField(imgf->tiff, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
	imgf->nchan = samplesperpixel;

//	printf("tiffOpen samplesperpixel %d\n", samplesperpixel );
/*
	if (samplesperpixel != 1 && samplesperpixel != 3) {
		fprintf(stderr, "%s: Bad samples/pixel %u.\n",
		    infile, samplesperpixel);
		return (-1);
	}
*/
	TIFFGetField(imgf->tiff, TIFFTAG_BITSPERSAMPLE, &bitspersample);
	TIFFGetField(imgf->tiff, TIFFTAG_SAMPLEFORMAT,  &sampleformat );
	TIFFGetField(imgf->tiff, TIFFTAG_ROWSPERSTRIP,  &rowsperstrip );
//	printf("tiffOpen bitspersample %d\n", bitspersample );
//	printf("tiffOpen sampleformat %d\n", sampleformat );
//	printf("tiffOpen rowsperstrip %d\n", rowsperstrip );
	if( bitspersample == 8 ){
		switch( sampleformat ){
		case SAMPLEFORMAT_UINT:
			imgf->data_type = IMAGE_CHAR;
			break;
		case SAMPLEFORMAT_INT:
			imgf->data_type = IMAGE_CHAR;
			break;
		default:
			imgf->data_type = IMAGE_CHAR;
			break;
		}
	} else if( bitspersample == 16 ) {
		switch( sampleformat ){
		case SAMPLEFORMAT_UINT:
			imgf->data_type = IMAGE_USHORT;
			break;
		case SAMPLEFORMAT_INT:
			imgf->data_type = IMAGE_SHORT;
			break;
		default:
			imgf->data_type = IMAGE_USHORT;
			break;
		}
	} else if( bitspersample == 32 ) {
		switch( sampleformat ){
		case SAMPLEFORMAT_UINT:
			imgf->data_type = IMAGE_ULONG;
			break;
		case SAMPLEFORMAT_INT:
			imgf->data_type = IMAGE_LONG;
			break;
		case SAMPLEFORMAT_IEEEFP:
			imgf->data_type = IMAGE_FLOAT;
			break;
		default:
			imgf->data_type = IMAGE_ULONG;
			break;
		}
	} else if( bitspersample == 64 ) {
		switch( sampleformat ){
		case SAMPLEFORMAT_IEEEFP:
			imgf->data_type = IMAGE_DOUBLE;
			break;
		case SAMPLEFORMAT_COMPLEXIEEEFP:
			imgf->data_type = IMAGE_COMPLEX;
			break;
		default:
			imgf->data_type = IMAGE_DOUBLE;
			break;
		}
	}
	imgf->nbit = bitspersample;
	TIFFGetField(imgf->tiff, TIFFTAG_IMAGEWIDTH, &w);
	TIFFGetField(imgf->tiff, TIFFTAG_IMAGELENGTH, &h);
	imgf->w = w;	imgf->h = h;
	TIFFGetField(imgf->tiff, TIFFTAG_PLANARCONFIG, &config);
	imgf->tiffLineBuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(imgf->tiff));

	linebytes = imgf->nchan * imgf->w * imgf->nbit / 8;
	if (TIFFScanlineSize(imgf->tiff) > linebytes)
		imgf->tiffLineBuf = (unsigned char *)_TIFFmalloc(linebytes);
	else
		imgf->tiffLineBuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(imgf->tiff));

	if((imgf->written = malloc( imgf->h ) ) == NULL ){
		fprintf(stderr,"tiffOpen: written buffer allocation error\n");
		exit(1);
	}
	for(i=0;i<imgf->h;i++)
		imgf->written[i] = 1;

//	printf("tiffOpen nbit = %d\n", imgf->nbit );
	return imgf->tiff;
}


static int tiff_read_body( IMAGE_FILE *imgf )
{
	int	row, j;
	for( row=0;row<imgf->h;row++){
		if (TIFFReadScanline(imgf->tiff, imgf->tiffLineBuf, row, 0) < 0){
			fprintf(stderr,"tiff_read_body: tiff read error %d\n",row);
			exit(1);
			break;
		}
		for(j=0;j<imgf->nchan;j++)
			bip2bndLine( imgf->tiffLineBuf, imgf->image->data[j][row], imgf->nchan, j, imgf->w, imgf->nbit );
	}
	return 0;
}

static int tiff_write_body( IMAGE_FILE *imgf )
{
	int	row, j;
	for( row=0;row<imgf->h;row++){
		for(j=0;j<imgf->nchan;j++)
			bndLine2bip( imgf->tiffLineBuf, imgf->image->data[j][row], imgf->nchan, j, imgf->w, imgf->nbit );
		if (TIFFWriteScanline(imgf->tiff, imgf->tiffLineBuf, row, 0) < 0){
			fprintf(stderr,"tiff_write_body: tiff write error %d\n",row);
			exit(1);
			break;
		}
		imgf->written[row] = 1;
	}
	return 0;
}
int tiff_image_file_read( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	i, row;
	int	byt = imgf->nbit / 8;
	int	bytnc= byt * imgf->nchan;

	for( i=0, row=ys; i<h; i++,row++){
		if (TIFFReadScanline(imgf->tiff, imgf->tiffLineBuf, row, 0) < 0){
			fprintf(stderr,"tiff_image_file_read: tiff read error %d\n",row);
			exit(1);
			break;
		}
		bip2bndLine( imgf->tiffLineBuf + xs*bytnc,
						buf[i], imgf->nchan, chanl, w, imgf->nbit );
	}
	return 0;
}

int tiff_image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	i;
	int	byt = imgf->nbit / 8;
	int	bytnc= byt * imgf->nchan;

	int	completed;

	if( xs != 0 || w != imgf->w || h != 1  ){
		fprintf( stderr,"tiff_image_file_write: xs != 0 || w != imgf->w || h != 1\n");
		exit(1);
		return 1;
	}
	if( ys != imgf->tiffWriteLineY ){
		fprintf( stderr,"tiff_image_file_write: cannot porceed to other line before complete 1 line %d %d\n", imgf->tiffWriteLineY, ys);
		exit(1);
		return 1;
	}

	imgf->tiffWriteLineInbuf[chanl] = 1;

	bndLine2bip( imgf->tiffLineBuf + xs*bytnc,
						buf[0], imgf->nchan, chanl, w, imgf->nbit );

	completed = 1;
	for( i=0;i<imgf->nchan; i++ ){
		if( imgf->tiffWriteLineInbuf[i] == 0 ) {
			completed = 0;
			break;
		}
	}

	if(completed){
/*		printf( "tiff_image_file_write: writing out %d\n", ys);	*/
		if (TIFFWriteScanline(imgf->tiff, imgf->tiffLineBuf, ys, 0) < 0){
			fprintf(stderr,"tiff_image_file_write: tiff write error %d\n",ys);
			exit(1);
		}
		for( i=0;i<imgf->nchan;i++)
			imgf->tiffWriteLineInbuf[i] = 0;
		imgf->tiffWriteLineY ++;
	}

	return 0;
}

/*
TIFFTAG_ARTIST	1	char**
TIFFTAG_BADFAXLINES	1	uint32*
TIFFTAG_BITSPERSAMPLE	1	uint16*
TIFFTAG_CLEANFAXDATA	1	uint16*
TIFFTAG_COLORMAP	3	uint16**	1<<BitsPerSample arrays
TIFFTAG_COMPRESSION	1	uint16*
TIFFTAG_CONSECUTIVEBADFAXLINES	1	uint32*
TIFFTAG_DATATYPE	1	uint16*
TIFFTAG_DATETIME	1	char**
TIFFTAG_DOCUMENTNAME	1	char**
TIFFTAG_DOTRANGE	2	uint16*
TIFFTAG_EXTRASAMPLES	2	uint16*,uint16**	count & types array
TIFFTAG_FAXMODE	1	int*	G3/G4 compression pseudo-tag
TIFFTAG_FAXFILLFUNC	1	TIFFFaxFillFunc*	G3/G4 compression pseudo-tag
TIFFTAG_FILLORDER	1	uint16*
TIFFTAG_GROUP3OPTIONS	1	uint32*
TIFFTAG_GROUP4OPTIONS	1	uint32*
TIFFTAG_HALFTONEHINTS	2	uint16*
TIFFTAG_HOSTCOMPUTER	1	char**
TIFFTAG_IMAGEDEPTH	1	uint32*
TIFFTAG_IMAGEDESCRIPTION	1	char**
TIFFTAG_IMAGELENGTH	1	uint32*
TIFFTAG_IMAGEWIDTH	1	uint32*
TIFFTAG_INKNAMES	1	char**
TIFFTAG_INKSET	1	uint16*
TIFFTAG_JPEGTABLES	2	u_short*,void**	count & tables
TIFFTAG_JPEGQUALITY	1	int*	JPEG pseudo-tag
TIFFTAG_JPEGCOLORMODE	1	int*	JPEG pseudo-tag
TIFFTAG_JPEGTABLESMODE	1	int*	JPEG pseudo-tag
TIFFTAG_MAKE	1	char**
TIFFTAG_MATTEING	1	uint16*
TIFFTAG_MAXSAMPLEVALUE	1	uint16*
TIFFTAG_MINSAMPLEVALUE	1	uint16*
TIFFTAG_MODEL	1	char**
TIFFTAG_ORIENTATION	1	uint16*
TIFFTAG_PAGENAME	1	char**
TIFFTAG_PAGENUMBER	2	uint16*
TIFFTAG_PHOTOMETRIC	1	uint16*
TIFFTAG_PLANARCONFIG	1	uint16*
TIFFTAG_PREDICTOR	1	uint16*
TIFFTAG_PRIMARYCHROMATICITIES	1	float**	6-entry array
TIFFTAG_REFERENCEBLACKWHITE	1	float**	2*SamplesPerPixel array
TIFFTAG_RESOLUTIONUNIT	1	uint16*
TIFFTAG_ROWSPERSTRIP	1	uint32*
TIFFTAG_SAMPLEFORMAT	1	uint16*
TIFFTAG_SAMPLESPERPIXEL	1	uint16*
TIFFTAG_SMAXSAMPLEVALUE	1	double*
TIFFTAG_SMINSAMPLEVALUE	1	double*
TIFFTAG_SOFTWARE	1	char**
TIFFTAG_STRIPBYTECOUNTS	1	uint32**
TIFFTAG_STRIPOFFSETS	1	uint32**
TIFFTAG_SUBFILETYPE	1	uint32*
TIFFTAG_SUBIFD	2	uint16*,uint32**	count & offsets array
TIFFTAG_TARGETPRINTER	1	char**
TIFFTAG_THRESHHOLDING	1	uint16*
TIFFTAG_TILEBYTECOUNTS	1	uint32**
TIFFTAG_TILEDEPTH	1	uint32*
TIFFTAG_TILELENGTH	1	uint32*
TIFFTAG_TILEOFFSETS	1	uint32**
TIFFTAG_TILEWIDTH	1	uint32*
TIFFTAG_TRANSFERFUNCTION	1 or 3\(dg	uint16**	1<<BitsPerSample entry arrays
TIFFTAG_WHITEPOINT	1	float**	2-entry array
TIFFTAG_XPOSITION	1	float*
TIFFTAG_XRESOLUTION	1	float*
TIFFTAG_YCBCRCOEFFICIENTS	1	float**	3-entry array
TIFFTAG_YCBCRPOSITIONING	1	uint16*
TIFFTAG_YCBCRSUBSAMPLING	2	uint16*
TIFFTAG_YPOSITION	1	float*
TIFFTAG_YRESOLUTION	1	float*
*/
#endif

int haveJPEGExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ".jpg") || !STRICMP(pt, ".JPG" ))
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

#if defined ( IMAGE_INCLD_JPEG )

void image_jpeg_quality( IMAGE_FILE	*imgf, int jpegQuality )
{
	((JPG*)(imgf->jpg))->jpegQuality = jpegQuality;
}
void image_jpeg_default_quality_set( int quality )
{
	jpeg_default_quality = quality;
}

static int jpegCreate( IMAGE_FILE  *imgf )
{
	long	linebytes;
	int	i;

	((JPG*)(imgf->jpg))->jpegCompressDir = JPEG_COMPRESS;
	((JPG*)(imgf->jpg))->cinfo.err = jpeg_std_error(&((JPG*)(imgf->jpg))->jerr);

	jpeg_create_compress(&((JPG*)(imgf->jpg))->cinfo);

	if ((((JPG*)(imgf->jpg))->jpegFp = fopen(imgf->fullPath, "wb")) == NULL) {
		fprintf(stderr, "jpegCreat: can't open %s\n", imgf->fname);
		return 1;
	}
	jpeg_stdio_dest(&((JPG*)(imgf->jpg))->cinfo, ((JPG*)(imgf->jpg))->jpegFp);
	((JPG*)(imgf->jpg))->cinfo.image_width = imgf->w; 	/* image width and height, in pixels */
	((JPG*)(imgf->jpg))->cinfo.image_height = imgf->h;
	((JPG*)(imgf->jpg))->cinfo.input_components = imgf->nchan;		/* # of color components per pixel */
	if( imgf->nbit == 8 && imgf->nchan == 3 ){
		((JPG*)(imgf->jpg))->cinfo.input_components = 3;
		((JPG*)(imgf->jpg))->cinfo.in_color_space = JCS_RGB;
	} else if( imgf->nbit == 8 && imgf->nchan == 1 ) {
		((JPG*)(imgf->jpg))->cinfo.input_components = 1;
		((JPG*)(imgf->jpg))->cinfo.in_color_space = JCS_GRAYSCALE;
	} else if( imgf->nbit == 24 && imgf->nchan == 1 ) {
		((JPG*)(imgf->jpg))->cinfo.input_components = 3;
		((JPG*)(imgf->jpg))->cinfo.in_color_space = JCS_RGB;
	} else {
		((JPG*)(imgf->jpg))->cinfo.input_components = imgf->nchan*imgf->nbit/8;
		((JPG*)(imgf->jpg))->cinfo.in_color_space = JCS_UNKNOWN;
	}

	jpeg_set_defaults(&((JPG*)(imgf->jpg))->cinfo);

	jpeg_set_quality(&((JPG*)(imgf->jpg))->cinfo, ((JPG*)(imgf->jpg))->jpegQuality, TRUE /* limit to baseline-JPEG values */);

	jpeg_start_compress(&((JPG*)(imgf->jpg))->cinfo, TRUE);

	linebytes = imgf->nchan * imgf->w * imgf->nbit / 8;

  	((JPG*)(imgf->jpg))->jpegLineBuf = malloc( linebytes );
	if( ((JPG*)(imgf->jpg))->jpegLineBuf == NULL ){
		fprintf( stderr,"jpegCreate: Line Buffer Allocation Error\n");
		return 1;
	}
	((JPG*)(imgf->jpg))->jpeg_row_pointer[0] = ((JPG*)(imgf->jpg))->jpegLineBuf;

	if((((JPG*)(imgf->jpg))->jpegWriteLineInbuf = malloc( sizeof(int)*imgf->nchan ) ) == NULL ){
		fprintf(stderr,"jpegCreate: jpegWriteLineInbuf allocation error\n");
		exit(1);
	}
	for(i=0;i<imgf->nchan;i++)
		((JPG*)(imgf->jpg))->jpegWriteLineInbuf[i] = 0;

	return 0;
}

static int jpegOpen( IMAGE_FILE  *imgf, int mode ){

	long	linebytes;

	((JPG*)(imgf->jpg))->jpegCompressDir = JPEG_DECOMPRESS;
	if( mode == IMAGE_RDONLY ) {
		if ((((JPG*)(imgf->jpg))->jpegFp = fopen(imgf->fullPath, "rb")) == NULL) {
			fprintf(stderr, "jpegOpen: can't open %s\n", imgf->fname);
			return 1;
		}
	} else {
		fprintf(stderr,"jpegOpen: illegal open mode %d\n", mode);
		return 1;
	}

	((JPG*)(imgf->jpg))->dcinfo.err = jpeg_std_error(&((JPG*)(imgf->jpg))->jerr);

	jpeg_create_decompress(&((JPG*)(imgf->jpg))->dcinfo);

	jpeg_stdio_src(&((JPG*)(imgf->jpg))->dcinfo, ((JPG*)(imgf->jpg))->jpegFp);

	(void) jpeg_read_header(&((JPG*)(imgf->jpg))->dcinfo, TRUE);

	(void) jpeg_start_decompress(&((JPG*)(imgf->jpg))->dcinfo);


	imgf->w = ((JPG*)(imgf->jpg))->dcinfo.output_width;
	imgf->h = ((JPG*)(imgf->jpg))->dcinfo.output_height;
	imgf->nchan = ((JPG*)(imgf->jpg))->dcinfo.output_components;
	imgf->data_type = IMAGE_CHAR;
	imgf->nbit = 8;

	linebytes = imgf->w * imgf->nchan * imgf->nbit/8;

  	((JPG*)(imgf->jpg))->jpegLineBuf = malloc( linebytes );
	if( ((JPG*)(imgf->jpg))->jpegLineBuf == NULL ){
		fprintf( stderr,"jpegOpen: Line Buffer Allocation Error\n");
		return 1;
	}
	((JPG*)(imgf->jpg))->jpeg_row_pointer[0] = ((JPG*)(imgf->jpg))->jpegLineBuf;
	((JPG*)(imgf->jpg))->jpegLineBufNo = -1;
	return 0;
}
static int jpegClose( IMAGE_FILE	*imgf )
{
	if( ((JPG*)(imgf->jpg))->jpegCompressDir == JPEG_COMPRESS ){
		jpeg_finish_compress(&((JPG*)(imgf->jpg))->cinfo);
		fclose(((JPG*)(imgf->jpg))->jpegFp);
		jpeg_destroy_compress(&((JPG*)(imgf->jpg))->cinfo);

	} else{
//		fprintf(stderr, "jpegClose: calling jpeg_finish_decompress\n");
		if( imgf->mode != IMAGE_RDONLY )
			(void) jpeg_finish_decompress(&((JPG*)(imgf->jpg))->dcinfo);
//		fprintf(stderr, "jpegClose: calling jpeg_destroy_decompress\n");
		jpeg_destroy_decompress(&((JPG*)(imgf->jpg))->dcinfo);

		fclose(((JPG*)(imgf->jpg))->jpegFp);

	}
	free(((JPG*)(imgf->jpg))->jpegLineBuf);
	((JPG*)(imgf->jpg))->jpegFp = NULL;

	return 0;

}

static int jpeg_read_body( IMAGE_FILE *imgf )
{
	int	row=0, j;
	while (((JPG*)(imgf->jpg))->dcinfo.output_scanline < ((JPG*)(imgf->jpg))->dcinfo.output_height) {

		(void) jpeg_read_scanlines(&((JPG*)(imgf->jpg))->dcinfo, ((JPG*)(imgf->jpg))->jpeg_row_pointer, 1);

		for(j=0;j<imgf->nchan;j++)
			bip2bndLine( ((JPG*)(imgf->jpg))->jpegLineBuf, imgf->image->data[j][row], imgf->nchan, j, imgf->w, imgf->nbit );

		row++;

	}
	return 0;
}

static int jpeg_write_body( IMAGE_FILE *imgf )
{
	int	row, j;

	for( row=0;row<imgf->h;row++){
		for(j=0;j<imgf->nchan;j++)
			bndLine2bip( ((JPG*)(imgf->jpg))->jpegLineBuf, imgf->image->data[j][row], imgf->nchan, j, imgf->w, imgf->nbit );

		(void) jpeg_write_scanlines(&((JPG*)(imgf->jpg))->cinfo, ((JPG*)(imgf->jpg))->jpeg_row_pointer, 1);

	}
	return 0;
}

int jpeg_image_file_read( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	byt = imgf->nbit / 8;
	int	bytnc= byt * imgf->nchan;

	if( xs != 0 || w != imgf->w || h != 1  ){
		fprintf( stderr,"jpeg_image_file_read: xs != 0 || w != imgf->w || h != 1\n");
		exit(1);
		return 1;
	}
	if( ys == ((JPG*)(imgf->jpg))->jpegLineBufNo ){
		bip2bndLine( ((JPG*)(imgf->jpg))->jpegLineBuf + xs*bytnc,
						buf[0], imgf->nchan, chanl, w, imgf->nbit );
	} else if( ys == (int) ((JPG*)(imgf->jpg))->dcinfo.output_scanline ){
		((JPG*)(imgf->jpg))->jpegLineBufNo = ((JPG*)(imgf->jpg))->dcinfo.output_scanline;
		(void) jpeg_read_scanlines(&((JPG*)(imgf->jpg))->dcinfo, ((JPG*)(imgf->jpg))->jpeg_row_pointer, 1);
		bip2bndLine( ((JPG*)(imgf->jpg))->jpegLineBuf + xs*bytnc,
						buf[0], imgf->nchan, chanl, w, imgf->nbit );
	} else {
		fprintf( stderr,"jpeg_image_file_read: cannot proceed to other line before complete 1 line %d %d\n", ((JPG*)(imgf->jpg))->dcinfo.output_scanline, ys);
		exit(1);
		return 1;
	}
	return 0;
}

int jpeg_image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	i;
	int	byt = imgf->nbit / 8;
	int	bytnc= byt * imgf->nchan;

	int	completed;

	if( xs != 0 || w != imgf->w || h != 1  ){
		fprintf( stderr,"jpeg_image_file_write: xs != 0 || w != imgf->w || h != 1\n");
		exit(1);
		return 1;
	}
	if( ys != (int) ((JPG*)(imgf->jpg))->cinfo.next_scanline ){
		fprintf( stderr,"jpeg_image_file_write: cannot proceed to other line before complete 1 line %d %d\n", ((JPG*)(imgf->jpg))->cinfo.next_scanline, ys);
		exit(1);
		return 1;
	}

	((JPG*)(imgf->jpg))->jpegWriteLineInbuf[chanl] = 1;

	bndLine2bip( ((JPG*)(imgf->jpg))->jpegLineBuf + xs*bytnc,
						buf[0], imgf->nchan, chanl, w, imgf->nbit );

	completed = 1;
	for( i=0;i<imgf->nchan; i++ ){
		if( ((JPG*)(imgf->jpg))->jpegWriteLineInbuf[i] == 0 ) {
			completed = 0;
			break;
		}
	}

	if(completed){

		(void) jpeg_write_scanlines(&((JPG*)(imgf->jpg))->cinfo, ((JPG*)(imgf->jpg))->jpeg_row_pointer, 1);

	/*	printf("image_jpeg_file_write: nextline = %d\n", ((JPG*)(imgf->jpg))->cinfo.next_scanline); */
		for( i=0;i<imgf->nchan;i++)
			((JPG*)(imgf->jpg))->jpegWriteLineInbuf[i] = 0;
	}

	return 0;
}

#endif
int haveBMPExtention ( char *fname )
{
	char	*pt;
	pt= fname + strlen( fname );
	while( pt > fname ){
		if( *pt == '.' ) {
			if( !STRICMP( pt, ".bmp") || !STRICMP(pt, ".BMP" ))
				return TRUE;
			else
				return FALSE;
		}
		pt--;
	}
	return FALSE;
}

#if defined( IMAGE_INCLD_BMP )
static int bmpOpen2( IMAGE_FILE  *imgf, int mode ){

	BMP	*bmp;

	if( mode == IMAGE_RDONLY ) {
		bmp = imgf->bmp = bmpOpen( imgf->fullPath );
		if( bmp == NULL ){
			fprintf(stderr,"bmpOpen2: bmpOpen error: %s\n", imgf->fullPath);
			return 1;
		}
	} else {
		fprintf(stderr,"bmpOpen2: support only IMAGE_RDONLY\n", mode);
		return 1;
	}

	imgf->w = bmp->w;
	imgf->h = bmp->h;
	imgf->nchan = bmp->bitCount/8;
	imgf->data_type = IMAGE_CHAR;
	imgf->nbit = 8;

	return 0;
}
static int bmpClose2( IMAGE_FILE  *imgf )
{
	bmpClose( imgf->bmp );
	imgf->bmp = NULL;
	return 0;
}
static int bmp_read_body( IMAGE_FILE  *imgf )
{
	int	i, j;
	BMP	*bmp;

	bmp = imgf->bmp;
	for (i = 0; i < imgf->h; i++) {
		bmpReadLine( bmp, i );
		bmpSwapBR( bmp->lineBuf, bmp->w, bmp->bitCount );
		for(j=0;j<imgf->nchan;j++)
			bip2bndLine( bmp->lineBuf, imgf->image->data[j][i], imgf->nchan, j, imgf->w, imgf->nbit );
	}
	return 0;
}
int bmp_image_file_read( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf )
{
	int	i, row;

	BMP	*bmp = imgf->bmp;
	int	byt = imgf->nbit / 8;
	int	bytnc= byt * ((BMP*)(imgf->bmp))->bitCount/8;

	for( i=0, row=ys; i<h; i++,row++){
		bmpReadLine( bmp, i );
		bmpSwapBR( bmp->lineBuf, bmp->w, bmp->bitCount );

		bip2bndLine( bmp->lineBuf + bytnc, buf[i], imgf->nchan, chanl, w, imgf->nbit );

	}
	return 0;
}
#endif

