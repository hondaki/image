#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "image.h"
#include "imgio.h"

#define H		256
#define W		1024
#define	NCHAN	3

#define	PI 3.14159265

void	img_setup(u_char***pix, int w, int h, int nchan);
void	img_setupw(u_char***pix, int w, int h, int nchan);

int main(void)
{
	IMAGE		*img;
	IMAGE_FILE	*imgf;

	image_file_default_interleave_set( IMAGE_BIL);
	
	printf("calling: image_alloc\n");	
	if((
	img = image_alloc( W, H, IMAGE_CHAR, NCHAN )
	) == NULL ){
		fprintf(stderr, "image allocation error\n");
		exit(1);
	}

	printf("calling: img_setup\n");	
	img_setupw( img->data, img->w, img->h, img->nchan );

	printf("calling: image_file_create\n");	
	if(( imgf = image_file_create("hsiLegend.jpg", IMAGE_TRUNC, 0, img )
		) == NULL ){
		printf("image_file_create error\n");
		exit(1);
	}
	printf("calling: image_file_close\n");	
	image_file_close( imgf );
		
	printf("calling: image_destroy\n");	
	image_destroy( img );

}

void color_gen( double f, double *hsi, double *rgb )
{

	double	hsi0, hsi1, rotation;

	int	step=16;

	double	*rgbwk;
	hsi0 =  2*PI* 0.05;
	hsi1 =  2*PI* 2./3.;
	rotation =  1.;

	f = (int)(f * step-0.00001) / ((double) step-1);
	f = f-sin( f*2*PI )*0.05;

	if( rotation > 0 ){
		if( hsi1 < hsi0 )
			hsi1 += 2*PI;
	} else {
		if( hsi1 > hsi0 )
			hsi1 -= 2*PI;
	}

	hsi[0] = ( hsi1 - hsi0 ) * f + hsi0;
	if( f < 0.5 ){
		hsi[1] = 0.5 + f*0.5;
		hsi[1] = 0.7 - f*0.3;
	} else {
		hsi[1] = 0.5 + 0.5 - (f-0.5)*0.5;
		hsi[1] = 0.4 +     + (f-0.5)*0.3;
	}
//	hsi[1] = 1.0;
	if( f < 0.5 ){
		hsi[2] = 0.3 + f*f*f/(0.5*0.5*0.5) * 0.6;
	} else {
		hsi[2] = 0.3 + 0.5 + (0.5-f)*(0.5-f)*(0.5-f)/(0.5*0.5*0.5)*0.5;
	}
//	hsi[2] = 0.5;
	

	printf("%f %f %f %f\n",f, hsi[0],hsi[1], hsi[2] );

	rgbwk = to_rgb( hsi ); 
//	printf("%f %f %f %f\n",f, rgbwk[0],rgbwk[1], rgbwk[2] );

	rgb[0] = rgbwk[0];
	rgb[1] = rgbwk[1];
	rgb[2] = rgbwk[2];

}

void img_setupw( u_char ***pix, int w, int h, int nchan )
{
	int	i, j, ichan;

	double	f;
	double	hsi[3], rgb[3];

	for(j=0;j<w;j++) {
		f = (double) j/(w-1);
		color_gen( f, hsi, rgb );

		for(i=0;i<h;i++) {
			pix[0][i][j] = rgb[0]*255;
			pix[1][i][j] = rgb[1]*255;
			pix[2][i][j] = rgb[2]*255;
		}
	}
}
void img_setup( u_char ***pix, int w, int h, int nchan )
{
	int	i, j, ichan;

	double	f;
	double	hsi[3], *rgb;

	for(ichan=0;ichan<nchan;ichan++){
		printf("img_setup: channel %d\n", ichan );
		for(i=0;i<h;i++) {
/*
			f = (double) (h-i) / h;
			hsi[0] = f*2*PI;
			hsi[1] = 1.0;
			hsi[2] = 0.5;
*/
		f = (double) (h-1-i) / h;
		f = ((int) (f*9)+0.5 )/9.0;
		if( f < 0.3333 ){
			hsi[0] = 0.+f/0.3333*2*PI/10;
			hsi[1] = 1.-f/0.3333*0.5;
			hsi[2] = 0.5;
		} else if( f < 0.6667 ) {
			hsi[0] = 2*PI/3.0+(f-0.5)/0.3333*2*PI/5.;
			hsi[1] = 0.4;
			hsi[2] = 0.5;
		} else {
			hsi[0] = 2*PI/3.0*2.-(1.0-f)/0.3333*2*PI/10;
			hsi[1] = 0.5-(1.0-f)/0.3333*0.5;
//			hsi[1] = 0.5;
			hsi[2] = 0.5;
		}
			
			rgb = to_rgb( hsi ); 

			for(j=0;j<w;j++) {
				pix[0][i][j] = rgb[0]*255;
				pix[1][i][j] = rgb[1]*255;
				pix[2][i][j] = rgb[2]*255;
			}
		}
	}
}
