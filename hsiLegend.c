#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "image.h"
#include "imgio.h"

#define W		256
#define H		1024
#define	NCHAN	3

void	img_setup(u_char***pix, int w, int h, int nchan);

int main(void)
{
	IMAGE		*img;
	IMAGE_FILE	*imgf;

	image_file_default_interleave_set( IMAGE_BIL);
	
	printf("calling: image_alloc\n");	
	if((
	img = image_alloc( W, H, IMAGE_CHAR, NCHAN )
	) == NULL ){
		fprintf(stderr, "image allocation error\n");
		exit(1);
	}

	printf("calling: img_setup\n");	
	img_setup( img->data, img->w, img->h, img->nchan );

	printf("calling: image_file_create\n");	
	if(( imgf = image_file_create("hsiLegend.jpg", IMAGE_TRUNC, 0, img )
		) == NULL ){
		printf("image_file_create error\n");
		exit(1);
	}
	printf("calling: image_file_close\n");	
	image_file_close( imgf );
		
	printf("calling: image_destroy\n");	
	image_destroy( img );

}

#define	PI 3.14159265
void img_setup( u_char ***pix, int w, int h, int nchan )
{
	int	i, j, ichan;

	double	f;
	double	hsi[3], *rgb;

	for(ichan=0;ichan<nchan;ichan++){
		printf("img_setup: channel %d\n", ichan );
		for(i=0;i<h;i++) {
/*
			f = (double) (h-i) / h;
			hsi[0] = f*2*PI;
			hsi[1] = 1.0;
			hsi[2] = 0.5;
*/
		f = (double) (h-1-i) / h;
		f = ((int) (f*9)+0.5 )/9.0;
		if( f < 0.3333 ){
			hsi[0] = 0.+f/0.3333*2*PI/10;
			hsi[1] = 1.-f/0.3333*0.5;
			hsi[2] = 0.5;
		} else if( f < 0.6667 ) {
			hsi[0] = 2*PI/3.0+(f-0.5)/0.3333*2*PI/5.;
			hsi[1] = 0.4;
			hsi[2] = 0.5;
		} else {
			hsi[0] = 2*PI/3.0*2.-(1.0-f)/0.3333*2*PI/10;
			hsi[1] = 0.5-(1.0-f)/0.3333*0.5;
//			hsi[1] = 0.5;
			hsi[2] = 0.5;
		}
			
			rgb = to_rgb( hsi ); 

			for(j=0;j<w;j++) {
				pix[0][i][j] = rgb[0]*255;
				pix[1][i][j] = rgb[1]*255;
				pix[2][i][j] = rgb[2]*255;
			}
		}
	}
}
