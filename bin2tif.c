#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"image.h"
#include"imgio.h"

#define	PROGRAM	"JDEMConv"

/*
	This program convert 1 channel flat binary image to tif
*/

#define	MFNAME	1024

char	*fNameI=NULL;
char	fNameO[MFNAME];

IMAGE_FILE	*imgFI, *imgFO;
IMAGE		*imgI, *imgO;

int	nchan, h, w;

IMAGE	*bufImg;

void usage(void)
{
	printf( "usage: " PROGRAM " [-help] fileName -h height -w width\n");
//	printf( "usage: " PROGRAM " [-help] fileName -h height -w width -nch nchan -dt [BSQ|BIL]\n");
}

int main( int argc, char **argv )
{
	
	int	i, ichan;
	int	h, w, nchan=1;
	int	data_type = IMAGE_CHAR;

	h = w = 0;

	while( --argc ){
		++argv;
		if( strcmp(argv[0], "-help") == 0 ){
			usage();
			exit(0);
		} else if( strcmp( argv[0], "-h") == 0 ){
			++argv; --argc;
			if( !argc ){
				usage();
				exit(0);
			}
			h = atoi( argv[0] );
		} else if( strcmp( argv[0], "-w") == 0 ){
			++argv; --argc;
			if( !argc ){
				usage();
				exit(0);
			}
			w = atoi( argv[0] );
		} else {
			fNameI = argv[0];
		}
	}


/*
	if( getWH( "cntl.dat", &nchan, &h, &w ) ){
		fprintf(stderr, "imgDiv: getWH\n");
		return -1;
	}
*/
	printf("nchan, h, w = %d %d %d\n", nchan, h, w );
	if( h==0 || w == 0 || !fNameI){
		usage();
		exit(0);
	}

	bufImg =  image_alloc( w, 1, data_type, nchan );
	if( !bufImg ){
		fprintf(stderr, PROGRAM ": buffer image %s allocation error %d %d\n",  fNameI, w, nchan );
			return -1;
	}

	imgFI = image_file_open_bin( fNameI, IMAGE_RDONLY, IMAGE_NO_BUF, w, h, data_type, nchan, IMAGE_BSQ );
	if( imgFI == NULL ){
		fprintf(stderr, "image open bin error %s\n", fNameI );
		return -1;
	}

	strcpy( fNameO, fNameI );
	strcat( fNameO, ".tif" );
	imgO = image_skelton( w, h, data_type, nchan );
	imgFO = image_file_create( fNameO, IMAGE_TRUNC, 0, imgO );
 
	for( i=0; i<h; i++ ){
		for(ichan=0; ichan < nchan; ichan++ ){
			if( image_file_read( imgFI, 0, i, w, 1, ichan, bufImg->data[ichan] ) ){
				fprintf(stderr, PROGRAM ": image_file_read error %s: line=%d channel = %d\n", fNameI, i, ichan );
				return -1;
			}
		}
		for(ichan=0; ichan < nchan; ichan++ ){
			if( image_file_write( imgFO, 0, i, w, 1, ichan, bufImg->data[ichan] ) ) {
				fprintf(stderr, PROGRAM ": image_file_write error %s: line=%d channel = %d\n", fNameO, i, ichan );
				return -1;
			}
		}
	}
	image_file_close( imgFO );
	image_destroy( imgO );
	image_file_close( imgFI );
	image_destroy( bufImg );
	return 0;
}
