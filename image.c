/*
	image.c : image handling routines

	Copyright(C)2011 HONDA Kiyoshi

  	Contact: Dr. HONDA Kiyoshi
			P.O.Box 4, Khlong Luang, Pathumthani, 12120, Thailand
			Asian Institute of Technology

			e-mail: honda@ait.ac.th
			http://www.rsgis.ait.ac.th/~honda

If you wish to use or distribute this software in a way
which does not follow the license stated below, you must obtain
written agreement from the copyright holder(s) above.

-------------------------------------------------------------------------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Program you will develop using this software must use the same
license with this software which is GNU General Public License
for any distribution.

The latest version of GNU General Public License may be obtained
from http://www.gnu.org/licenses

*/

/*
 *	Modification History						*
 *									*
 *	12 Oct	2003	Remove DOS WIN16 and MAC options
 *	10 Oct	2003	Geo-Registration Information Added to IMAGE
 *	29 June 2003	pixel spacing information added	dx, dy
 *	May 1996	Extention for Win32 by H.Ishibashi		*
 *	May 1996	Remove old function definition by H.Ishibashi	*
 *	Mar 1995	Extention for compress files by H.Ishibashi	*
 *	Oct 1994	Extention for Windows by H.Ishibashi		*
 *	Jan 1994	Extention for MS-DOS by H.Ishibashi		*
 *									*
 *									*/

#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#define _LARGEFILE64_SOURCE

#include"image.h"

#include"stdio.h"
#include"stdlib.h"
#include"string.h"
#if !defined( LINUX )
#include"malloc.h"
#endif
#include"memory.h"
#include"sys/types.h"

# define MALLOC(p)			malloc((size_t)p)
# define FREE(p)			free(p)
# define MEMCPY(p1,p2,c)	memcpy(p1,p2,(size_t)(c))
# define MEMCMP(p1,p2,c)	memcmp(p1,p2,(size_t)(c))

static int alloc_image_mode = BIL_CONT_IMAGE;
//static int alloc_image_mode = BSQ_CONT_IMAGE;

/* static functions definition */
//static long long image_offset( IMAGE *img, int x, int y, int chanl);
static int errmes( char *message, int erno );
//static int errmesSys( char *message, int erno );

static	int	img_err_exit_sw = 1;
static	int	img_err_exit_pushed = 0;

int	img_err_exit_set( int sw )
{
	int	swPrev;
	swPrev = img_err_exit_sw;
	if( sw )
		img_err_exit_sw = 1;
	else
		img_err_exit_sw = 0;

	return swPrev;
}

void	img_err_exit_push( void )
{
	img_err_exit_pushed++;
}

void	img_err_exit_pop( void )
{
	img_err_exit_pushed--;
}

void	img_err_exit( char *message, int code )
{
	fprintf(stderr, "%s: ", message);
	if( img_err_exit_pushed == 0 && img_err_exit_sw ){
		fprintf(stderr, "exit: %d\n", code);
		exit( code );
	} else {
		fprintf(stderr, "return\n");
		return;
	}
}

IMAGE *new_image( int w, int h, int nchan, int data_type)
{
	return image_alloc( w, h, data_type, nchan );
}

IMAGE *
image_alloc( int w, int h, int data_type, int nchan )
{
	IMAGE * img;

	img_err_exit_push();

	if((
	img = image_skelton(w,h,data_type,nchan)
		) == NULL ) {
		fprintf(stderr, "image_alloc: image_skelton: error: %d %d %d %d\n",
			w, h, data_type, nchan );
		img_err_exit_pop(); img_err_exit( "image_alloc", 1 );
		return NULL;
	}

	if((
	img->data = alloc_image_data( img->md_linebytes, h, nchan )
		) == NULL ) {
		image_destroy( img );
//		fprintf(stderr, "image_alloc: alloc_image_data_error %lld %d %d\n",
		fprintf(stderr, "image_alloc: alloc_image_data_error %d %d %d\n",
			img->md_linebytes, h, nchan );
		img_err_exit_pop(); img_err_exit( "image_alloc", 1 );
		return NULL;
	}
	img->alloc_image_mode = alloc_image_mode;

	img->md_primary = TRUE;

	img_err_exit_pop();
	return img;
}


IMAGE *
image_point( int w, int h, int data_type, int nchan, u_char ***data )
{
	IMAGE * img;

	img_err_exit_push();

	if((
	img = image_skelton(w,h,data_type,nchan)
		) == NULL ) {
		fprintf(stderr, "image_point: image_skelton: error: %d %d %d %d\n",
			w, h, data_type, nchan );
		img_err_exit_pop(); img_err_exit( "image_point", 1 );
 		return NULL;
	}

	img->data = data;
	img->md_primary = FALSE;

	img_err_exit_pop();
	return img;
}


void
image_point_set( IMAGE *img, u_char ***data )
{
	if( img->md_primary == TRUE )
		free_image_data( img );
	img->data = data;
	img->md_primary = FALSE;
}


IMAGE *
image_skelton( int w, int h, int data_type, int nchan )
{
	IMAGE *img;

	img_err_exit_push();
	if((
	img = (IMAGE * ) MALLOC( sizeof( IMAGE ) )
		) == NULL ) {

		img_err_exit_pop(); img_err_exit( "image_skelton: malloc error", 1 );
		return NULL;
	}
	img->w = w;
	img->h = h;
	img->nchan = nchan;
	img->data_type = data_type;
	if((
	img->nbit = image_nbit_decide( data_type )
		) <= 0 ){
		FREE( img );
		img_err_exit_pop(); img_err_exit( "image_skelton: image_nbit_decide error", 1 );
		return NULL;
	}
	img->md_linebytes = 
		image_size(img->nbit,img->w,1,1);
	img->md_channelbytes = 
		img->md_linebytes * img->h;
	img->size = img->max_size =
		img->md_channelbytes * img->nchan;
	img->md_primary = FALSE;
	img->data = NULL;
	initGeoRegistrationInfo ( &img->geoRegistration );
	initAttr( &img->attr );

	img_err_exit_pop();
	return img;
}

void initGeoRegistrationInfo( IMAGE_GEOREGISTRATION_INFO * geoRegistration )
{
	geoRegistration->projection = IMAGE_PROJECTION_NONE;
	geoRegistration->datum = IMAGE_DATUM_NONE;
	geoRegistration->geoRegistrationMapX=0;
	geoRegistration->geoRegistrationMapY=500000;
	geoRegistration->geoRegistrationX=0;
	geoRegistration->geoRegistrationY=0;
	geoRegistration->dx = -1.;
	geoRegistration->dy = -1.;
	geoRegistration->Unit = IMAGE_UNITMETER;
	geoRegistration->datumName[0] = 0;
	geoRegistration->UTMZoneNumber = 52;
	geoRegistration->UTMNorthSouth = IMAGE_UTMNORTH;
}

void initAttr( IMAGE_ATTR * attr )
{
	attr->haveNoDataValue = FALSE;
	attr->noDataValue = -999.;
}

void
image_destroy( IMAGE *img )
{
	if( img->md_primary == TRUE ){
		free_image_data( img );
		img->md_primary = FALSE;
	}
	FREE( img );
}


int
alloc_image_mode_set( int mode )
{
	switch( mode ){
	case LINE_CONT_IMAGE:
	case BSQ_ALLCONT_IMAGE:
	case BIL_ALLCONT_IMAGE:
	case BSQ_CONT_IMAGE:
	case BIL_CONT_IMAGE:
		alloc_image_mode = mode;
		break;
	default:
		errmes("alloc_image_mode_set:illegal mode",0);
		return -1;
		/* break; */
	}
	return 0;
}


u_char***
alloc_image_data( u_int linebytes, int h, int nchan )
{
	int	ichan,i,j;
	u_char	***data, *cltmpl;
	u_char	*pt;


	linebytes = ((linebytes-1)/ALIGN+1)*ALIGN;

	if((
	data = (u_char ***)MALLOC( sizeof( u_char ** ) * nchan )
		) == NULL ) return NULL;

	for(ichan=0; ichan<nchan; ichan++ ){
		if((
		data[ichan] = (u_char **)MALLOC( sizeof( u_char * ) * h )
		) == NULL ) {
			for( i=0; i<ichan; i++)
				FREE( data[i] );
			FREE( data );
			return NULL;
		}
	}
	switch( alloc_image_mode ){
	case	BSQ_ALLCONT_IMAGE:
		if((
		data[0][0] = ( u_char * ) MALLOC( (long long)(linebytes)*(long long)(h)*(long long)(nchan) )
		) == NULL ) {
			for( i=0; i<nchan; i++ )
				FREE( data[i] );
			FREE( data );
			return NULL;
		}
		pt = data[0][0];
		for(j=0;j<nchan;j++){
			for(i=0;i<h;i++){
				data[j][i] = pt;
				pt += linebytes;
			}
		}
		break;

	case	BIL_ALLCONT_IMAGE:
		if((
		data[0][0] = ( u_char * ) MALLOC( (long long)(linebytes)*(long long)(h)*(long long)(nchan) )
		) == NULL ) {
			for( i=0; i<nchan; i++ )
				FREE( data[i] );
			FREE( data );
			return NULL;
		}
		pt = data[0][0];
		for(i=0;i<h;i++){
			for(j=0;j<nchan;j++){
				data[j][i] = pt;
				pt += linebytes;
			}
		}
		break;

	case	BIL_CONT_IMAGE:
		for(i=0;i<h;i++){
			if((
			data[0][i] = ( u_char * ) MALLOC( linebytes*nchan )
			) == NULL ) {
				for(j=0;j<i;j++)
					FREE( data[0][j] );
				for(j=0; j<nchan; j++ )
					FREE( data[j] );
				FREE( data );
				return NULL;
			}
		}
		for(i=0;i<h;i++){
			for(j=1;j<nchan;j++){
				data[j][i] = data[j-1][i] + linebytes;
			}
		}
		break;

	case	BSQ_CONT_IMAGE:
	default:
		for(ichan=0; ichan<nchan; ichan++ ){
			if((
			data[ichan][0] = ( u_char * ) MALLOC( linebytes*h )
			) == NULL ) {
				for( i=0; i<ichan; i++ )
					FREE( data[i][0] );
				for( i=0; i<nchan; i++ )
					FREE( data[i] );
				FREE( data );
				return NULL;
			}
		}
		for(ichan=0; ichan<nchan; ichan++ ){
			for(i=1; i<h; i++){
				data[ichan][i] = data[ichan][i-1]
						+linebytes;
			}
		}
		break;
	}
	if( ( cltmpl = ( u_char * )malloc( linebytes ) ) == NULL ){
		for(ichan=0; ichan<nchan; ichan++ )
			for(i=0; i<h; i++)
				for(j=0;j<(int)linebytes;j++ )
					data[ichan][i][j] = 0;
	} else {
		for(j=0;j<(int)linebytes;j++ )
			cltmpl[j] = 0;
		for(ichan=0; ichan<nchan; ichan++ )
			for(i=0; i<h; i++)
				memcpy( data[ichan][i], cltmpl, linebytes );
		FREE( cltmpl );
	}
	return data;
}


int
free_image_data( IMAGE *img )
{
	int	i;

	switch( img->alloc_image_mode ){
	case BIL_ALLCONT_IMAGE:
	case BSQ_ALLCONT_IMAGE:
		FREE( img->data[0][0] );
		break;
	case BIL_CONT_IMAGE:
		for(i=0;i<img->h;i++){
			FREE( img->data[0][i] );
		}
		break;
	case BSQ_CONT_IMAGE:
	default:
		for(i=0;i<img->nchan;i++){
			FREE( img->data[i][0] );
		}
		break;
	}
	for(i=0;i<img->nchan;i++)
		FREE( img->data[i] );
	FREE( img->data );
	img->data = NULL;
	return 0;
}


int
image_nbit_decide( int data_type )
{
 /*      Set number of bit/pixel */

        switch( data_type ){
        case IMAGE_PLANE:
                return 1;
        case IMAGE_CHAR:
        case IMAGE_8BIT_COL:
                return 1*8;
        case IMAGE_SHORT:
        case IMAGE_USHORT:
                return 2*8;
        case IMAGE_FLOAT:
        case IMAGE_LONG:
        case IMAGE_ULONG:
        case IMAGE_24BIT_COL:
        case IMAGE_32BIT_COL:
                return 4*8;
        case IMAGE_DOUBLE:
        case IMAGE_COMPLEX:
                return 8*8;
        case IMAGE_DCOMPLEX:
                return 16*8;
        default:
                errmes("image_nbit_decide:illegal data type",0);
                return -1;
        }
}


int
image_cpy( IMAGE *dst, IMAGE *src )
{
	int	k,i;

	if(    dst->nchan != src->nchan 
	    || dst->h     != src->h     
	    || dst->md_linebytes != src->md_linebytes 
		) return -1;
	for(k=0;k<src->nchan;k++)
		for(i=0;i<src->h;i++)
			MEMCPY( dst->data[k][i], src->data[k][i], src->md_linebytes);
	return 0;
}


int
image_cut_data( IMAGE *img, int xs, int ys, int w, int h, int chanl, u_char **buf)
{
	int i;
	long long count /*delta */;
/*	u_char *data;	*/

	count = image_size(img->nbit,w,1,1);
	for(i=0;i<h;i++,ys++)
		MEMCPY( buf[i], image_pixel(img,xs,ys,chanl), count);
/*	if( img->buf == NULL ) return -1;	*/
/*
	if( xs == 0 && w == img->w ){
		data = image_pixel(img,xs,ys,chanl);
		count = image_size(img->nbit,w,h,1);
		MEMCPY( buf, data, count );
	} else {
		count = image_size(img->nbit,w,1,1);
		data = img->data[chanl][ys] + (off_T)img->nbit*xs/8;
		delta = img->md_linebytes;
		for(i=0;i<h;i++,buf+=count,data+=delta)
			MEMCPY( buf, data, count );
	}
*/
	return 0;
}


int
image_put_data( IMAGE *img, int xs, int ys, int w, int h, int chanl, u_char **buf)
{
	int i;
	long long count /*delta */;
/*	u_char *data;	*/

	
	count = image_size(img->nbit,w,1,1);
	for(i=0;i<h;i++,ys++)
		MEMCPY( image_pixel(img,xs,ys,chanl), buf[i],count);


/*	if( img->buf == NULL ) return -1;	*/
/*
	if( xs == 0 && w == img->w ){
		data = image_pixel(img,xs,ys,chanl);
		count = image_size(img->nbit,w,h,1);
		MEMCPY( data, buf, count );
	} else {
		count = image_size(img->nbit,w,1,1);
		data = img->data[chanl][ys] + img->nbit*xs/8;
		delta = img->md_linebytes;
		for(i=0;i<h;i++,buf+=count,data+=delta)
			MEMCPY( data, buf, count );
	}
*/
	return 0;
}


u_char *
image_pixel( IMAGE *img, int x, int y, int chanl )
{
	return
	img->data[chanl][y] + img->nbit * x / 8; 
}


int
image_pixel_v(IMAGE *img, int x, int y, int chanl, double *v )
{
	switch( img->data_type ){
	case	IMAGE_CHAR:
		*v = *(u_char*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_SHORT:
		*v = *(short*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_USHORT:
		*v = *(u_short*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_LONG:
		*v = *(long*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_ULONG:
        case	IMAGE_32BIT_COL:
		*v = *(u_long*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_FLOAT:
		*v = *(float*)image_pixel( img, x, y, chanl );
		break;
	case	IMAGE_DOUBLE:
		*v = *(double*)image_pixel( img, x, y, chanl );
		break;
	default:
		fprintf(stderr,"image_pixel_v:data type unknown %d\n",img->data_type);
		return -1;
		/* break; */
	}
	return 0;
}


int
image_pixel_vset( IMAGE *img, int x, int y, int chanl, double v )
{
	u_char	*ucp;
	short	*stp;
	u_short	*usp;
	u_long	*ulp;
	long	*lgp;
	float	*ftp;
	double	*dbp;
	int	iwk;

	switch( img->data_type ){
	case	IMAGE_CHAR:
		ucp = image_pixel( img, x, y, chanl );
		iwk = (int)v;
		if( iwk < 0 )
			iwk = 0;
		else if( iwk > 255 )
			iwk = 255;
		*ucp = (u_char) iwk;
		break;
	case	IMAGE_SHORT:
		stp = (short*)image_pixel( img, x, y, chanl );
		*stp = (short) v;
		break;
	case	IMAGE_USHORT:
		usp = (u_short*)image_pixel( img, x, y, chanl );
		*usp = (u_short) v;
		break;
	case	IMAGE_LONG:
		lgp = (long*)image_pixel( img, x, y, chanl );
		*lgp = (long) v;
		break;
	case	IMAGE_ULONG:
        case	IMAGE_32BIT_COL:
		ulp = (u_long*)image_pixel( img, x, y, chanl );
		*ulp = (u_long) v;
		break;
	case	IMAGE_FLOAT:
		ftp = (float*)image_pixel( img, x, y, chanl );
		*ftp = (float) v;
		break;
	case	IMAGE_DOUBLE:
		dbp = (double*)image_pixel( img, x, y, chanl );
		*dbp = v;
		break;
	default:
		fprintf(stderr,"pixel_set:data type unknown %d\n",img->data_type);
		/* return -1; */
		break;
	}
	return 0;
}


int
image_pixel_v2D( IMAGE *img, int x, int y, int chanl, int nx, int ny, double **v )
{
	u_char	*ucp;
	short	*stp;
	u_short	*usp;
	u_long	*ulp;
	long	*lgp;
	float	*ftp;
	double	*dbp;
	int	i,j;
	switch( img->data_type){
	case	IMAGE_CHAR:
		for(j=0;j<ny;j++,y++){
			ucp = image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,ucp++)
				v[j][i] = (double)*ucp;
		}
		break;
	case	IMAGE_SHORT:
		for(j=0;j<ny;j++,y++){
			stp = (short*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,stp++)
				v[j][i] = (double)*stp;
		}
		break;
	case	IMAGE_USHORT:
		for(j=0;j<ny;j++,y++){
			usp = (u_short*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,usp++)
				v[j][i] = (double)*usp;
		}
		break;
	case	IMAGE_LONG:
		for(j=0;j<ny;j++,y++){
			lgp = (long*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,lgp++)
				v[j][i] = (double)*lgp;
		}
		break;
	case	IMAGE_ULONG:
        case	IMAGE_32BIT_COL:
		for(j=0;j<ny;j++,y++){
			ulp = (u_long*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,ulp++)
				v[j][i] = (double)*ulp;
		}
		break;
	case	IMAGE_FLOAT:
		for(j=0;j<ny;j++,y++){
			ftp = (float*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,ftp++)
				v[j][i] = (double)*ftp;
		}
		break;
	case	IMAGE_DOUBLE:
		for(j=0;j<ny;j++,y++){
			dbp = (double*)image_pixel( img, x, y, chanl );
			for(i=0;i<nx;i++,dbp++)
				v[j][i] = (double)*dbp;
		}
		break;
	default:
		fprintf(stderr,"image_pixel_v2D:data type unknown %d\n",
			img->data_type);
		return -1;
		/* break; */
	}
	return 0;
}


/***** static functions *****/

/* temporarily disabled
static long long 
image_offset( IMAGE *img, int x, int y, int chanl)
{
	return
	image_size(img->nbit,img->w,img->h*chanl+y,1 ) + img->nbit*x/8;
}
*/


/************************************************
	Print out error message to stderr
 ************************************************/
static int
errmes( char *message, int erno )
{
	fprintf(stderr,"%s", message);
	fprintf(stderr,"%d\n", erno);
	return 0;
}
/*
static int
errmesSys( char *message, int erno )
{
		errmes( message, erno );
        if( erno != 0 ){
				perror(message);
		}
	return 0;
}
*/
