all:	libimage.a ex7_1.a imgDup.a bin2tif JDEMconv hsiLegend.a jpgcnv

install:
	cp -p libimage.a /usr/local/lib
	cp -p image.h imgio.h bmp.h /usr/local/include
	cp -p bin2tif jpgcnv /usr/local/bin
#	cp -p image.c imgio.c bmp.c image.h imgio.h bmp.h ~/win/image/fromLinux/

archive:
	tar cfv image.tar image.c image.h imgio.c imgio.h hsishd.c bmp.c bmp.h image.txt imgio.txt makefile ex7_1.c hsiLegend.c imgDup.c bin2tif.c JDEMconv.c jpgcnv.c
	gzip image.tar

clean:
	rm *.o *.a

libimage.a: image.o imgio.o bmp.o hsishd.o
	ar rc libimage.a image.o imgio.o bmp.o hsishd.o
	ranlib libimage.a
image.o: image.c image.h
	gcc -c -DLINUX -I /usr/local/include image.c
imgio.o: imgio.c imgio.h image.h
	gcc -c -DLINUX -I /usr/local/include imgio.c
hsishd.o: hsishd.c image.h
	gcc -c -DLINUX hsishd.c
bmp.o: bmp.c bmp.h
	gcc -c -DLINUX bmp.c
ex7_1.a: ex7_1.c libimage.a
	gcc -o ex7_1.a ex7_1.c -L. -L/usr/local/lib -limage -ltiff -ljpeg
hsiLegend.a: hsiLegend.c libimage.a
	gcc -o hsiLegend.a hsiLegend.c -L. -L/usr/local/lib -limage -ltiff -ljpeg
imgDup.a: imgDup.c libimage.a
	gcc -o imgDup.a imgDup.c -L. -L/usr/local/lib -limage -ltiff -ljpeg
bin2tif: bin2tif.c libimage.a
	gcc -o bin2tif bin2tif.c -L. -L/usr/local/lib -limage -ltiff -ljpeg
JDEMconv: JDEMconv.c libimage.a
	gcc -o JDEMconv -I /usr/local/include JDEMconv.c -L. -L/usr/local/lib -limage -ltiff -ljpeg -lutilHonda
jpgcnv: jpgcnv.c libimage.a
	gcc -o jpgcnv -I /usr/local/include jpgcnv.c -L. -L/usr/local/lib -limage -ltiff -ljpeg -lutilHonda
