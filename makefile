INC=-I. -I/opt/homebrew/opt/jpeg/include -I/opt/homebrew/opt/libtiff/include -I/usr/local/include
LIBS=-L/opt/homebrew/opt/jpeg/lib  -L/opt/homebrew/lib -limage -ltiff -ljpeg
LIBS_GTF=-L/opt/homebrew/opt/jpeg/lib  -L/opt/homebrew/lib -limage_gtf -lgeotiff -ltiff -ljpeg

all:	libimage.a libimage_gtf.a ex7_1.a ex7_1_gtf.a imgDup.a bin2tif JDEMconv hsiLegend.a jpgcnv rm8con.a

install:
	cp -p libimage.a /usr/local/lib
	cp -p libimage_gtf.a /usr/local/lib
	cp -p image.h imgio.h bmp.h /usr/local/include
	cp -p bin2tif jpgcnv /usr/local/bin
#	cp -p image.c imgio.c bmp.c image.h imgio.h bmp.h ~/win/image/fromLinux/

archive:
	tar cfv image.tar image.c image.h imgio.c imgio.h hsishd.c bmp.c bmp.h image.txt imgio.txt makefile ex7_1.c hsiLegend.c imgDup.c bin2tif.c JDEMconv.c jpgcnv.c
	gzip image.tar

clean:
	rm *.o *.a

libimage.a: image.o imgio.o bmp.o hsishd.o
	ar rc libimage.a image.o imgio.o bmp.o hsishd.o
	ranlib libimage.a
libimage_gtf.a: image.o imgio_gtf.o bmp.o hsishd.o
	ar rc libimage_gtf.a image.o imgio_gtf.o bmp.o hsishd.o
	ranlib libimage_gtf.a
image.o: image.c image.h
	gcc -c -DLINUX $(INC) image.c
imgio.o: imgio.c imgio.h image.h
	gcc -c -DLINUX $(INC) imgio.c
imgio_gtf.o: imgio_gtf.c imgio_gtf.h image.h
	gcc -c -DLINUX $(INC) imgio_gtf.c
#	gcc -c -DLINUX -I/opt/homebrew/opt/jpeg/include -I/opt/homebrew/opt/libtiff/include -I/usr/local/include imgio.c
hsishd.o: hsishd.c image.h
	gcc -c -DLINUX hsishd.c
bmp.o: bmp.c bmp.h
	gcc -c -DLINUX bmp.c
rm8con.a: rm8con.c libimage.a
	gcc -o rm8con.a rm8con.c $(LIBS) -lm
ex7_1.a: ex7_1.c libimage.a
	gcc -o ex7_1.a ex7_1.c $(INC) -L. $(LIBS) -lm
#	gcc -o ex7_1.a ex7_1.c -L. -L/opt/homebrew/opt/jpeg/lib  -L/opt/homebrew/lib -limage -ltiff -ljpeg -lm
ex7_1_gtf.a: ex7_1.c libimage.a
	gcc -o ex7_1_gtf.a ex7_1.c $(INC) -L. $(LIBS_GTF) -lm
hsiLegend.a: hsiLegend.c libimage.a
	gcc -o hsiLegend.a hsiLegend.c $(INC) -L. $(LIBS) -lm
imgDup.a: imgDup.c libimage.a
	gcc -o imgDup.a imgDup.c $(INC) -L. $(LIBS) -lm
bin2tif: bin2tif.c libimage.a
	gcc -o bin2tif bin2tif.c $(INC) -L. $(LIBS) -lm
JDEMconv: JDEMconv.c libimage.a
	gcc -o JDEMconv $(INC) JDEMconv.c $(INC) -L. $(LIBS) -lutilHonda -lm
jpgcnv: jpgcnv.c libimage.a
	gcc -o jpgcnv jpgcnv.c $(INC) -L. $(LIBS) -lutilHonda -lm
