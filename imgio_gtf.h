/*	
	imgio.h : image I/O handling routines header file

	Copyright(C)2011 HONDA Kiyoshi

  	Contact: Dr. HONDA Kiyoshi
			P.O.Box 4, Khlong Luang, Pathumthani, 12120, Thailand
			Asian Institute of Technology

			e-mail: honda@ait.ac.th
			http://www.rsgis.ait.ac.th/~honda

If you wish to use or distribute this software in a way
which does not follow the license stated below, you must obtain
written agreement from the copyright holder(s) above.

-------------------------------------------------------------------------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Program you will develop using this software must use the same
license with this software which is GNU General Public License
for any distribution.

The latest version of GNU General Public License may be obtained
from http://www.gnu.org/licenses

*/

/*
 * 									*
 *	Modification History						*
 *									*
 *	12 Dec 2003		map info I/O for ENVI ( for Lat/Lon and UTM )
 *	29 June 2003	pixel spacing I/O for ENVI: dx, dy
 *	Jun 1996	Extension for external header files		*
 *				(EXHDR, ERM, ENVI) by H.Ishibashi	*
 *	Jun 1996	Extension for byte order definition by H.Ishibashi  *
 *	May 1996	Extension for Win32 by H.Ishibashi		*
 *	May 1996	Remove old function definition by H.Ishibashi	*
 *	Mar 1995	Extension for compress files by H.Ishibashi	*
 *	Oct 1994	Extension for Windows by H.Ishibashi		*
 *	Jan 1994	Extension for MS-DOS by H.Ishibashi		*

*/


#include"image.h"
#if ! defined (IMGIO_H)
#define	IMGIO_H

#define IMAGE_INCLD_TIFF
#define IMAGE_INCLD_JPEG
#define IMAGE_INCLD_BMP
/*
*/

/*
#if defined ( IMAGE_INCLD_TIFF )
//#include"tiffio.h"
#include"xtiffio.h"
#include"geotiffio.h"
#endif
*/
#if defined ( IMAGE_INCLD_JPEG )
//#include"jpeglib.h"
#endif

#if defined ( IMAGE_INCLD_BMP )
//#include"bmp.h"
#endif

#define IMAGE_RDONLY 0
#define IMAGE_RDWR   1
#define IMAGE_EXCL   2
#define IMAGE_TRUNC  3

#define IMAGE_BUFFERED		1
#define IMAGE_NO_BUF		2

#define	IMAGE_BSQ	1
#define IMAGE_BIL	2
#define IMAGE_PSQ	3
#define IMAGE_BIP	3
#define	IMAGE_BSQ_MULT	4

/* byte order */
#define IMAGE_SUN	1
#define IMAGE_MX	2
#define	IMAGE_DOS	3
#define	IMAGE_MAC	4
#define	IMAGE_MSB_1ST	1
#define	IMAGE_LSB_1ST	2

/* header format */
#define	IMAGE_UNKNOWN_FORMAT	-1
#define MSIF			0
#define IMAGE_MSIF		0
#define EXHDR			1
#define IMAGE_EXHDR		1
#define ERM				2
#define IMAGE_ERM		2
#define ENVI			3
#define IMAGE_ENVI		3
#define	ALLEXHDR		4
#define IMAGE_TIFF		5
#define IMAGE_JPEG		6
#define IMAGE_ARCINFO	7
#define IMAGE_ARC_INFO	7
#define IMAGE_BMP		8


/* file extension */
#define EXHDR_EXTENTION			".mif"
#define ERM_EXTENTION			".ers"
#define ENVI_EXTENTION			".hdr"
#define TIFF_EXTENTION			".tif"
#define JPEG_EXTENTION			".jpg"
#define BMP_EXTENTION			".bmp"
#define ARCINFO_EXTENTION		".bil"
#define ARCINFO_HDR_EXTENTION	".hdr"

/* compression format */
#define COMPRESS	100
#define GZIP		101

#define IMAGE_FNAME_MAX		256
#define	IMAGE_HEADER_LENGTH	512
#define	IMAGE_FULLPATH_LENGTH	256

#if defined(WINDOWS) || defined(LINUX)
# define LSB_1ST
#else
# define MSB_1ST
#endif

typedef struct header_tag{
	char	dname[13];
	unsigned short	fx,fy;
	unsigned short	sx,sy;
	unsigned short	nsx,nsy;
	unsigned char	nbit,ebit;
	unsigned char	lr,scale;
	long		lleng;
	unsigned short	pleng;
	unsigned char	podr,sodr;
	unsigned char	ml,mr,mu,md;
	char	device[7];
	char	method[7];
	unsigned short	vx,vy;
	unsigned short	rxs,rys,rxe,rye;
	unsigned short	fxs,fys;
	char		mmddyy[7];
	char		place[7];
	unsigned short	sampldx,sampldy;
	unsigned short	mask_level;
	unsigned short	nchan;
	char		channel_id[8][5];
	unsigned char	dt1,dt2;
	unsigned short	ndesc;
	unsigned short	mode;
	unsigned short	nsamppx,nsamppy;
	unsigned short	nsamplx,nsamply;
	char		comment[61];
	unsigned short	version;
	struct {
		char	comment[17];
		unsigned short	maxv,minv; } bit_plane[15];
	unsigned short	data_type;
	unsigned short	file_spec;
	unsigned short	byte_order;
} IMAGE_HEADER;

union PHYSICAL_HEADER{
	unsigned short	us[IMAGE_HEADER_LENGTH/2];
	unsigned char	ch[IMAGE_HEADER_LENGTH  ];
	long		lg[IMAGE_HEADER_LENGTH/4];
};
	
typedef struct IMAGE_FILE_TAG{
	int	new_case;
	int	mode;		/*IMAGE_RDONLY,RDWR	*/
	int	data_type;	/*PLANE,CHAR,SHORT,..	*/
	int	interleave;	/*IMAGE_BSQ,BIL,BIP	*/
	int	w;
	int	h;
	int	nchan;
	int	nbit;
	int	ndesc;
	IMAGE_GEOREGISTRATION_INFO	geoRegistration;
	IMAGE_ATTR	attr;
	int	buffering;	/*IMAGE_NO_BUF,EXT,INT	*/
	long long	size;
	int	byte_order;	/*IMAGE_LSB_1ST, IMAGE_MSB_1ST	*/
	int	fhandl;
	char    fname[IMAGE_FNAME_MAX+1];
	char	fullPath[IMAGE_FULLPATH_LENGTH];
	char	hdrFullPath[IMAGE_FULLPATH_LENGTH+4];
	IMAGE_HEADER header;
	union PHYSICAL_HEADER physical_header;
	IMAGE	*image;
	int	md_primary;
	int	compressed;
	int	file_header;
	int	need_reverse;
	int	header_length;
#if defined ( IMAGE_INCLD_TIFF )
/*	TIFF	*tiff;	*/
	void	*tiff;
	u_char	*tiffLineBuf;
	long	tiffBufRow;
	u_char	*written;
	long	tiffWriteLineY;
	int		*tiffWriteLineInbuf;
	int		tiffRowsperstrip;
	int		tiffPlanarconfig;
#endif
#if defined ( IMAGE_INCLD_JPEG )
	void	*jpg;
/*
	FILE	*jpegFp;
	int	jpegQuality;
	int	jpegLineBufNo;
	int	*jpegWriteLineInbuf;
	u_char	*jpegLineBuf;
	int	jpegCompressDir;

	struct	jpeg_compress_struct cinfo;
	struct	jpeg_decompress_struct dcinfo;
	JSAMPROW jpeg_row_pointer[1];
	struct	jpeg_error_mgr jerr;
	struct jpegst	*jpeg;

*/
#endif

#if defined ( IMAGE_INCLD_BMP )
	void	*bmp;
#endif
} IMAGE_FILE;

IMAGE_FILE * image_file_create( char *fname, int mode, int ndesc, IMAGE *img );
IMAGE_FILE * image_file_open( const char *fname, int mode, int buffering );
IMAGE_FILE * image_file_open_bin( char *fname, int mode, int buffering, int w, int h, int data_type, int nchan, int interleave );
int image_file_close( IMAGE_FILE *imgf );
void image_file_quit( IMAGE_FILE *imgf );
int image_file_save( IMAGE_FILE *imgf );
int image_file_read( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf );
int image_file_write( IMAGE_FILE *imgf, int xs, int ys, int w, int h, int chanl, u_char **buf);
int image_file_image_set( IMAGE_FILE *imgf, IMAGE *img );
u_char * image_file_get_pixel( IMAGE_FILE *imgf, int x, int y, int chanl );
int image_file_put_pixel( IMAGE_FILE *imgf, int x, int y, int chanl, u_char *buf );
int image_file_set_fname( IMAGE_FILE *imgf, char *fname );
int image_file_get_comment( IMAGE_FILE *imgf, char *comment );
int image_file_set_comment( IMAGE_FILE *imgf, char *comment );
void CompressSwitch( int mode );
void image_file_default_header_set( int mode );
void image_file_default_interleave_set( int mode );
void image_file_header_set( IMAGE_FILE *imgf, int mode );
void image_file_interleave_set( IMAGE_FILE *imgf, int mode );
int haveERMExtention ( char *fname );
int haveENVIExtention ( char *fname );
int haveARCINFOExtention ( char *fname );
int haveTiffExtention ( char *fname );
int haveJPEGExtention ( char *fname );
int haveBMPExtention ( char *fname );
#if defined ( IMAGE_INCLD_TIFF )
void image_tif_compress_lzw( IMAGE_FILE	*imgf, int lzwPredictor );
#endif
#if defined ( IMAGE_INCLD_JPEG )
void image_jpeg_quality( IMAGE_FILE *imgf, int quality );
void image_jpeg_default_quality_set( int quality );
#endif

#if defined( MAC )
	int ImageIOeventCheckSet( int (*aEventCheck)() );
#endif

extern	char *enviRegistrationString[];
extern	char *enviUTMNorthSouthString[];
extern	char *enviDatumString[]; 
extern	char *enviUTMUnitString[];
/*
char	*enviRegistrationString[] = 
{
	"Geographic Lat/Lon",
	"UTM",
	"Arbitrary",
	NULL
};

char	*enviUTMNorthSouthString[] = 
{
	"North",
	"South",
	NULL
};

char	*enviDatumString[] = 
{
	"WGS-84",
	"None",
	"Tokyo mean",
	NULL
};

char	*enviUTMUnitString[] = 
{
	"units=Degree",
	"units=Meters",
	NULL
};
*/
#endif /* !defined IMGIO_H */
