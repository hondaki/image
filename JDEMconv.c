#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"image.h"
#include"imgio.h"

#include"utilHonda.h"

#define	PRGNAME	"JDEMConv"

/*
	This program convert Japan National DEM 250m, and 50m to ENVI and Tiff image file
*/

#define	OCEANVALUE	-9999

void usage(void)
{
	printf( "usage: " PRGNAME " [-help] [-envi|-tiff] [-ocean elevationOcean] fileName [[filename]...]\n");
	printf( "\tdefault: file format = tiff, oceanelevation = %.1f\n", OCEANVALUE/10.);
	printf(	"\n\tNote: Datum is TOKYO ( not GRS80 ), Data source is aerialphotograph\n" );
	printf( "\t\tFile Format Reference:\n"
		"\t\t250m dem: http://www.jmc.or.jp/data/sem/doc/index250.htm\n"
		"\t\t 50m dem: http://www.jmc.or.jp/data/mem/doc/index50m.htm\n"
		);
//	printf( "usage: " PRGNAME " [-help] fileName -h height -w width -nch nchan -dt [BSQ|BIL]\n");
}

int conv( char *fNameI, char *extension, int data_type, double oceanValue );

int main( int argc, char **argv )
{
	
	int	data_type = IMAGE_FLOAT;
	char	*fNameI;
	char	*extension = ".tif";
	int	fileType = IMAGE_TIFF;
	int	nConv = 0;
	double	oceanValue = OCEANVALUE/10.0;

	while( --argc ){
		++argv;
		if( strcmp(argv[0], "-help") == 0 ){
			usage();
			exit(0);
		} else if( strcmp( argv[0], "-envi") == 0 ){
			extension = ENVI_EXTENTION;
			fileType = IMAGE_ENVI;
		} else if( strcmp( argv[0], "-tiff") == 0 ){
			extension = TIFF_EXTENTION;
			fileType = IMAGE_TIFF;
		} else if( strcmp( argv[0], "-ocean") == 0 ){
			++argv; --argc;
			if( !argc ){
				usage();
				exit(0);
			}
			oceanValue = atof( argv[0] );
		} else {
			if( argv[0][0] == '-' ){
				usage();
				break;
			}
			fNameI = argv[0];
			conv( fNameI, extension, data_type, oceanValue );
			nConv++;
		}
	}
	if( !nConv )
		usage();
	return nConv;
}

#define	BUFLEN	4096

int conv( char *fNameI, char *extension, int data_type, double oceanValue )
{
	FILE	*fp, *fpNt;
	int	w, h, nchan = 1;
	int	meshcode;
	int	deg, minu, sec;
	double	leftLon, rightLon;
	double	upperLat, lowerLat;
	int	srcScale=0, srcYear=0, srcModYear=0, digiYear=0;
	double	dx, dy;

	IMAGE	*demImg;
	IMAGE_FILE	*demImgF;
	float	**dem;

	u_char	buf1[BUFLEN], buf2[BUFLEN];
	int	ieleve;
	char	fNameO[BUFLEN];
	char	fNameNote[BUFLEN];

	int	i, j;

	fp = fopen( fNameI, "r");
	if( !fp ){
		fprintf( stderr, PRGNAME ": DEM file %s open error\n", fNameI );
		return 1;
	}

	fgets( buf1, BUFLEN-1, fp );
	strncpy( buf2, buf1+ 0, 6); buf2[6]=0; sscanf( buf2, "%6d", &meshcode );	printf( PRGNAME ": meshcode = %d\n", meshcode );

	strncpy( buf2, buf1+ 6, 5); buf2[5]=0; sscanf( buf2, "%5d", &srcScale );	printf( PRGNAME ": srcScale = %d\n", srcScale );
	strncpy( buf2, buf1+11, 4); buf2[4]=0; sscanf( buf2, "%4d", &srcYear );	printf( PRGNAME ": srcYear = %d\n", srcYear );
	strncpy( buf2, buf1+15, 4); buf2[4]=0; sscanf( buf2, "%4d", &srcModYear );	printf( PRGNAME ": srcModYear = %d\n", srcModYear );
	strncpy( buf2, buf1+19, 4); buf2[4]=0; sscanf( buf2, "%4d", &digiYear );	printf( PRGNAME ": digiYear = %d\n", digiYear );

	strncpy( buf2, buf1+23, 3); buf2[3]=0; sscanf( buf2, "%3d", &w );				//printf( PRGNAME ": w = %d\n", w );
	strncpy( buf2, buf1+26, 3); buf2[3]=0; sscanf( buf2, "%3d", &h );				//printf( PRGNAME ": h = %d\n", h );

	strncpy( buf2, buf1+29, 3); buf2[3]=0; sscanf( buf2, "%3d", &deg );
	strncpy( buf2, buf1+32, 2); buf2[2]=0; sscanf( buf2, "%3d", &minu );
	strncpy( buf2, buf1+34, 2); buf2[2]=0; sscanf( buf2, "%3d", &sec );
	lowerLat = deg + (minu + sec/60. ) /60.;		printf( PRGNAME ": lower lat: %s\n", dmsStr( lowerLat, 2 ));

	strncpy( buf2, buf1+36, 3); buf2[3]=0; sscanf( buf2, "%3d", &deg );
	strncpy( buf2, buf1+39, 2); buf2[2]=0; sscanf( buf2, "%3d", &minu );
	strncpy( buf2, buf1+41, 2); buf2[2]=0; sscanf( buf2, "%3d", &sec );
	leftLon = deg + (minu + sec/60. ) /60.;			printf( PRGNAME ": left  lon: %s\n", dmsStr( leftLon, 2 ));

	strncpy( buf2, buf1+43, 3); buf2[3]=0; sscanf( buf2, "%3d", &deg );
	strncpy( buf2, buf1+46, 2); buf2[2]=0; sscanf( buf2, "%3d", &minu );
	strncpy( buf2, buf1+48, 2); buf2[2]=0; sscanf( buf2, "%3d", &sec );
	upperLat = deg + (minu + sec/60. ) /60.;		printf( PRGNAME ": upper lat: %s\n", dmsStr( upperLat, 2 ));

	strncpy( buf2, buf1+50, 3); buf2[3]=0; sscanf( buf2, "%3d", &deg );
	strncpy( buf2, buf1+53, 2); buf2[2]=0; sscanf( buf2, "%3d", &minu );
	strncpy( buf2, buf1+55, 2); buf2[2]=0; sscanf( buf2, "%3d", &sec );
	rightLon = deg + (minu + sec/60. ) /60.;		printf( PRGNAME ": right lon: %s\n", dmsStr( rightLon, 2 ));

	demImg =  image_alloc( w, h, data_type, nchan );
	dem = (float** ) demImg->data[0];

	for(i=0;i<h;i++){
		for(j=0;j<w;j++){
			dem[i][j] = oceanValue;
		}
	}
	while( fgets( buf1, BUFLEN-1, fp ) ){
		sscanf( buf1 + 6, "%3d", &i );
//		printf( PRGNAME ": reading line %d\n", i );
		i--;
		for(j=0; j<w; j++ ){
			sscanf( buf1 + 9 + j * 5 , "%5d", &ieleve );
			if( ieleve == OCEANVALUE )
				dem[i][j] = oceanValue;		
			else
				dem[i][j] = (float) ieleve / 10.0;
		}
	}

	strcpy( fNameO, fNameI );
	removeExtension2( fNameO );
	strcat( fNameO, extension );

//	printf( "fNameI = %s\n", fNameI );
//	printf( "fNameO = %s\n", fNameO);
//	printf( "fNameO = %s\n", fNameO);
//	printf( "fNameO = %s\n", fNameO);

	dx = (rightLon - leftLon )/w;
	dy = (upperLat - lowerLat )/h;

	demImg->geoRegistration.projection = IMAGE_PROJECTION_GEOGRAPHIC;
	demImg->geoRegistration.geoRegistrationX = 0;
	demImg->geoRegistration.geoRegistrationY = 0;
	demImg->geoRegistration.geoRegistrationMapX = leftLon;
	demImg->geoRegistration.geoRegistrationMapY = upperLat;
	demImg->geoRegistration.dx = dx;
	demImg->geoRegistration.dy = dy;
	demImg->geoRegistration.datum = IMAGE_DATUM_TOKYO;
	demImg->geoRegistration.Unit = IMAGE_UNITDEGREE;

	demImgF = image_file_create( fNameO, IMAGE_TRUNC, 0, demImg );

	image_file_close( demImgF );
	image_destroy( demImg );
	fclose( fp );

	strcpy( fNameNote, fNameI );
	removeExtension2( fNameNote );
	strcat( fNameNote, ".txt" );
	fpNt = fopen( fNameNote, "wt");

	fprintf( fpNt, "mesh code\t%d\n", meshcode );
	fprintf( fpNt, "colum\t%d\n", w );
	fprintf( fpNt, "line\t%d\n", h );
	fprintf( fpNt, "left  longitude\t%.8lf\t%s\n", leftLon, dmsStr(leftLon, 2) );
	fprintf( fpNt, "right longitude\t%.8lf\t%s\n", rightLon, dmsStr(rightLon, 2) );
	fprintf( fpNt, "upper latitude\t%.8lf\t%s\n", upperLat, dmsStr(upperLat, 2) );
	fprintf( fpNt, "lower latitude\t%.8lf\t%s\n", lowerLat, dmsStr(lowerLat, 2) );
	fprintf( fpNt, "dx\t%.8lf\t%s\n", dx, dmsStr(dx, 2) );
	fprintf( fpNt, "dy\t%.8lf\t%s\n", dy, dmsStr(dy, 2) );
	fprintf( fpNt, "source map scale\t1/%d\n", srcScale );
	fprintf( fpNt, "source map year\t%d\n", srcYear );
	fprintf( fpNt, "source map modified in year\t%d\n", srcModYear );
	fprintf( fpNt, "digitized in year\t%d\n", digiYear );

	fclose( fpNt );

	return 0;
}
