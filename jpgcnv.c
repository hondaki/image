#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<errno.h>

#define	LINUX

#if defined ( WINDOWS )
#include<io.h>
#include<sys/utime.h>
#define	STAT	_stat
#define	UTIMBUF	_utimbuf
#define	UTIME	_utime
#else
#include<utime.h>
#define	STAT	stat
#define	UTIMBUF	utimbuf
#define	UTIME	utime
#endif

#include"image.h"
#include"imgio.h"
#include"utilHonda.h"

int jcnv( char *fname1, int ratio, int jpegQuality  );

void main( int argc, char *argv[] )
{
	int			ratio = 1;
	int	jpegQuality = 80;

	char	fname1[256];
	char	*v;


	while ( --argc ){
		v = *++argv;
		if( strncmp( v, "-r", 2 ) == 0 ){
			ratio = atoi( v+2 );
			if( ratio < 1 ){
				fprintf( stderr, "illegal reduction ratio\n");
				return;
			} else {
				fprintf( stderr, "reduction ration: %d\n", ratio );
			}
		} else if( strncmp( v, "-q", 2 ) == 0 ){
			jpegQuality = atoi( v+2 );
			if( jpegQuality < 10 ){
				fprintf( stderr, "illegal jpegQuality\n");
				return;
			} else {
				fprintf( stderr, "jpegQuality: %d\n", jpegQuality );
			}
		} else {
			fprintf(stderr, "%s\n", v);
			jcnv( v, ratio, jpegQuality );
		}
	}
}

int jcnv( char *fname1, int ratio, int jpegQuality )
{
	IMAGE_FILE	*imgf1, *imgf2;
	IMAGE		*img1,  *img2;
	int			i,j,ii,jj,k,m,ichan, total, w1, h1, w2, h2, nchan;
	char	fname2[256];


	struct STAT buffer;
	int		result;
	struct UTIMBUF times;

	result = STAT( fname1, &buffer );

	if( result != 0 ){
		fprintf( stderr, "image information cannot be obtained %s\n", fname1 );
		return 1;
	}

	imgf1 = image_file_open( fname1, IMAGE_RDONLY, IMAGE_BUFFERED );
	if( imgf1 == NULL ){
		fprintf( stderr, "image file open error %s\n", fname1 );
		return 2;
	}
	img1 = imgf1->image;
//		strcpy( fname2, fname1 );
	strcpy( fname2, "imgcnvtmp.jpg" );
//		_mktemp( 

	if( ratio == 1 ){
		img2 = img1;
	} else {
		h1 = imgf1->h;
		w1 = imgf1->w;
		nchan = imgf1->nchan;

		h2 = h1 / ratio;
		w2 = w1 / ratio;

		img2 = image_alloc( w2, h2, IMAGE_CHAR, nchan );
		if( img2 == NULL ){
			fprintf(stderr, "image allocate error %d %d %d\n",w2, h2, nchan );
			image_file_close( imgf1 );
			return 3;
		}
		for( ichan=0; ichan<nchan; ichan++ ){
			for( i=0, ii=0; i<h2; i++, ii += ratio ){
				for( j=0, jj=0; j<w2; j++, jj += ratio){
					total = 0;
					for( k=0; k<ratio; k++ ){
						for(m=0;m<ratio;m++){
							total += img1->data[ichan][ii+k][jj+m];
						}
					}
					img2->data[ichan][i][j] = total / ( ratio * ratio );
				}
			}
		}
	}
	imgf2 = image_file_create( fname2, IMAGE_TRUNC, 0, img2 );
	if( imgf2 == NULL ){
		fprintf(stderr, "image file create error %s\n", fname2 );
		image_file_close( imgf1 );
		return 4;
	}

	image_jpeg_quality( imgf2, jpegQuality );

	if( image_file_close( imgf2 ) != 0 ){
		fprintf(stderr, "image file close error %s\n", fname2 );
		image_file_close( imgf1 );
		unlink( fname2 );
		return 5;
	}
	if( img2 != img1 ){
		image_destroy( img2 );
	}

	image_file_close( imgf1 );

	if(  haveJPEGExtention ( fname1 ) )	{
		unlink( fname1 );
	} else {
		if( haveENVIExtention(fname1) || haveERMExtention(fname1) ||
			haveARCINFOExtention(fname1) || haveTiffExtention(fname1)
		){
			removeExtension2(fname1);
		}
		strcat( fname1, ".jpg" );
	}
	switch( rename( fname2, fname1 ) ){
	case 0:
		break;
	case EACCES:
		fprintf(stderr, "jpgcnv: output file already exists %s\n", fname1);
		return 1;
		break;
	default:
		fprintf(stderr, "jpgcnv: file rename error %s\n", fname1);
		return 1;
		break;
	}
//	printf("jpgcnv: fname2, fname1 = %s %s\n", fname2, fname1 );

	times.actime = buffer.st_atime;
	times.modtime = buffer.st_mtime;
	UTIME( fname1, &times );

	return 0;

}
