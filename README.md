image
=====

Light-weight image routines for tiff, jpeg, and especially ENVI with georegistration information.  

See image.txt for on memory image manipulation.  
See imgio.txt for image file manipulation.  

Required jpeg, and tiff to handle them.
