#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "image.h"
#include "imgio.h"

#define W		256
#define H		256
#define	NCHAN	3

void	img_setup(u_char***pix, int w, int h, int nchan);

int main(void)
{
	IMAGE		*img;
	IMAGE_FILE	*imgf;

	image_file_default_interleave_set( IMAGE_BIL);
	
	printf("calling: image_alloc\n");	
	if((
	img = image_alloc( W, H, IMAGE_CHAR, NCHAN )
	) == NULL ){
		fprintf(stderr, "image allocation error\n");
		exit(1);
	}

	printf("calling: img_setup\n");	
	img_setup( img->data, img->w, img->h, img->nchan );

	printf("calling: image_file_create\n");	
	if(( imgf = image_file_create("colorbil.jpg", IMAGE_TRUNC, 0, img )
		) == NULL ){
		printf("image_file_create error\n");
		exit(1);
	}
	printf("calling: image_file_close\n");	
	image_file_close( imgf );
		
	printf("calling: image_destroy\n");	
	image_destroy( img );

}

#define	PI 3.14159265
void img_setup( u_char ***pix, int w, int h, int nchan )
{
	int	i, j, ichan;

	for(ichan=0;ichan<nchan;ichan++){
		printf("img_setup: channel %d\n", ichan );
		for(i=0;i<h;i++)
			for(j=0;j<w;j++)
				pix[ichan][i][j] =
//					-i-1;
//							(sin((double)j/(w*(1+ichan*0.5))*2.*PI*(10.0*i/h)/0.5) + cos((double)i/(w*(1+ichan*0.5))*2.*PI*(10.0*j/h)))*64+128;
//				((( sin( (double)i/w * 2.*3.14 *(ichan+1) )  ) +1 ) * 128 + (( sin( (double)j/w * 2.*3.14 *(ichan+1) )  ) +1 ) * 128)/2. ;
				0;
	}

	for(ichan=0;ichan<nchan;ichan++){
		for(i=0;i<360;i++){
			int	x, y;
			int	xc = 128;
			int	yc = 128;
	
			x = 100.*cos((double)i/360.*2.*3.1459)+xc;
			y = 100.*-1.*sin((double)i/360.*2.*3.1459)+yc;

			pix[ichan][y][x] = 255;
		}
	}
}
