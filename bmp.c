#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"bmp.h"

//#if defined(WIN32) || defined(WIN64)
//#define _CRT_SECURE_NO_WARNINGS
//#endif

#if defined(MAIN)
#define	PRG	"bmpinfo"
usage()
{
	fprintf(stderr, "usage: " PRG " bmpFileName\n");
}
int main( int argc, char **argv )
{

	BMP	*bmp;
	char	*fname = "0-0.bmp";
	int	w, h, bitCount, lineByte;

	int	i, j, line, row;

	if( argc != 2 ) {
		usage();
		return 0;
	}
	fname = argv[1];

	if( (bmp = bmpOpen( fname )) == NULL ){
		fprintf(stderr, "bmpMain: bmp file open error %s\n", fname );
		return 1;
	}

	printf("BMP File:\t%s\n", fname );
	printf("width:\t%d\n", bmp->w );
	printf("height:\t%d\n", bmp->h );
	printf("Offset:\t%d\n", bmp->bf.bfOffBits );
	printf("linebytes:\t%d\n", bmp->linebytes );
	printf("bitCount:\t%d\n", bmp->bitCount );

	bmpClose( bmp );
}
#endif

BMP *bmpOpen( char *fname ){

	BMP	*bmp;

	bmp = malloc( sizeof( BMP ) );
	bmp->fname = malloc( strlen(fname)+1 );
	strcpy( bmp->fname, fname );
	bmp->fp = NULL;
	bmp->lineBuf = NULL;

	if ((bmp->fp = fopen(fname, "rb")) == NULL) {
		fprintf(stderr, "bmpOpen: can't open %s\n", fname);
		goto err_ret;
	}

/*
	if( sizeof(BITMAPFILEHEADER2) != 14 ){
		fprintf(stderr,"bmpOpen: size of BMP header is %d != 14\n", sizeof(BITMAPFILEHEADER2) );
		goto err_ret;
	}

	if( fread( &bmp->bf, 14, 1, bmp->fp ) != 1 ) {
		fprintf(stderr,"bmpOpen: 14 bytes header read error\n" );
		goto err_ret;
	}
*/
	if( fread( &bmp->bf.bfType, 2, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.bfType read error\n" );
		goto err_ret;
	}

/*
	printf("bmp->bf.bfType = %x\n", bmp->bf.bfType);
	printf("bmp->bf.bfType = %x\n", (((unsigned short) 'B') << 8) | (unsigned short)'M' );
	if( bmp->bf.bfType != ((unsigned short) 'B') | ((unsigned short) 'M'<<8) ){
*/
	if( bmp->bf.bfType[0] != 'B' || bmp->bf.bfType[1] != 'M' ){
		fprintf(stderr,"bmpOpen: 1st 2types is not 'BM': not bmp file\n" );
		goto err_ret;
	} 
	if( fread( &bmp->bf.bfSize, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.bfSize read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bf.bfReserved1, 2, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.bfReserved1 read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bf.bfReserved2, 2, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.bfReserved2 read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bf.bfOffBits, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.bfOffbits read error\n" );
		goto err_ret;
	}


	if( fread( &bmp->bi.biSize, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biSize read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bi.biWidth, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biWidth read error\n" ); 
		goto err_ret;
	}
	if( fread( &bmp->bi.biHeight, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biHeight read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bi.biPlanes, 2, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biPlanes read error\n" );
		goto err_ret;
	}
	if( fread( &bmp->bi.biBitCount, 2, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biBitCount read error\n" );
		goto err_ret;
	}
	if( bmp->bi.biBitCount != 24 ){
		fprintf(stderr,"bmpOpen: This program is only for 24bits/pixel\n" );
		goto err_ret;
	}
	if( fread( &bmp->bi.biCompression, 4, 1, bmp->fp ) != 1 ){
		fprintf(stderr,"bmpOpen: InfoHeader.biCompression read error\n" );
		goto err_ret;
	}
	if( bmp->bi.biCompression != 0 ){
		fprintf(stderr,"bmpOpen: This program can handle only uncompressed BMP file\n" );
		goto err_ret;
	}

	bmp->w = bmp->bi.biWidth;
	bmp->h = bmp->bi.biHeight;
	bmp->bitCount = bmp->bi.biBitCount;

	bmp->linebytes = bmp->w * bmp->bi.biBitCount/8;
	if( bmp->linebytes % 4 != 0 ){
		bmp->linebytes = (bmp->linebytes/4+1)+1;
	}

  	bmp->lineBuf = malloc( bmp->linebytes );
	if( bmp->lineBuf == NULL ){
		fprintf( stderr,"bmpOpen: Line Buffer Allocation Error\n");
		goto err_ret;
	}

	printf("bmpOpen: %s: w:%d h:%d linebytes:%d biBitCount:%d\n", bmp->fname, bmp->w, bmp->h, bmp->linebytes, bmp->bi.biBitCount );
	return	bmp;

	err_ret:
	if( bmp->lineBuf != NULL )
		free( bmp->lineBuf);
	if( bmp->fp != NULL )
		fclose(bmp->fp);
	if( bmp->fname != NULL )
		free( bmp->fname);
	free( bmp );
	return NULL;
}
int bmpClose( BMP *bmp )
{
	free( bmp->lineBuf);
	fclose(bmp->fp);
	free( bmp->fname);
	free( bmp );
	return 0;
}

unsigned char *bmpReadLine( BMP *bmp, int row )
{
	size_t	position;

	position = bmp->bf.bfOffBits + bmp->linebytes * (bmp->h - (row + 1));
	fseek( bmp->fp, position, SEEK_SET);
	if( fread( bmp->lineBuf, bmp->linebytes, 1, bmp->fp ) != 1){
		fprintf(stderr, "bmpReadLine: fread error %s:%d\n", bmp->fname, row);
		return NULL;
	}
	return bmp->lineBuf;
}

// BMP color order = B G R ( Alpha ). bmpSwapBR swap B & R and convert to R G B
void bmpSwapBR( unsigned char *lineBuf, int w, int bitCount )
{
	unsigned char	*r, *b;
	unsigned char	c;

	int	step = bitCount/8;

	int	i;

	b = lineBuf;
	r = b+2;
	for(i=0;i<w;i++){
		c = *b;
		*b = *r;
		*r = c;

		b += step;
		r += step;
	}
}
