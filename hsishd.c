#include<stdio.h>
#include<math.h>
#include"image.h"
#include"imgio.h"
static double ffmax( double *v,int n);
static double ffmin( double *v,int n);
static double to_rgb_sub( double h, double m1,double m2);

#define ESTA	1.0e-40
#define PAI	3.14159265
#define R	(*(rgb+0))
#define G	(*(rgb+1))
#define B	(*(rgb+2))
#define H	hsi[0]
#define S	hsi[1]
#define I	hsi[2]

double *to_hsi(double *rgb)
{
	double r,g,b,imax,imin;
	static double hsi[3] = {0.,0.,0.};

	/*printf("to_hsi: %lf %lf %lf\n",rgb[0],rgb[1],rgb[2]);*/
	imax = ffmax( rgb, 3 );
	imin = ffmin( rgb, 3 );
	/*printf("to_hsi: imax,imin = %lf %lf\n",imax,imin);*/
	I = ( imax+imin ) / 2.;
	if( fabs(imax-imin) < ESTA ){
		S = 0.;
		H = 0.;
		return hsi;
	}
	if( I <= 0.5 )
		S = ( imax-imin )/( imax+imin );
	else
		S = ( imax-imin )/( 2. - imax-imin );
	r = ( imax-R )/( imax-imin );
	g = ( imax-G )/( imax-imin );
	b = ( imax-B )/( imax-imin );
	if(      fabs(R-imax) < ESTA )
		H = PAI/3.*(b-g);
	else if( fabs(G-imax) < ESTA )
		H = PAI/3.*(2.+r-b);
	else
		H = PAI/3.*(4.+g-r);

	/*printf("to_hsi: H,S,I = %lf %lf %lf\n",H,S,I);*/
	return hsi;
}
#undef	R
#undef	G
#undef	B
#undef	H
#undef	S
#undef	I

#define H	(*(hsi+0))
#define S	(*(hsi+1))
#define I	(*(hsi+2))
#define R	rgb[0]
#define G	rgb[1]
#define B	rgb[2]
double * to_rgb( double * hsi)
{
	double m1,m2,h;
	static double rgb[3] = {0.,0.,0.};

	/*printf("to_rgb:H S I = %lf %lf %lf\n",H,S,I);*/
	if( I <= 0.5 )
		m2 = I * ( 1. + S );
	else
		m2 = I + S - I * S;
	m1 = 2. * I - m2;
	if( fabs(S) < ESTA )
		R = G = B = I;
	else {
		h = H + 2./3.*PAI;
		R = to_rgb_sub( h,m1,m2 );
		h = H;
		G = to_rgb_sub( h,m1,m2 );
		h = H - 2./3.*PAI;
		B = to_rgb_sub( h,m1,m2 );
	}
	/*printf("to_rgb:R G B = %lf %lf %lf\n",R,G,B);*/
	return rgb;
}
#undef	R
#undef	G
#undef	B
#undef	H
#undef	S
#undef	I
static double to_rgb_sub( double h, double m1,double m2)
{
	double x,hd;
	hd = h;
	if( h < 0. )
		hd = h + 2. * PAI;
	else if( h > 2.0 * PAI )
		hd = h - 2. * PAI;

	if( hd < PAI/3. )
		x = m1 + ( m2-m1 ) * hd/PAI*3.;
	else if( hd < PAI )
		x = m2;
	else if( hd < 4./3.*PAI )
		x = m1 + ( m2-m1 ) * ( 4./3.*PAI - hd )/PAI*3.;
	else
		x = m1;
	/*printf("to_rgb_sub:h,hd,m1,m2,x= %lf %lf %lf %lf %lf\n",h,hd,m1,m2,x);*/
	return x;
}
#undef ESTA
static double ffmax( double *v,int n)
{
	double f;

	f = *v;
	v++; n--;
	while( n > 0 ){
		if( *v > f ) f = *v;
		v++; n--;
	}
	return f;
}
static double ffmin(double *v,int n)
{
	double f;

	f = *v;
	v++; n--;
	while( n > 0 ){
		if( *v < f ) f = *v;
		v++; n--;
	}
	return f;
}

#if defined( MAIN )
IMAGE_FILE	*col_imgf, 	*shd_imgf,	*out_imgf;
IMAGE		*col_img, 	*shd_img,	*out_img;
int	histo[256];

#define	R	rgb[0]
#define	G	rgb[1]
#define	B	rgb[2]
#define	H	hsi[0]
#define	S	hsi[1]
#define	I	hsi[2]
#define	OR	orgb[0]
#define	OG	orgb[1]
#define	OB	orgb[2]
main( argc, argv )
int    argc;
char **argv;
{

	double	cnv_max = 0.15;
	double	ca,ca1,ca2,cb;
	int	i,nd,imax,imin,imean;
	unsigned char	*r,*g,*b,*sdr,*sdg,*sdb;
	unsigned char	*or,*og,*ob;
	double	rgb[3],*hsi,*orgb;
	double	cnv;
	unsigned char	ave;

	if( argc < 4 ){
		printf("usage: ");
		printf(argv[0]);
		printf(" color_image shade_image output_image\n");
		exit(0);
	}

	/*	IMAGE	file open	*/
	if((
	col_imgf = image_file_open(argv[1], IMAGE_RDONLY, IMAGE_BUFFERED )
		) == NULL )
		er_exit("color image open error");
	col_img = col_imgf->image;
	if((
	shd_imgf = image_file_open(argv[2], IMAGE_RDONLY, IMAGE_BUFFERED )
		) == NULL )
		er_exit("shade image open error");
	shd_img = shd_imgf->image;

	if((
	out_img = image_alloc( col_img->w, col_img->h, IMAGE_CHAR, 3 )
		) == NULL )
		er_exit("out image alloc error");
	if((
	out_imgf = image_file_create(argv[3], IMAGE_TRUNC, 0, out_img )
		) == NULL )
		er_exit("shade image open error");

	/*	Historgram	*/
	nd = img_histo( shd_img, histo );

	/*	Decide I scaling	*/
	imin  = img_limit( histo, nd, 0.01 );
	imean = img_limit( histo, nd, 0.50 );
	imax  = img_limit( histo, nd, 0.99 );
	ca1 = cnv_max / ( imean - imin );
	ca2 = cnv_max / ( imax  - imean );
	cb = imean;
	cb = imax;
	ca = 2.0* cnv_max / ( imax - imin );

	printf("hsishd: imin,imean,imax,ca1,ca2= %d %d %d %lf %lf\n",
			imin,imean,imax,ca1,ca2);

	/*	Conversion	*/
	r  = (unsigned char * )col_img->data;
	g  = (unsigned char * )col_img->data+(col_img->w*col_img->h);
	b  = (unsigned char * )col_img->data+(col_img->w*col_img->h)*2;
	sdr= (unsigned char * )shd_img->data;
	if( shd_imgf->nchan == 3 ){
	sdg  = (unsigned char * )shd_img->data+(shd_img->w*shd_img->h);
	sdb  = (unsigned char * )shd_img->data+(shd_img->w*shd_img->h)*2;
	} else {
		sdg = sdb = sdr;
	}
	or = (unsigned char * )out_img->data;
	og = (unsigned char * )out_img->data+(out_img->w*out_img->h);
	ob = (unsigned char * )out_img->data+(out_img->w*out_img->h)*2;

	for(i=0;i<nd;i++,r++,g++,b++,sdr++,sdg++,sdb++,or++,og++,ob++){
		if( (*r)+(*g)+(*b) == 0 ||
		    (*r)+(*g)+(*b) == 255*3 ) {
/*
			*or = *sdr;
			*og = *sdg;
			*ob = *sdb;
*/
			*or = *og = *ob = 0;
		} else {

			R = *r/255.;
			G = *g/255.;
			B = *b/255.;
			hsi = to_hsi( rgb );
/*
			if( *sd < imean )
				cnv = ca1*(*sd-cb);
			else
				cnv = ca2*(*sd-cb);
*/
			ave = ( *sdr + *sdg + *sdb ) / 3;
			cnv = ca*(ave-cb);
			/*printf("I,cnv,sd = %lf %lf %d\n",I,cnv,*sd);*/
			I += ca*(ave-cb);
			if( I >  1.0 ) I=1.0;
			if( I <= 0.0 ) I=0.0;

			orgb = to_rgb( hsi );
			*or = OR * 255. + 0.4999;
			*og = OG * 255. + 0.4999;
			*ob = OB * 255. + 0.4999;
		}
	}
	/*	Close	*/

	image_file_close( col_imgf );
	image_file_close( shd_imgf );
	image_file_close( out_imgf );
	image_destroy( out_img );
}

int img_histo( img, histo )
IMAGE	*img;
int	*histo;
{
	unsigned char	*dat, *datr, *datg, *datb;
	unsigned char	ave;
	int	n,i;

	for(i=0;i<255;i++)
		histo[i] = 0;

	if( img->nchan == 3 ){
		datr = (unsigned char *) img->data;
		datg = (unsigned char *) img->data + img->w * img->h;
		datb = (unsigned char *) img->data + img->w * img->h * 2;
		n = img->w*img->h;
		for(i=0;i<n;i++,datr++,datg++,datb++){
			ave = ( *datr + *datg + *datb ) / 3;
			histo[ave]++;
		}
	} else {
		n = img->w*img->h;
		dat = (unsigned char *) img->data;

		for(i=0;i<n;i++,dat++)
			histo[*dat]++;
	}
	return n;
}

int
img_limit( histo, nd, limit )
int	*histo,nd;
double	limit;
{
	int	nlim,i,count=0;

	nlim = nd * limit+0.4999;
	for(i=0;i<255;i++){
		count += histo[i];
		if( count > nlim ) break;
	}
	if( i == 256 ) i=255;
	return i;
}
int er_exit(mes)
char	*mes;
{
	printf( mes );
	printf("\n");
	exit(1);
}
#endif
