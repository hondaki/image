#include<stdio.h>
#include<stdlib.h>
#include"image.h"
#include"imgio.h"

void usage()
{
	fprintf(stderr, "remove 8 connectivities in inputfile and writ out to outputfile\n"
			" The input file should be 8 bit and the 1st channel is considered as the binary image. 0 is false non 0 is true\n"
			"usage: rm8con.a inputfile outputfile\n");
}
int main( int argc, char **argv )
{
	IMAGE_FILE	*imgf1, *imgf2;
	char	*fname1, *fname2;

	int	i,j,w,h;

	u_char	**pix;

	fname1 = argv[1];
	fname2 = argv[2];

	if( argc != 3 ){
		usage();
		exit(1);
	}

	img_err_exit_set( 0 );
	imgf1 = image_file_open( fname1, IMAGE_RDONLY, IMAGE_BUFFERED);
	if( imgf1 == NULL ){
		fprintf(stderr, "file open error %s\n", fname1 );
		exit(1);
	}

	w = imgf1->w;
	h = imgf1->h;

	pix = (u_char**) imgf1->image->data[0];

	for(i=0;i<h-1;i++){
		for(j=1;j<w;j++){
			if( !pix[i][j] )
				continue;
			if( !pix[i][j-1] && !pix[i+1][j] && pix[i+1][j-1] ){
				pix[i][j] = 0;
			}
		}
	}
			

	imgf2 = image_file_create( fname2, IMAGE_TRUNC, 0, imgf1->image );
	if( imgf2 == NULL ){
		fprintf(stderr, "file open error %s\n", fname1 );
		exit(1);
	}

	image_file_close( imgf2 );
	image_file_close( imgf1 );

	return 0;
}
